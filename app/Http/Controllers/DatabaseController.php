<?php

namespace App\Http\Controllers;

use App\database;
use Illuminate\Http\Request;

class DatabaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\database  $database
     * @return \Illuminate\Http\Response
     */
    public function show(database $database)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\database  $database
     * @return \Illuminate\Http\Response
     */
    public function edit(database $database)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\database  $database
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, database $database)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\database  $database
     * @return \Illuminate\Http\Response
     */
    public function destroy(database $database)
    {
        //
    }
}
