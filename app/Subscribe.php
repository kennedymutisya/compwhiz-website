<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Subscribe
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe query()
 * @mixin \Eloquent
 */
class Subscribe extends Model
{
    //
}
