<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    public function database()
    {
       return $this->hasMany(database::class, 'school_id', 'id');
    }
}
