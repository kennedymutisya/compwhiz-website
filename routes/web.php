<?php

Route::get('/', function () {
//    \Illuminate\Support\Facades\Mail::to(['jumaronald.net@gmail.com','kenmsh@gmail.com','Omu2003@gmail.com'])->send(new
//    \App\Mail\DomainStuff ());
//    return 'Mutisya';
//    return new \App\Mail\DomainStuff();

    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('contact-compwhiz-today', function () {
    return view('contactus');
});

Route::get('compwhiz-services', function () {
    return view('services');
});

Route::post('school', function (\Illuminate\Http\Request $request) {
    return response()->json($request->all());
//    Storage::append('school.txt', 'Appended Text');
});

Route::get('school/{school}', function ($school) {
    return response()->json($school);
});

Route::get('install', function () {
    $output = Artisan::call('migrate:fresh');
    $output1 = Artisan::call('nova:install');
    return response()->json($output1);
});
Auth::routes();
