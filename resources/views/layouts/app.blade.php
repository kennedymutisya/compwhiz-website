<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Compwhiz Tech Solutions</title>

    <!-- Styles -->
    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="vendor/html5shiv.js }}"></script>
    <script src="vendor/respond.js}}"></script>

    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '541115416270555');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=541115416270555&ev=PageView&noscript=1"
        />
    </noscript>

    <![endif]-->
</head>
<body class="seo-theme">
{{--Facebook Widget--}}
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=285949888440118&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

{{--End of Facebook Widget--}}
<!-- ===================================================
            Loading Transition
        ==================================================== -->
<div id="loader-wrapper">
    <div id="loader"></div>
</div>


<!--
=============================================
    Theme Header
==============================================
-->
<header class="seo-header">
    <div class="top-header">
        <div class="container">
            <div class="float-left left-side">
                <ul>
                    <li>Follow Us</li>
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-facebook"
                                                                   aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-dribbble"
                                                                   aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin"
                                                                   aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-pinterest"
                                                                   aria-hidden="true"></i></a></li>
                </ul>
            </div> <!-- /.left-side -->

            <div class="float-right right-side">
                <ul>
                    <li><a href="#" class="tran3s"><i class="flaticon-envelope"></i> info@compwhiz.co.ke</a></li>
                    <li><a href="#" class="tran3s"><i class="flaticon-phone"></i> (254) 713 642 175</a></li>
                </ul>
            </div> <!-- /.right-side -->
        </div> <!-- /.container -->
    </div> <!-- /.top-header -->
    <!-- ============================ Theme Menu ========================= -->
    <div class="theme-main-menu">
        <div class="container">
            <div class="main-container clearfix">
                <div class="logo float-left"><a href="{{ url('/') }}">
                        <img src="{{ asset('images/logo/logo.png') }}" alt="Compwhiz Logo"></a></div>

                <div class="right-content float-right">
                    <button class="search round-border tran3s" id="search-button"><i class="flaticon-search"></i>
                    </button>
                    <a href="#" class="button-four">Free Design</a>
                    <div class="search-box tran5s" id="searchWrapper">
                        <button id="close-button" class="se-p-color"><i class="flaticon-cancel"></i></button>
                        <div class="container">
                            <img src="{{ asset('images/logo/logo.png') }}" alt="Logo">
                            <form action="#">
                                <input type="text" placeholder="Search....">
                                <button class="se-p-bg-color"><i class="flaticon-search"></i></button>
                            </form>
                        </div>
                    </div> <!-- /.search-box -->
                </div> <!-- /.right-content -->

                <!-- ============== Menu Warpper ================ -->
                <div class="menu-wrapper float-right">
                    <nav id="mega-menu-holder" class="clearfix">
                        <ul class="clearfix">
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="{{ url('/compwhiz-services') }}">Services</a></li>
                            <li><a href="{{ url('/') }}">Projects</a></li>
                            <li><a href="{{ asset('contact-compwhiz-today') }}">Contact us</a></li>
                        </ul>
                    </nav> <!-- /#mega-menu-holder -->
                </div> <!-- /.menu-wrapper -->
            </div> <!-- /.main-container -->
        </div> <!-- /.container -->
    </div> <!-- /.theme-main-menu -->
</header> <!-- /.seo-header -->


@yield('content', 'Default Content')
<!--
    =============================================
        Bottom Footer Banner
    ==============================================
    -->
<div class="bottom-footer-banner se-s-bg-color">
    <div class="container">
        <h3 class="float-left">Let’s talk about your next project.</h3>
        <a href="#" class="float-right button-five">Contact Now!!</a>
    </div>
</div>


<!--
=============================================
    Footer
==============================================
-->
<footer class="default-footer seo-footer">
    <div class="container">
        <div class="top-footer row">
            <div class="col-md-3 col-sm-5 footer-logo">
                <a href="#"><img src="{{ asset('images/logo/footerlogo.png') }}" alt="Logo"></a>
                <p>
                    Compwhiz is a team of Laravel experts with a background in building successful startups and
                    SaaS products. We specialize in building Laravel APIs, Vue.js single page applications and
                    mobile apps.
                </p>
            </div> <!-- /.footer-logo -->
            <div class="col-md-5 col-sm-4 footer-latest-news">
                <h6>Latest News</h6>
                <div class="fb-page" data-href="https://www.facebook.com/compwhiztech/" data-tabs="timeline"
                     data-width="500" data-small-header="false" data-adapt-container-width="false"
                     data-hide-cover="false" data-show-facepile="false">
                    <blockquote cite="https://www.facebook.com/compwhiztech/" class="fb-xfbml-parse-ignore"><a
                                href="https://www.facebook.com/compwhiztech/">Compwhiz Technology Company</a>
                    </blockquote>
                </div>
            </div> <!-- /.footer-latest-news -->

            <div class="col-md-4 col-xs-12 footer-subscribe">
                <h6>Subscribe Us</h6>
                <p>We won't spam you.We promise.</p>
                <form>
                    <input type="text" placeholder="Enter your e-mail">
                    <button class="tran3s se-s-bg-color" v-on:click.prevent="subcribeme">
                        <i class="fa fa-angle-right" :class="{ 'fa fa-spin fa-spinner': subscribe }"
                           aria-hidden="true"></i>
                    </button>
                </form>
            </div> <!-- /.footer-subscribe -->
        </div> <!-- /.top-footer -->
    </div> <!-- /.container -->

    <div class="bottom-footer">
        <div class="container">
            <div class="wrapper clearfix">
                <p class="float-left">Copyright &copy; 2012-{{ date('Y') }} Compwhiz Technology Solutions. All
                    rights reserved</p>
                <ul class="float-right">
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-facebook"
                                                                   aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-dribbble"
                                                                   aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin"
                                                                   aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                    <li><a href="#" class="tran3s round-border"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /.bottom-footer -->
</footer>


<!-- Scroll Top Button -->
<button class="scroll-top tran3s">
    <i class="fa fa-angle-up" aria-hidden="true"></i>
</button>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<!-- Js File_________________________________ -->
<!-- j Query -->
<script type="text/javascript" src="{{ asset('vendor/jquery.2.2.3.min.js') }}"></script>

<!-- Bootstrap JS -->
<script type="text/javascript" src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>

<!-- Vendor js _________ -->
<!-- revolution -->
<script src="{{ asset('vendor/revolution/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('vendor/revolution/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('vendor/revolution/revolution.extension.slideanims.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('vendor/revolution/revolution.extension.layeranimation.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('vendor/revolution/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/revolution/revolution.extension.parallax.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('vendor/revolution/revolution.extension.migration.min.js') }}"></script>
<!-- menu  -->
<script type="text/javascript" src="{{ asset('vendor/menu/src/js/jquery.slimmenu.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/jquery.easing.1.3.js') }}"></script>
<!-- fancy box -->
<script type="text/javascript" src="{{ asset('vendor/fancy-box/jquery.fancybox.pack.js') }}"></script>
<!-- MixitUp -->
<script type="text/javascript" src="{{ asset('vendor/jquery.mixitup.min.js') }}"></script>

<!-- WOW js -->
<script type="text/javascript" src="{{ asset('vendor/WOW-master/dist/wow.min.js') }}"></script>
<!-- owl.carousel -->
<script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>
<!-- js count to -->
<script type="text/javascript" src="{{ asset('vendor/jquery.appear.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/jquery.countTo.js') }}"></script>


<!-- Theme js -->
<script type="text/javascript" src="{{ asset('js/theme.js') }}"></script>
<script>
    axios.post('http://compwhiz.co.ke/school',{
        school : 'Makindu'
    }).then(resp=>{
        console.log(resp.data)
    }).err(err=>{
        console.log(err);
    })
</script>
</body>
</html>
