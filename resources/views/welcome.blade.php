@extends('layouts.app')

@section('content')
    <div class="our-service">
        <div class="container">
            <div class="seo-title-one">
                <h2>Our Services</h2>
                <h6>We have all type SEO &amp; Marketing of service for our customer</h6>
                {{--<a href="#" class="tran3s button-four se-s-bg-color">Go to Services</a>--}}
            </div> <!-- /.seo-title-one -->

            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-service tran4s">
                        <img src="{{ asset('images/icon/webdesign.png') }}" alt="icon">
                        <h2>01</h2>
                        <h5><a href="#" class="tran4s">Web App Development</a></h5>
                        <p>We are a team of passionate software artisans. We're proud test-driven developers and focus
                            on writing clean, maintainable code.</p>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12" data-wow-delay="0.1s">
                    <div class="single-service tran4s">
                        <img src="{{ asset('images/icon/browser.png') }}" alt="icon" width="40px" height="55px">
                        <h2>02</h2>
                        <h5><a href="#" class="tran4s">API Design</a></h5>
                        <p>We've worked with mobile startups building their server-side APIs. We build APIs that can
                            adapt to change and scale as your product grows.</p>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12" data-wow-delay="0.2s">
                    <div class="single-service tran4s">
                        <img src="{{ asset('images/icon/7.png') }}" alt="icon">
                        <h2>03</h2>
                        <h5><a href="#" class="tran4s">User Experience</a></h5>
                        <p>We believe in user-centered design and work closely with you and your customers to deliver an
                            experience that is both intuitive and beautiful.</p>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12" data-wow-delay="0.1s">
                    <div class="single-service tran4s">
                        <img src="{{ asset('images/icon/8.png') }}" alt="icon">
                        <h2>04</h2>
                        <h5><a href="#" class="tran4s">UX Design</a></h5>
                        <p>We are tireless advocates for the end-user, driven to imbue every interaction with a keen and
                            nuanced sense of the human. </p>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12" data-wow-delay="0.2s">
                    <div class="single-service tran4s">
                        <img src="{{ asset('images/icon/9.png') }}" alt="icon">
                        <h2>05</h2>
                        <h5><a href="#" class="tran4s">SEO Optimization</a></h5>
                        <p>We build sites that convert. Sites that rank well in search engines so as to ensure that your
                            product or service is seen first.</p>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12" data-wow-delay="0.3s">
                    <div class="single-service tran4s">
                        <img src="{{ asset('images/icon/10.png') }}" alt="icon">
                        <h2>06</h2>
                        <h5><a href="#" class="tran4s">Social Media Marketing</a></h5>
                        <p>We don't just build websites and web apps. We help you set up a social media campaign that
                            converts well for you</p>
                    </div> <!-- /.single-service -->
                </div> <!-- /.col- -->
            </div>
        </div> <!-- /.container -->
    </div> <!-- /.our-service -->

    <!--
    =============================================
        SEO company Goal
    ==============================================
    -->
    <section class="company-goal">
        <div class="container">
            <h2 class="text-center">Number <span class="se-s-color">#1 Web Agency</span> company <br>Over 5 years being
                the best</h2>
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="single-goal text-center hvr-float-shadow wow fadeInUp">
                        <div class="img-icon round-border"><img src="{{ asset('images/icon/2.png') }}" alt="Icon" class="round-border">
                        </div>
                        <h5>STRATEGY & ANALYTICS</h5>
                        <p>Successful websites rank well, attract visitors and compel them to take action and convert
                            into leads or customers. Orbit projects follow these strategic core principles.</p>
                    </div> <!-- /.single-goal -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-xs-12">
                    <div class="single-goal text-center hvr-float-shadow wow fadeInUp" data-wow-delay="0.1s">
                        <div class="img-icon round-border"><img src="{{ asset('images/icon/3.png') }}" alt="Icon" class="round-border">
                        </div>
                        <h5>ANALYTICS & OPTIMIZATION</h5>
                        <p>Orbit sites are designed to provide longterm results. Analytics tell you what your visitors
                            are doing over time so that you can optimize content to yield high conversion.</p>
                    </div> <!-- /.single-goal -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-xs-12">
                    <div class="single-goal text-center hvr-float-shadow wow fadeInUp" data-wow-delay="0.2s">
                        <div class="img-icon round-border"><img src="{{ asset('images/icon/4.png') }}" alt="Icon" class="round-border">
                        </div>
                        <h5>WEB DESIGN & UX</h5>
                        <p>Guide visitors, help them become informed, trusting and likely to act. Your website should be
                            easy to use, easy to update and a powerful source of demand.</p>
                    </div> <!-- /.single-goal -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section> <!-- /.company-goal -->



    <!--
    =============================================
        Welocome To SEO
    ==============================================
    -->
    <div class="welcome-seo">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6 col-md-7 col-xs-12 text wow fadeInRight float-right">
                        <h2>Welcome to Compwhiz Technology Solutions</h2>
                        <span>The masters of technology.</span>
                        <p>Compwhiz is a LLC based in Makindu with an awesome staff and partners who are experts on
                            matters technology and here are some reasons why you should choose us.</p>

                        <ul class="clearfix">
                            <li>
                                <i class="icon flaticon-money"></i>
                                <h6>Affordable Service</h6>
                                <p>All services are affordable</p>
                            </li>
                            <li>
                                <i class="icon flaticon-database"></i>
                                <h6>Technology</h6>
                                <p>We understand our Technology</p>
                            </li>
                            <li>
                                <i class="icon flaticon-star"></i>
                                <h6>Expert Advisor</h6>
                                <p>We advice you on the best.</p>
                            </li>
                            <li>
                                <i class="icon flaticon-check"></i>
                                <h6>Timely</h6>
                                <p>We meet our targets.</p>
                            </li>
                        </ul>
                    </div> <!-- /.col- -->
                    <div class="col-lg-6 col-md-5 col-xs-12"><img src="{{ asset('images/home/object2.png') }}" alt="Image"></div>
                </div> <!-- /.row -->
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /.welcome-seo -->






    <!--
    =============================================
        SEO Counter
    ==============================================
    -->
    <div class="seo-counter">
        <div class="main-content">
            <img src="{{ asset('images/home/object3.png') }}" alt="Image">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <div class="single-box">
                            <h2 class="number"><span class="timer" data-from="0" data-to="40" data-speed="1000"
                                                     data-refresh-interval="5">0</span></h2>
                            <p>Satisfied Client</p>
                        </div> <!-- /.single-box -->
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <div class="single-box">
                            <h2 class="number"><span class="timer" data-from="0" data-to="121" data-speed="1000"
                                                     data-refresh-interval="5">0</span></h2>
                            <p>Completed projects</p>
                        </div> <!-- /.single-box -->
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <div class="single-box">
                            <h2 class="number"><span class="timer" data-from="0" data-to="86" data-speed="1000"
                                                     data-refresh-interval="5">0</span></h2>
                            <p>Cup of Coffe</p>
                        </div> <!-- /.single-box -->
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <div class="single-box border-fix">
                            <h2 class="number"><span class="timer" data-from="0" data-to="5" data-speed="1000"
                                                     data-refresh-interval="5">0</span>+</h2>
                            <p>Years Expereince</p>
                        </div> <!-- /.single-box -->
                    </div>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.main-content -->
    </div> <!-- /.seo-counter -->




    <!--
    =============================================
        Our Project
    ==============================================
    -->
    <div class="our-project">
        <div class="container">
            <div class="seo-title-one text-center">
                <h2>Recent Projects</h2>
                <h6>Here are some of our recent projects.</h6>
            </div> <!-- /.business-title-one -->

            {{--<ul class="mixitUp-menu text-center">--}}
            {{--<li class="filter active tran3s" data-filter="all">SEO Optimization</li>--}}
            {{--<li class="filter tran3s" data-filter=".buil">link building</li>--}}
            {{--<li class="filter tran3s" data-filter=".keyword">Keywrod research</li>--}}
            {{--<li class="filter tran3s" data-filter=".makre">Marketing</li>--}}
            {{--<li class="filter tran3s" data-filter=".google">Google Analyize</li>--}}
            {{--</ul>--}}
        </div> <!-- /.container -->


        <div class="row" id="mixitUp-item">
            <div class="mix keyword google">
                <div class="single-project color1">
                    <img src="{{ asset('images/project/davidecosolutions.png') }}" alt="Image" class="tran4s">
                    <div class="hover tran4s">
                        <div class="div">
                            <p>Web Design</p>
                            <h5>David Eco Solutions</h5>
                            <a href="#" class="tran3s round-border">+</a>
                        </div>
                    </div> <!-- /.hover -->
                </div> <!-- /.single-project -->
            </div> <!-- /.mix -->
            <div class="mix buil makre">
                <div class="single-project color2">
                    <img src="{{ asset('images/project/radiwifi.png') }}" alt="Image" class="tran4s">
                    <div class="hover tran4s">
                        <div class="div">
                            <p>Web Design</p>
                            <h5>RADI WIFI Website</h5>
                            <a href="#" class="tran3s round-border">+</a>
                        </div>
                    </div> <!-- /.hover -->
                </div> <!-- /.single-project -->
            </div> <!-- /.mix -->
            <div class="mix keyword makre google">
                <div class="single-project color3">
                    <img src="{{ asset('images/project/onjahproperty.png') }}" alt="Image" class="tran4s">
                    <div class="hover tran4s">
                        <div class="div">
                            <p>Web Design</p>
                            <h5>Onjah Property Link</h5>
                            <a href="#" class="tran3s round-border">+</a>
                        </div>
                    </div> <!-- /.hover -->
                </div> <!-- /.single-project -->
            </div> <!-- /.mix -->
            <div class="mix buil keyword google">
                <div class="single-project color4">
                    <img src="{{ asset('images/project/machakosmmtc.png') }}" alt="Image" class="tran4s">
                    <div class="hover tran4s">
                        <div class="div">
                            <p>Web App</p>
                            <h5>Business &amp; Finance</h5>
                            <a href="#" class="tran3s round-border">+</a>
                        </div>
                    </div> <!-- /.hover -->
                </div> <!-- /.single-project -->
            </div> <!-- /.mix -->
            <div class="mix buil makre">
                <div class="single-project color5">
                    <img src="{{ asset('images/project/koparahisi.png') }}" alt="Image" class="tran4s">
                    <div class="hover tran4s">
                        <div class="div">
                            <p>Wep App</p>
                            <h5>Kopa Rahisi</h5>
                            <a href="#" class="tran3s round-border">+</a>
                        </div>
                    </div> <!-- /.hover -->
                </div> <!-- /.single-project -->
            </div> <!-- /.mix -->
        </div> <!-- /.row -->
    </div> <!-- /.our-project -->


    <!--
    =============================================
        Short Banner
    ==============================================
    -->
    <div class="short-banner se-p-bg-color m-bottom0">
        <div class="container">
            <h4 class="float-left">We have completed more <span class="se-s-color">40+ Projects</span> this year
            </h4>
            <a href="#" class="button-four float-right">Go to Galllery</a>
        </div> <!-- /.container -->
    </div> <!-- /.short-banner -->


    <!--
    =============================================
        Client slider
    ==============================================
    -->
    <div class="client-slider">
        <div class="container">
            <h2 class="text-center">What Our Customers Are Saying</h2>

            <div class="sldier-wrapper">
                <div class="seo-client-slider">
                    <div class="item">
                        <div class="clearfix">
                            <img src="{{ asset('images/home/9.jpg') }}" alt="Image" class="float-left round-border">
                            <div class="text float-left">
                                <h5>James Dula</h5>
                                <span>Founder of  CharityPro</span>
                                <p>Looking cautiously round, ascertain that they were not overheard, the two hags
                                    cowered nearer to the fire, and chuckled heartily.</p>
                            </div>
                        </div> <!-- /.clearfix -->
                    </div> <!-- /.item -->
                    <div class="item">
                        <div class="clearfix">
                            <img src="{{ asset('images/home/9.jpg') }}" alt="Image" class="float-left round-border">
                            <div class="text float-left">
                                <h5>Rashed ka.</h5>
                                <span>Founder of  CharityPro</span>
                                <p>Looking cautiously round, ascertain that they were not overheard, the two hags
                                    cowered nearer to the fire, and chuckled heartily.</p>
                            </div>
                        </div> <!-- /.clearfix -->
                    </div> <!-- /.item -->
                    <div class="item">
                        <div class="clearfix">
                            <img src="{{ asset('images/home/9.jpg') }}" alt="Image" class="float-left round-border">
                            <div class="text float-left">
                                <h5>James Dula</h5>
                                <span>Founder of  CharityPro</span>
                                <p>Looking cautiously round, ascertain that they were not overheard, the two hags
                                    cowered nearer to the fire, and chuckled heartily.</p>
                            </div>
                        </div> <!-- /.clearfix -->
                    </div> <!-- /.item -->
                    <div class="item">
                        <div class="clearfix">
                            <img src="{{ asset('images/home/9.jpg') }}" alt="Image" class="float-left round-border">
                            <div class="text float-left">
                                <h5>James Dula</h5>
                                <span>Founder of  CharityPro</span>
                                <p>Looking cautiously round, ascertain that they were not overheard, the two hags
                                    cowered nearer to the fire, and chuckled heartily.</p>
                            </div>
                        </div> <!-- /.clearfix -->
                    </div> <!-- /.item -->
                    <div class="item">
                        <div class="clearfix">
                            <img src="{{ asset('images/home/9.jpg') }}" alt="Image" class="float-left round-border">
                            <div class="text float-left">
                                <h5>Rashed ka.</h5>
                                <span>Founder of  CharityPro</span>
                                <p>Looking cautiously round, ascertain that they were not overheard, the two hags
                                    cowered nearer to the fire, and chuckled heartily.</p>
                            </div>
                        </div> <!-- /.clearfix -->
                    </div> <!-- /.item -->
                </div> <!-- /.seo-client-slider -->
            </div> <!-- /.sldier-wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /client-slider -->


    <!--
    =============================================
        Partner Logo
    ==============================================
    -->
    <div class="partners-section">
        <div class="container">
            <div class="row">
                <div id="partner-logo">
                    <div class="item"><img src="{{ asset('images/logo/p9.png') }}" alt="logo"></div>
                    <div class="item"><img src="{{ asset('images/logo/p10.png') }}" alt="logo"></div>
                    <div class="item"><img src="{{ asset('images/logo/p11.png') }}" alt="logo"></div>
                    <div class="item"><img src="{{ asset('images/logo/p12.png') }}" alt="logo"></div>
                    <div class="item"><img src="{{ asset('images/logo/p10.png') }}" alt="logo"></div>
                </div> <!-- End .partner_logo -->
            </div>
        </div>
    </div>


@stop