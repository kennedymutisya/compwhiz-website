@component('mail::message')
# Hi there,

Not sure if you knew this but your website has some problems that you might want to consider looking into.
I spent 2 &mdash; 3 minutes looking around and found:

* It doesn't work well on mobile phones ( which is how most people browse the web these days)
* It looks really, really dated compared to some of your competitors ( check out these guys: http://zuhrimedicare.co.ke)
* Its hard to read on new monitors
* Its not integrated with your Hospital Management System.
* It does not reflect the elegance associated with Radiant Group of Hospitals


I actually do web design and web app development (Hospital Management System tailored to your hospital) as a living so
I figured I'd reach out to you and let you know there's serious room for dead-easy (and affordable) improvement.
We can increase Sales, Engagement, Conversions and more through our design and UX strategies to keep your audience engaged and on your site longer!
We Would like to work with you on a risk-free trial period? You won’t pay anything if you’re not happy with the work.

Is that something you'd be interested in?
For more information about our company please visit www.compwhiz.co.ke

Best Regards,<br>

Kennedy Mutisya <br>
Web Development Specialist, Compwhiz Tech Solutions. <br>
Mobile &mdash; (254) 713 642 175
@endcomponent
