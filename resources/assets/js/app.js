/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',

    data: {
        'subscribe': false,
        'savingContactForm': false
    },
    methods: {

        subcribeme() {
            let scope = this;
            scope.subscribe = true;
            axios.post('subscribe', this.$data).then(function () {
                scope.subscribe = false;
            }).catch(function () {
                scope.subscribe = false;
            });
            this.subscribe = true;

        },

        ContactUs() {
            let sco = this;

            sco.savingContactForm = true;

            axios.post('subscribe', this.$data).then(function () {
                sco.subscribe = false;
            }).catch(function () {
                sco.subscribe = false;
            });
        }
    }
});
