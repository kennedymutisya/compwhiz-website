!function(t) {
    function e(e) {
        for (var i = e[0], s = e[1], r = e[2], l, c, d = 0, p = []; d < i.length; d++)
            c = i[d],
            n[c] && p.push(n[c][0]),
                n[c] = 0;
        for (l in s)
            Object.prototype.hasOwnProperty.call(s, l) && (t[l] = s[l]);
        for (u && u(e); p.length; )
            p.shift()();
        return o.push.apply(o, r || []),
            a()
    }
    function a() {
        for (var t, e = 0; e < o.length; e++) {
            for (var a = o[e], i = !0, s = 1; s < a.length; s++) {
                var l = a[s];
                0 !== n[l] && (i = !1)
            }
            i && (o.splice(e--, 1),
                t = r(r.s = a[0]))
        }
        return t
    }
    var i = {}
        , n = {
        1: 0
    }
        , o = [];
    function s(t) {
        return r.p + "" + t + "." + {
            4: "c7840cc71263772106db",
            5: "6ab9cf12115ff0a15ce2",
            6: "831ee0129106fc8a1016",
            7: "66b8103143e7676583f9",
            8: "1b34a92c73745b3b84e4",
            9: "9a9ec3345fee62e60d68",
            10: "d0232c9edc264a313560",
            11: "3e5cecbe62736db4cabb",
            12: "aa386d62c72e0c1efb77",
            13: "b90dca2fe9374aa3ebb8",
            14: "f6d6cad55719d0bcd6bb",
            15: "aa95bed29cdf890787d4",
            16: "8f1bf4b57a6e31a10738",
            17: "7074a1398f53988775d3",
            18: "2550186e841a314e364f",
            19: "5ad3739aeac55844d0f0",
            20: "697b973b65e7d546420b",
            21: "6b37b57f3842f0fca936",
            22: "c800dfe7b4b43a78b2b3",
            23: "ab8026661ea5e06b0011",
            24: "aa8695a8cc17aef8a857",
            25: "522e614afd22b029d4cc",
            26: "f530519697876b880821",
            27: "118eb8484392df279a74",
            28: "56274dc731414bb69e08"
        }[t] + ".js"
    }
    function r(e) {
        if (i[e])
            return i[e].exports;
        var a = i[e] = {
            i: e,
            l: !1,
            exports: {}
        };
        return t[e].call(a.exports, a, a.exports, r),
            a.l = !0,
            a.exports
    }
    r.e = function t(e) {
        var a = []
            , i = n[e];
        if (0 !== i)
            if (i)
                a.push(i[2]);
            else {
                var o = new Promise(function(t, a) {
                        i = n[e] = [t, a]
                    }
                );
                a.push(i[2] = o);
                var l = document.getElementsByTagName("head")[0], c = document.createElement("script"), d;
                c.charset = "utf-8",
                    c.timeout = 120,
                r.nc && c.setAttribute("nonce", r.nc),
                    c.src = s(e),
                    d = function(t) {
                        c.onerror = c.onload = null,
                            clearTimeout(u);
                        var a = n[e];
                        if (0 !== a) {
                            if (a) {
                                var i = t && ("load" === t.type ? "missing" : t.type)
                                    , o = t && t.target && t.target.src
                                    , s = new Error("Loading chunk " + e + " failed.\n(" + i + ": " + o + ")");
                                s.type = i,
                                    s.request = o,
                                    a[1](s)
                            }
                            n[e] = void 0
                        }
                    }
                ;
                var u = setTimeout(function() {
                    d({
                        type: "timeout",
                        target: c
                    })
                }, 12e4);
                c.onerror = c.onload = d,
                    l.appendChild(c)
            }
        return Promise.all(a)
    }
        ,
        r.m = t,
        r.c = i,
        r.d = function(t, e, a) {
            r.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: a
            })
        }
        ,
        r.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                value: "Module"
            }),
                Object.defineProperty(t, "__esModule", {
                    value: !0
                })
        }
        ,
        r.t = function(t, e) {
            if (1 & e && (t = r(t)),
            8 & e)
                return t;
            if (4 & e && "object" == typeof t && t && t.__esModule)
                return t;
            var a = Object.create(null);
            if (r.r(a),
                Object.defineProperty(a, "default", {
                    enumerable: !0,
                    value: t
                }),
            2 & e && "string" != typeof t)
                for (var i in t)
                    r.d(a, i, function(e) {
                        return t[e]
                    }
                        .bind(null, i));
            return a
        }
        ,
        r.n = function(t) {
            var e = t && t.__esModule ? function e() {
                    return t.default
                }
                : function e() {
                    return t
                }
            ;
            return r.d(e, "a", e),
                e
        }
        ,
        r.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        }
        ,
        r.p = "./",
        r.oe = function(t) {
            throw t
        }
    ;
    var l = window.webpackJsonp = window.webpackJsonp || []
        , c = l.push.bind(l);
    l.push = e,
        l = l.slice();
    for (var d = 0; d < l.length; d++)
        e(l[d]);
    var u = c;
    o.push([371, 0, 3]),
        a()
}(Array(18).concat([function(t, e, a) {
    "use strict";
    Object.defineProperty(e, "__esModule", {
        value: !0
    });
    var i, n = u(a(52)), o, s = u(a(357)), r, l = u(a(283)), c, d = u(a(285));
    function u(t) {
        return t && t.__esModule ? t : {
            default: t
        }
    }
    n.default.use(s.default),
        n.default.use(l.default);
    var p = "GET_LIVE_MATCHES"
        , f = "GET_MATCHES"
        , m = new s.default.Store({
        state: {
            betSlip: JSON.parse(localStorage.getItem("betSlip")) ? JSON.parse(localStorage.getItem("betSlip")) : [],
            jengabetBetSlip: JSON.parse(localStorage.getItem("jengabetBetSlip")) ? JSON.parse(localStorage.getItem("jengabetBetSlip")) : [],
            jackpot: JSON.parse(sessionStorage.getItem("jackpot")) ? JSON.parse(sessionStorage.getItem("jackpot")) : [],
            freeJackpot: [],
            matches: [],
            liveMatches: [],
            context: "",
            stake: 100,
            snackBar: {
                show: !1,
                messages: [""]
            },
            search: {
                show: !1
            },
            user: JSON.parse(localStorage.getItem("user")) ? JSON.parse(localStorage.getItem("user")) : {},
            balance: 0,
            numberOfLiveMatches: 0
        },
        mutations: {
            setNumberOfLiveMatches: function t(e, a) {
                e.numberOfLiveMatches = a
            },
            setBalance: function t(e, a) {
                e.balance = a
            },
            setUser: function t(e, a) {
                e.user = a
            },
            updateStake: function t(e, a) {
                e.stake = a
            },
            addToBetslip: function t(e, a) {
                if (!navigator.onLine)
                    return e.snackBar = {
                        show: !0,
                        messages: ["Please get back online to add to your betslip"],
                        status: "error"
                    },
                        void setTimeout(function() {
                            e.snackBar = {
                                show: !1,
                                messages: [""]
                            }
                        }, 5e3);
                if (e.betSlip.length > 19)
                    e.snackBar = {
                        show: !0,
                        messages: ["Maximum number of bets reached (20)"],
                        status: "warning"
                    },
                        setTimeout(function() {
                            e.snackBar = {
                                show: !1,
                                messages: [""]
                            }
                        }, 5e3);
                else {
                    var i = {
                        profile_id: a.profile_id,
                        app_name: a.app_name,
                        parent_match_id: a.parent_match_id,
                        game_description: a.game_description,
                        bet_pick: a.bet_pick,
                        sub_type_id: a.sub_type_id,
                        market_name: a.market,
                        odd_key: a.odd_key,
                        odd_value: a.odd_value,
                        special_bet_value: a.special_bet_value,
                        bet_type: a.bet_type,
                        home_team: a.home_team,
                        away_team: a.away_team
                    };
                    d.default.session.betslip.add(i),
                        e.betSlip.push(a),
                        localStorage.setItem("betSlip", JSON.stringify(e.betSlip))
                }
            },
            addToJengabetBetslip: function t(e, a) {
                if (!navigator.onLine)
                    return e.snackBar = {
                        show: !0,
                        messages: ["Please get back online to add to your betslip"],
                        status: "error"
                    },
                        void setTimeout(function() {
                            e.snackBar = {
                                show: !1,
                                messages: [""]
                            }
                        }, 5e3);
                if (e.betSlip.length > 19)
                    e.snackBar = {
                        show: !0,
                        messages: ["Maximum number of bets reached (20)"],
                        status: "warning"
                    },
                        setTimeout(function() {
                            e.snackBar = {
                                show: !1,
                                messages: [""]
                            }
                        }, 5e3);
                else {
                    var i = {
                        parent_match_id: a.parent_match_id,
                        game_description: a.game_description,
                        bet_pick: a.bet_pick,
                        sub_type_id: a.sub_type_id,
                        market_name: a.market,
                        odd_key: a.odd_key,
                        odd_value: a.odd_value,
                        special_bet_value: a.special_bet_value,
                        bet_type: a.bet_type,
                        home_team: a.home_team,
                        away_team: a.away_team
                    };
                    d.default.session.betslip.add(i),
                        e.jengabetBetSlip.push(a),
                        localStorage.setItem("jengabetBetSlip", JSON.stringify(e.jengabetBetSlip))
                }
            },
            removeFromBetslip: function t(e, a) {
                var i = {
                    profile_id: a.profile_id,
                    app_name: a.app_name,
                    parent_match_id: a.parent_match_id,
                    game_description: a.game_description,
                    bet_pick: a.bet_pick,
                    sub_type_id: a.sub_type_id,
                    market_name: a.market,
                    odd_key: a.odd_key,
                    odd_value: a.odd_value,
                    special_bet_value: a.special_bet_value,
                    bet_type: a.bet_type,
                    home_team: a.home_team,
                    away_team: a.away_team
                };
                d.default.session.betslip.remove(i),
                    e.betSlip = e.betSlip.filter(function(t) {
                        return t.matchId != a.matchId
                    }),
                    localStorage.setItem("betSlip", JSON.stringify(e.betSlip))
            },
            removeFromJengabetBetslip: function t(e, a) {
                var i = {
                    profile_id: a.profile_id,
                    app_name: a.app_name,
                    parent_match_id: a.parent_match_id,
                    game_description: a.game_description,
                    bet_pick: a.bet_pick,
                    sub_type_id: a.sub_type_id,
                    market_name: a.market,
                    odd_key: a.odd_key,
                    odd_value: a.odd_value,
                    special_bet_value: a.special_bet_value,
                    bet_type: a.bet_type,
                    home_team: a.home_team,
                    away_team: a.away_team
                };
                d.default.session.betslip.remove(i),
                    e.jengabetBetSlip = e.jengabetBetSlip.filter(function(t) {
                        return t.matchId != a.matchId || t.market != a.market
                    }),
                    localStorage.setItem("jengabetBetSlip", JSON.stringify(e.jengabetBetSlip))
            },
            removeMarketFromJengabetBetslip: function t(e, a) {
                var i = {
                    profile_id: a.profile_id,
                    app_name: a.app_name,
                    parent_match_id: a.parent_match_id,
                    game_description: a.game_description,
                    bet_pick: a.bet_pick,
                    sub_type_id: a.sub_type_id,
                    market_name: a.market,
                    odd_key: a.odd_key,
                    odd_value: a.odd_value,
                    special_bet_value: a.special_bet_value,
                    bet_type: a.bet_type,
                    home_team: a.home_team,
                    away_team: a.away_team
                };
                d.default.session.betslip.remove(i),
                    e.jengabetBetSlip = e.jengabetBetSlip.filter(function(t) {
                        return t.parent_match_id != a.parent_match_id || t.type != a.type
                    }),
                    localStorage.setItem("jengabetBetSlip", JSON.stringify(e.jengabetBetSlip))
            },
            replaceBetInBetslip: function t(e, a) {
                var i = e.betSlip.findIndex(function(t) {
                    return t.matchId == a.matchId
                });
                e.betSlip.splice(i, 1, a),
                    localStorage.setItem("betSlip", JSON.stringify(e.betSlip))
            },
            removeInvalidMatches: function t(e) {
                e.betSlip = e.betSlip.filter(function(t) {
                    return "STOPPED" != t.status && Number(t.odd.odd_value) && t.active
                }),
                    localStorage.setItem("betSlip", JSON.stringify(e.betSlip))
            },
            replaceBetslip: function t(e, a) {
                e.betSlip = a
            },
            clearBetslip: function t(e) {
                var a = [];
                e.betSlip.forEach(function(t) {
                    var e = {
                        parent_match_id: t.parent_match_id,
                        game_description: t.game_description,
                        bet_pick: t.bet_pick,
                        sub_type_id: t.sub_type_id,
                        market_name: t.market,
                        odd_key: t.odd_key,
                        odd_value: t.odd_value,
                        special_bet_value: t.special_bet_value,
                        bet_type: t.bet_type,
                        home_team: t.home_team,
                        away_team: t.away_team
                    };
                    a.push(e)
                }),
                    a = {
                        array_of_bets: a
                    },
                    d.default.session.betslip.clear(a),
                    e.betSlip = [],
                    localStorage.setItem("betSlip", JSON.stringify(e.betSlip))
            },
            clearJengabetBetslip: function t(e) {
                var a = [];
                e.jengabetBetSlip.forEach(function(t) {
                    var e = {
                        parent_match_id: t.parent_match_id,
                        game_description: t.game_description,
                        bet_pick: t.bet_pick,
                        sub_type_id: t.sub_type_id,
                        market_name: t.market,
                        odd_key: t.odd_key,
                        odd_value: t.odd_value,
                        special_bet_value: t.special_bet_value,
                        bet_type: t.bet_type,
                        home_team: t.home_team,
                        away_team: t.away_team
                    };
                    a.push(e)
                }),
                    a = {
                        array_of_bets: a
                    },
                    d.default.session.betslip.clear(a),
                    e.jengabetBetSlip = [],
                    localStorage.setItem("jengabetBetSlip", JSON.stringify(e.jengabetBetSlip))
            },
            GET_MATCHES: function t(e, a) {
                e.matches = a.data
            },
            getLiveMatches: function t(e, a) {
                e.context = a,
                    n.default.http.get(LIVE_URL + a + "/").then(function(t) {
                        e.liveMatches = t.body
                    }).catch(function(t) {})
            },
            addToJackpot: function t(e, a) {
                if (13 == e.jackpot.length)
                    return e.snackBar = {
                        show: !0,
                        messages: ["You can only select outcomes for a maximum of 13 matches"],
                        status: "warning"
                    },
                        void window.setTimeout(function() {
                            e.snackBar = {
                                show: !1,
                                messages: [""]
                            }
                        }, 3e3);
                e.jackpot.push(a),
                    sessionStorage.setItem("jackpot", JSON.stringify(e.jackpot))
            },
            removeFromJackpot: function t(e, a) {
                e.jackpot = e.jackpot.filter(function(t) {
                    return t.matchId != a.matchId
                }),
                    sessionStorage.setItem("jackpot", JSON.stringify(e.jackpot))
            },
            clearJackpot: function t(e, a) {
                e.jackpot = e.jackpot.filter(function(t) {
                    return t.type != a
                }),
                    sessionStorage.setItem("jackpot", JSON.stringify(e.jackpot))
            },
            addToFreeJackpot: function t(e, a) {
                e.freeJackpot.push(a)
            },
            removeFromFreeJackpot: function t(e, a) {
                e.freeJackpot = e.freeJackpot.filter(function(t) {
                    return t.matchId != a.matchId
                })
            },
            clearFreeJackpot: function t(e, a) {
                e.freeJackpot = e.freeJackpot.filter(function(t) {
                    return t.type != a
                })
            },
            toggleSnackBar: function t(e, a) {
                e.snackBar = a
            },
            toggleSearch: function t(e) {
                e.search.show ? e.search.show = !1 : e.search.show = !0
            },
            openSearch: function t(e) {
                e.search.show = !0
            },
            closeSearch: function t(e) {
                e.search.show = !1
            },
            logout: function t(e) {
                localStorage.removeItem("user"),
                    localStorage.removeItem("token"),
                    e.user = {},
                    e.snackBar = {
                        show: !0,
                        messages: ["Your session has expired. Please log back in"],
                        status: "info"
                    },
                    setTimeout(function() {
                        e.snackBar = {
                            show: !1,
                            messages: [""]
                        }
                    }, 5e3)
            }
        },
        actions: {
            getMatches: function t(e, a) {
                var i = e.commit;
                return new Promise(function(t, e) {
                        n.default.http.get(BASE_URL + "uo/matches?page=" + a.page + "&limit=" + a.limit + "&keyword=" + a.keyword + "&tab=" + a.tab + "&sub_type_id=" + a.sub_type_id + "&tag_id=" + a.tag_id + "&sport_id=" + a.sport_id).then(function(e) {
                            i(f, {
                                data: e.body.data,
                                meta: e.body.meta
                            }),
                                t({
                                    data: e.body.data,
                                    meta: e.body.meta
                                })
                        }, function(e) {
                            t(e)
                        })
                    }
                )
            },
            getCompetitionMatches: function t(e, a) {
                var i = e.commit;
                return new Promise(function(t, e) {
                        n.default.http.post(BASE_URL + "sports/competition", a).then(function(e) {
                            i(f, e.body.data),
                                t(e.body.data)
                        }, function(e) {
                            t(e)
                        })
                    }
                )
            }
        },
        getters: {
            numberOfLiveMatches: function t(e) {
                return e.numberOfLiveMatches
            },
            betSlipCount: function t(e) {
                return e.betSlip.length
            },
            jengabetBetSlipCount: function t(e) {
                return e.jengabetBetSlip.length
            },
            betSlip: function t(e) {
                return e.betSlip
            },
            stake: function t(e) {
                return e.stake
            },
            jengabetBetSlip: function t(e) {
                return e.jengabetBetSlip
            },
            selectedBets: function t(e) {
                return e.betSlip.map(function(t) {
                    return t.matchId
                })
            },
            jackpotLength: function t(e) {
                return e.jackpot.filter(function(t) {
                    return 12 == t.type
                }).length
            },
            bingwaLength: function t(e) {
                return e.jackpot.filter(function(t) {
                    return 5 == t.type
                }).length
            },
            snackBar: function t(e) {
                return e.snackBar
            },
            user: function t(e) {
                return e.user
            },
            balance: function t(e) {
                return e.balance
            },
            jackpot: function t(e) {
                return e.jackpot
            }
        }
    });
    e.default = m
}
    , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(197)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = x(a(607)), o, s = x(a(646)), r, l = x(a(362)), c, d = x(a(368)), u, p = x(a(18)), f, m = x(a(681)), h, g = x(a(684)), b, _ = x(a(370)), v, w = x(a(694));
        function x(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        var k, y = a(311).initAddToHomeScreen;
        e.default = {
            name: "app",
            components: {
                Top: n.default,
                Bottom: s.default,
                SnackBar: d.default,
                SearchModal: m.default,
                MiniBetslip: g.default,
                Fab: _.default
            },
            data: function t() {
                return {
                    msg: "Main View",
                    show: {
                        slideNav: !0,
                        header: !0,
                        footer: !0
                    },
                    search: !1,
                    online: window.navigator.onLine,
                    liveMatches: [],
                    matchesInBetslip: [],
                    isNotLogin: !1,
                    btag: null,
                    affiliateData: null,
                    affiliateMatchData: null,
                    affiliateMatchMeta: null,
                    show_income_access: !1,
                    show_banner: !1,
                    loginMessageTitle: "",
                    loginMessageContent: ""
                }
            },
            beforeMount: function t() {
                var e = this;
                window.showIncomeAccess = this.showIncomeAccess,
                    window.addToHomeScreen = this.addToHomeScreen,
                this.getParameterByName("utm_source") && sessionStorage.setItem("affiliate", this.getParameterByName("utm_source")),
                this.getParameterByName("affiliate_id") && sessionStorage.setItem("affiliate", this.getParameterByName("affiliate_id")),
                this.getParameterByName("affiliate") && sessionStorage.setItem("affiliate", this.getParameterByName("affiliate")),
                this.getParameterByName("btag") && localStorage.setItem("btag", this.getParameterByName("btag")),
                    this.track(),
                    this.updateLiveMatches(),
                    setInterval(function() {
                        e.updateLiveMatches()
                    }, 1e4)
            },
            mounted: function t() {
                var e = this
                    , a = this;
                "Opera_Reactivation" == this.getParameterByName("utm_source") && this.claimBonus(!0);
                var i, n = (new w.default).getResult();
                localStorage.user_agent = JSON.stringify(n);
                var o = window.document.querySelector("footer .nav")
                    , s = window.document.querySelector("header")
                    , r = window.document.querySelector("header .top-search .top")
                    , l = window.document.querySelector("header .top-search .search")
                    , c = window.document.querySelector("header .nav");
                if ("#/" == window.location.hash)
                    var d = window.document.querySelector(".betslip-fab")
                        , u = window.document.querySelector("main .filter")
                        , p = window.document.querySelector("main .slider");
                ("#/" == window.location.hash || window.location.hash.indexOf("live")) && (a.show.header = !0,
                    a.show.footer = !0,
                    a.show.slideNav = !0),
                window.location.hash.indexOf("betslip") > 0 && (a.show.header = !0,
                    a.show.footer = !0,
                    a.show.slideNav = !1),
                window.location.hash.indexOf("markets") > 0 && (a.show.header = !0,
                    a.show.footer = !0,
                    a.show.slideNav = !1),
                window.location.hash.indexOf("login") > 0 && (a.show.header = !1,
                    a.show.footer = !1),
                window.location.hash.indexOf("forgotpassword") > 0 && (a.show.header = !1,
                    a.show.footer = !1),
                window.location.hash.indexOf("signup") > 0 && (a.show.header = !1,
                    a.show.footer = !1),
                window.location.hash.indexOf("profile") > 0 && (a.show.header = !0,
                    a.show.footer = !0,
                    a.show.slideNav = !1),
                window.location.hash.indexOf("jackpot") > 0 && (a.show.header = !0,
                    a.show.footer = !0),
                window.location.hash.indexOf("virtual") > 0 && (a.show.header = !1,
                    a.show.footer = !0),
                window.location.hash.indexOf("mybets") > 0 && (a.show.header = !0,
                    a.show.footer = !0,
                    a.show.slideNav = !1),
                window.location.hash.indexOf("live/stats") > 0 && (a.show.header = !1,
                    a.show.footer = !1,
                    a.show.slideNav = !1),
                    window.addEventListener("online", function() {
                        e.online = !0
                    }, !1),
                    window.addEventListener("offline", function() {
                        e.online = !1
                    }, !1)
            },
            watch: {
                $route: function t() {
                    var e = this;
                    p.default.commit("closeSearch"),
                    ("#/" == window.location.hash || window.location.hash.indexOf("live")) && (e.show.header = !0,
                        e.show.footer = !0,
                        e.show.slideNav = !0),
                    window.location.hash.indexOf("sports") > 0 && (e.show.header = !0,
                        e.show.footer = !0,
                        e.show.slideNav = !0),
                    window.location.hash.indexOf("sport") > 0 && (e.show.header = !0,
                        e.show.footer = !0,
                        e.show.slideNav = !0),
                    window.location.hash.indexOf("betslip") > 0 && (e.show.header = !0,
                        e.show.footer = !0,
                        e.show.slideNav = !1),
                    window.location.hash.indexOf("markets") > 0 && (e.show.header = !0,
                        e.show.footer = !0,
                        e.show.slideNav = !1),
                    window.location.hash.indexOf("virtual") > 0 && (e.show.header = !1,
                        e.show.footer = !0),
                    window.location.hash.indexOf("forgotpassword") > 0 && (e.show.header = !1,
                        e.show.footer = !1),
                    window.location.hash.indexOf("signup") > 0 && (e.show.header = !1,
                        e.show.footer = !1),
                    window.location.hash.indexOf("profile") > 0 && (e.show.header = !0,
                        e.show.footer = !0,
                        e.show.slideNav = !1),
                    window.location.hash.indexOf("jackpot") > 0 && (e.show.header = !0,
                        e.show.footer = !0),
                    window.location.hash.indexOf("login") > 0 && (e.show.header = !1,
                        e.show.footer = !1),
                    window.location.hash.indexOf("mybets") > 0 && (e.show.header = !0,
                        e.show.footer = !0,
                        e.show.slideNav = !1),
                    window.location.hash.indexOf("live") < 0 && (window.clearInterval(window.localStorage.getItem("liveUpdateID")),
                        window.clearInterval(window.localStorage.getItem("liveMatchUpdateID"))),
                        e.isNotLogin = location.hash.indexOf("login") < 0 && location.hash.indexOf("profile") < 0
                },
                online: function t() {
                    this.online || p.default.commit("toggleSnackBar", {
                        show: !0,
                        messages: ["You are offline. Please refresh your matches once you get back online"],
                        status: "error"
                    }),
                    this.online && p.default.commit("toggleSnackBar", {
                        show: !1,
                        messages: [""]
                    })
                }
            },
            computed: {
                userHasLiveMatchesInBetslip: function t() {
                    return p.default.state.betSlip.filter(function(t) {
                        return t.live
                    }).length > 0
                },
                user: {
                    get: function t() {
                        return this.$store.getters.user
                    },
                    set: function t(e) {
                        this.$store.commit("setUser", e)
                    }
                },
                showMiniBetslip: function t() {
                    return 1 == this.$route.meta.miniBetslipViewable
                },
                showBetslipFab: function t() {
                    return 1 == this.$route.meta.betslipFabViewable
                },
                amount: function t() {
                    return this.$store.getters.stake
                }
            },
            created: function t() {
                var e = this;
                window.forcePlaceBet = e.forcePlaceBet,
                    y(this.addToHomeScreen, this.addToHomeScreen, this.addToHomeScreen),
                    this.fetchRouteParams()
            },
            methods: {
                forcePlaceBet: function t() {
                    var e = this;
                    return new Promise(function(t, a) {
                            e.acceptOddChanges(),
                                e.removeInvalidMatches();
                            var i = new Date;
                            if (e.$store.getters.betSlip.find(function(t) {
                                return t.live
                            })) {
                                if (i.getUTCHours() > 22 && i.getUTCHours() < 5 && e.amount > 1e5)
                                    return e.$store.commit("toggleSnackBar", {
                                        show: !0,
                                        messages: ["Maximum live bets amount is 100000KES."],
                                        status: "warning"
                                    }),
                                        setTimeout(function() {
                                            e.$store.commit("toggleSnackBar", {
                                                show: !1,
                                                messages: [""]
                                            })
                                        }, 5e3),
                                        void a("Maximum live bets amount is 100000KES.");
                                if (i.getUTCHours() > 5 && i.getUTCHours() < 22 && e.amount > 1e5)
                                    return p.default.commit("toggleSnackBar", {
                                        show: !0,
                                        messages: ["Maximum live bets amount is 100000KES."],
                                        status: "warning"
                                    }),
                                        setTimeout(function() {
                                            p.default.commit("toggleSnackBar", {
                                                show: !1,
                                                messages: [""]
                                            })
                                        }, 5e3),
                                        void a("Maximum live bets amount is 100000KES.")
                            }
                            if (!JSON.parse(window.localStorage.getItem("authenticated")))
                                return e.$store.commit("toggleSnackBar", {
                                    show: !0,
                                    messages: ["Please login first"],
                                    status: "warning"
                                }),
                                    setTimeout(function() {
                                        e.$store.commit("toggleSnackBar", {
                                            show: !1,
                                            messages: [""]
                                        })
                                    }, 3e3),
                                    e.$router.push("/login?next=" + e.$route.fullPath),
                                    void a("Not authenticated");
                            if (e.amount < 1)
                                return e.$store.commit("toggleSnackBar", {
                                    show: !0,
                                    messages: ["Minimum bet amount is 1 bob."],
                                    status: "info"
                                }),
                                    setTimeout(function() {
                                        e.$store.commit("toggleSnackBar", {
                                            show: !1,
                                            messages: [""]
                                        })
                                    }, 5e3),
                                    void a("Stake less than 1.");
                            var n = [];
                            e.$store.getters.betSlip.forEach(function(t) {
                                var e = {
                                    parent_match_id: t.parent_match_id,
                                    game_description: t.game_description,
                                    bet_pick: t.bet_pick,
                                    sub_type_id: t.sub_type_id,
                                    market_name: t.market,
                                    odd_key: t.odd_key,
                                    odd_value: t.odd_value,
                                    outcome_id: t.odd.outcome_id,
                                    special_bet_value: t.special_bet_value,
                                    bet_type: t.bet_type,
                                    home_team: t.home_team,
                                    away_team: t.away_team
                                };
                                n.push(e)
                            });
                            var o = e.$store.getters.user.balance, s = e.$store.getters.user.bonus, r = e.$store.getters.betSlip.map(function(t) {
                                return {
                                    sub_type_id: t.type,
                                    bet_pick: t.odd.odd_key,
                                    odd_value: t.odd.odd_value,
                                    outcome_id: t.odd.outcome_id,
                                    special_bet_value: t.odd.special_bet_value ? t.odd.special_bet_value : "",
                                    parent_match_id: t.odd.parent_match_id,
                                    bet_type: t.live ? 8 : 7
                                }
                            }), l, c = e.$store.getters.betSlip.map(function(t) {
                                return Number(t.odd.odd_value)
                            }).reduce(function(t, e) {
                                return t * e
                            }, 1), d = {
                                profile_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : "",
                                stake: e.amount.toString(),
                                total_odd: c.toString(),
                                src: "MOBILE_WEB",
                                betslip: r,
                                token: window.localStorage.getItem("token") ? JSON.parse(window.localStorage.getItem("token")) : "",
                                user_agent: localStorage.user_agent ? localStorage.user_agent : null,
                                affiliate: localStorage.affiliate ? localStorage.affiliate : null
                            };
                            e.$cookie.get("btag") && (d.affiliate_id = 19,
                                d.affiliate_tag = e.$cookie.get("btag"));
                            var u = e.$store.getters.betSlip.findIndex(function(t) {
                                return t.live
                            }) > -1, f;
                            setTimeout(function() {
                                e.$axios.post(LIVE_URL + "uo/placebet", d).then(function(a) {
                                    var i = {
                                        bet_id: a.data.bet_id,
                                        stake: e.amount,
                                        possible_win: e.possibleWinnings,
                                        total_odds: e.totalOdds,
                                        number_of_games: e.numberOfBets,
                                        wallet_balance_before: o,
                                        wallet_balance_after: a.data.user.balance,
                                        wallet_bonus_before: s,
                                        wallet_bonus_after: a.data.user.bonus,
                                        array_of_matches: n,
                                        first_bet: a.data.first_bet
                                    };
                                    e.$track.session.betslip.place(i),
                                        e.$store.commit("clearBetslip"),
                                        e.$store.commit("toggleSnackBar", {
                                            show: !0,
                                            messages: [a.data.message],
                                            status: "success"
                                        }),
                                        setTimeout(function() {
                                            e.$store.commit("toggleSnackBar", {
                                                show: !1,
                                                messages: [""]
                                            })
                                        }, 1e4),
                                        t(!0)
                                }).catch(function(t) {
                                    if (t.response) {
                                        if (401 == t.response.status)
                                            return window.localStorage.setItem("authenticated", !1),
                                                window.localStorage.removeItem("user"),
                                                window.localStorage.removeItem("token"),
                                                e.$store.commit("toggleSnackBar", {
                                                    show: !0,
                                                    messages: ["Your session has expired. Please login again"],
                                                    status: "error"
                                                }),
                                                setTimeout(function() {
                                                    e.$store.commit("toggleSnackBar", {
                                                        show: !1,
                                                        messages: [""]
                                                    })
                                                }, 5e3),
                                                void e.$router.replace("/login?next=" + e.$route.fullPath);
                                        e.$store.commit("toggleSnackBar", {
                                            show: !0,
                                            messages: [t.response.data.message],
                                            status: "error"
                                        }),
                                            setTimeout(function() {
                                                e.$store.commit("toggleSnackBar", {
                                                    show: !1,
                                                    messages: [""]
                                                })
                                            }, 1e4)
                                    }
                                    a(t)
                                })
                            }, u ? 5e3 : 0)
                        }
                    )
                },
                randomMessageCheck: function t() {
                    var e = this;
                    !this.$cookie.get("shown_today") && localStorage.authenticated && (e.createCookie("shown_today", 1, "/"),
                        e.getRandomMessage().then(function(t) {
                            e.loginMessageTitle = t.loginMessageTitle,
                                e.loginMessageContent = t.loginMessageContent
                        }).catch(function(t) {}))
                },
                removeInvalidMatches: function t() {
                    this.$store.commit("removeInvalidMatches")
                },
                acceptOddChanges: function t() {
                    this.$store.getters.betSlip.map(function(t) {
                        return t.changed = !1
                    })
                },
                getRandomMessage: function t() {
                    var e = this;
                    return new Promise(function(t, a) {
                            e.loading = !0;
                            var i = {
                                token: window.localStorage.getItem("token") ? JSON.parse(window.localStorage.getItem("token")) : ""
                            };
                            e.$http.post(BASE_URL + "randomMessage", i).then(function(e) {
                                t({
                                    loginMessageTitle: e.data.message_title,
                                    loginMessageContent: e.data.display
                                })
                            }).catch(function(t) {
                                a(t)
                            })
                        }
                    )
                },
                createCookie: function t(e, a, i) {
                    var n = "", o = new Date, s;
                    n = "; expires=" + new Date(o.getFullYear(),o.getMonth(),o.getDate(),23,59,59).toGMTString(),
                    i || (i = "/"),
                        document.cookie = e + "=" + a + n + "; path=" + i
                },
                showLoginMessage: function t(e, a) {
                    var i = this;
                    this.loginMessageTitle = e,
                        this.loginMessageContent = a,
                        this.$nextTick(function() {
                            i.show_banner = !0
                        })
                },
                initBanner: function t() {
                    var e = this;
                    sessionStorage.already_seen_banner_count >= 2 ? this.show_banner = !1 : (this.show_banner = !0,
                        setTimeout(function() {
                            return e.closeBanner()
                        }, 1e4))
                },
                closeBanner: function t() {
                    var e = sessionStorage.already_seen_banner_count ? Number(sessionStorage.already_seen_banner_count) : 0;
                    sessionStorage.already_seen_banner_count = e + 1,
                        this.show_banner = !1
                },
                toBetslip: function t() {
                    this.$router.push("/betslip")
                },
                showIncomeAccess: function t() {
                    this.show_income_access = !0
                },
                fetchRouteParams: function t() {
                    var e = this, a;
                    if ("instantbet" == utils.getParameterByName("action")) {
                        var i = {
                            parent_match_id: utils.getParameterByName("parent_match_id"),
                            sub_type_id: utils.getParameterByName("sub_type_id"),
                            odd_key: utils.getParameterByName("odd_key"),
                            odd_value: utils.getParameterByName("odd_value"),
                            special_bet_value: utils.getParameterByName("special_bet_value"),
                            stake: utils.getParameterByName("stake")
                        };
                        e.affiliateData = i,
                            this.getAffiliateMatchDetails()
                    }
                    "instantbet" === this.$route.query.action && (e.affiliateData = this.$route.query,
                        this.getAffiliateMatchDetails()),
                    this.$route.query.btag && (this.btag = this.$route.query.btag || null,
                        this.$cookie.set("btag", this.btag, 365)),
                    utils.getParameterByName("btag") && (this.btag = utils.getParameterByName("btag") || null,
                        this.$cookie.set("btag", this.btag, 365))
                },
                getAffiliateMatchDetails: function t() {
                    var e = this;
                    this.$http.get(BASE_URL + "/match?parent_match_id=" + this.affiliateData.parent_match_id).then(function(t) {
                        if (e.affiliateMatchData = t.data.data,
                            e.affiliateMatchMeta = t.data.meta,
                        null == e.affiliateMatchMeta)
                            return p.default.commit("toggleSnackBar", {
                                show: !0,
                                messages: ["Match add unsuccessful, expired match"],
                                status: "error"
                            }),
                                void setTimeout(function() {
                                    p.default.commit("toggleSnackBar", {
                                        show: !1,
                                        messages: [""]
                                    })
                                }, 3e3);
                        e.addToBetslip()
                    }).catch(function(t) {})
                },
                addToBetslip: function t() {
                    var e = this
                        , a = e.affiliateMatchData.filter(function(t) {
                        return t.sub_type_id == e.affiliateData.sub_type_id
                    });
                    if (0 == a.length)
                        return p.default.commit("toggleSnackBar", {
                            show: !0,
                            messages: ["Match add unsuccessful, check the parameters"],
                            status: "error"
                        }),
                            void setTimeout(function() {
                                p.default.commit("toggleSnackBar", {
                                    show: !1,
                                    messages: [""]
                                })
                            }, 3e3);
                    var i = (a = a[0]).odds.filter(function(t) {
                        return t.odd_key == e.affiliateData.odd_key && ("" == e.affiliateData.special_bet_value ? "" == t.special_bet_value || null == t.special_bet_value : e.affiliateData.special_bet_value == t.special_bet_value)
                    });
                    if (0 == i.length)
                        return p.default.commit("toggleSnackBar", {
                            show: !0,
                            messages: ["Match add unsuccessful, check the parameters"],
                            status: "error"
                        }),
                            void setTimeout(function() {
                                p.default.commit("toggleSnackBar", {
                                    show: !1,
                                    messages: [""]
                                })
                            }, 3e3);
                    var n = {
                        display: (i = i[0]).display,
                        odd_key: i.odd_key,
                        odd_value: i.odd_value,
                        special_bet_value: i.special_bet_value || "",
                        parent_match_id: e.affiliateData.parent_match_id
                    };
                    e.odd = n;
                    var o = {
                        matchId: e.affiliateMatchMeta.match_id,
                        odd: n,
                        type: e.affiliateData.sub_type_id,
                        market: a.name,
                        display: e.affiliateMatchMeta.home_team + " vs " + e.affiliateMatchMeta.away_team,
                        startTime: e.affiliateMatchMeta.start_time,
                        live: !1,
                        active: !0,
                        parent_match_id: n.parent_match_id,
                        game_description: e.affiliateMatchMeta.home_team + " vs. " + e.affiliateMatchMeta.away_team,
                        bet_pick: n.odd_key,
                        sub_type_id: e.affiliateData.sub_type_id,
                        market_name: a.name,
                        odd_key: n.odd_key,
                        odd_value: n.odd_value,
                        special_bet_value: n.specialBetValue || "",
                        bet_type: "prebet",
                        home_team: e.affiliateMatchMeta.home_team,
                        away_team: e.affiliateMatchMeta.away_team
                    };
                    this.$store.state.betSlip.find(function(t) {
                        return t.matchId == e.affiliateMatchMeta.match_id && t.odd == n
                    }) ? this.removeFromBetslip(o) : (this.$store.state.betSlip.find(function(t) {
                        return t.matchId == e.affiliateMatchMeta.match_id
                    }) && this.removeFromBetslip(o),
                        this.$store.commit("updateStake", parseFloat(e.affiliateData.stake)),
                        this.$store.commit("addToBetslip", o),
                        p.default.commit("toggleSnackBar", {
                            show: !0,
                            messages: ["Bet successfully added to betslip"],
                            status: "success"
                        }),
                        setTimeout(function() {
                            p.default.commit("toggleSnackBar", {
                                show: !1,
                                messages: [""]
                            })
                        }, 5e3),
                        this.toBetslip())
                },
                removeFromBetslip: function t(e) {
                    this.selected = "";
                    var a = this;
                    this.$store.commit("removeFromBetslip", {
                        matchId: e.matchId,
                        odd: e.odd,
                        type: e.type,
                        market: e.market,
                        display: e.display,
                        startTime: e.startTime,
                        live: e.live,
                        active: e.active
                    })
                },
                checkVersion: function t() {
                    try {
                        var e = this.$version
                            , a = null
                    } catch (t) {
                        return
                    }
                    this.$axios.get("http://35.195.34.31/utils/checkversion/").then(function(t) {
                        currentVersion = t.data.version;
                        try {
                            parseFloat(currentVersion) > parseFloat(appVersion) && window.reload()
                        } catch (t) {}
                    }).catch(function(t) {})
                },
                addToHomeScreen: function t() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0]
                        , a = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                    this.$refs.addToHomeScreen.style.display = e ? "" : "none"
                },
                claimBonus: function t() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                    this.$refs.claimBonus.style.display = e ? "" : "none"
                },
                updateLiveMatches: function t() {
                    var e = this
                        , a = e.$store.state.betSlip.filter(function(t) {
                        return t.live
                    });
                    a.length > 0 && a.forEach(function(t) {
                        var a = [];
                        e.getMarkets(t.matchId, t.type).then(function(a) {
                            if (a.data)
                                if (0 != a.data.length) {
                                    if (a.meta) {
                                        var i = a.data[0].odds;
                                        for (var n in i)
                                            i[n].odd_key.toLowerCase() == t.odd.odd_key.toLowerCase() && (i[n].special_bet_value ? i[n].special_bet_value == t.odd.special_bet_value && e.informBetslipThatMarketOddsHaveChanged({
                                                matchId: a.meta.match_id,
                                                type: a.data[0].sub_type_id,
                                                newOdd: i[n].odd_value,
                                                specialBetValue: i[n].special_bet_value,
                                                oddKey: i[n].odd_key
                                            }, {
                                                bet_status: a.meta.bet_status,
                                                active: "1" == i[n].odd_active && "1" == i[n].market_active
                                            }) : e.informBetslipThatMarketOddsHaveChanged({
                                                matchId: a.meta.match_id,
                                                type: a.data[0].sub_type_id,
                                                newOdd: i[n].odd_value,
                                                specialBetValue: i[n].special_bet_value,
                                                oddKey: i[n].odd_key
                                            }, {
                                                bet_status: a.meta.bet_status,
                                                active: "1" == i[n].odd_active && "1" == i[n].market_active
                                            }))
                                    }
                                } else
                                    e.informBetslipThatMarketOddsHaveChanged({
                                        matchId: t.matchId,
                                        type: t.type,
                                        newOdd: null,
                                        specialBetValue: t.odd.special_bet_value,
                                        oddKey: t.odd.odd_key
                                    }, {
                                        bet_status: "STOPPED",
                                        active: !1
                                    });
                            else
                                e.informBetslipThatMarketOddsHaveChanged({
                                    matchId: t.matchId,
                                    type: t.type,
                                    newOdd: null,
                                    specialBetValue: t.odd.special_bet_value,
                                    oddKey: t.odd.odd_key
                                }, {
                                    bet_status: "STOPPED",
                                    active: !1
                                })
                        }).catch(function(t) {})
                    })
                },
                getLiveGames: function t() {
                    var e = this;
                    return new Promise(function(t, a) {
                            e.$axios.get(LIVE_URL + "uo/matches?limit=100&page=1&keyword=").then(function(a) {
                                e.liveMatches = a.data.data,
                                    t(a.data.data)
                            }).catch(function(t) {
                                a(t)
                            })
                        }
                    )
                },
                getMarkets: function t(e, a) {
                    var i = this;
                    return new Promise(function(t, n) {
                            i.$axios.get(LIVE_URL + "uo/match?id=" + e + "&type=" + a).then(function(e) {
                                t(e.data)
                            }).catch(function(t) {
                                n(t)
                            })
                        }
                    )
                },
                informBetslipThatOddsHaveChanged: function t(e, a) {
                    var i = this
                        , n = this;
                    this.$store.state.betSlip.filter(function(t) {
                        return t.matchId == e.matchId && t.type == e.type
                    }).map(function(t) {
                        return {
                            display: t.display,
                            live: t.live,
                            market: t.market,
                            matchId: t.matchId,
                            odd: {
                                display: t.odd.display,
                                odd_key: t.odd.odd_key,
                                odd_value: t.odd.odd_key != e.type || t.odd.special_bet_value ? t.odd.odd_value : e.newOdd,
                                parent_match_id: t.odd.parent_match_id,
                                special_bet_value: t.odd.special_bet_value,
                                parsed_special_bet_value: t.odd.parsed_special_bet_value,
                                outcome_id: t.odd.outcome_id
                            },
                            initialOdd: t.initialOdd,
                            startTime: t.startTime,
                            type: t.type,
                            changed: t.odd.odd_key == e.type && Number(e.newOdd) != Number(t.odd.odd_value) || t.changed,
                            status: a.bet_status,
                            active: "1" == a.odd_active,
                            parent_match_id: t.parent_match_id,
                            game_description: t.game_description,
                            bet_pick: t.bet_pick,
                            sub_type_id: t.sub_type_id,
                            market_name: t.market_name,
                            odd_key: t.odd_key,
                            odd_value: t.odd_value,
                            special_bet_value: t.special_bet_value,
                            bet_type: t.bet_type,
                            home_team: t.home_team,
                            away_team: t.away_team
                        }
                    }).forEach(function(t) {
                        i.$store.commit("replaceBetInBetslip", t)
                    })
                },
                informBetslipThatMarketOddsHaveChanged: function t(e, a) {
                    var i = this
                        , n = this;
                    this.$store.state.betSlip.filter(function(t) {
                        return t.matchId == e.matchId && t.type == e.type && t.odd.special_bet_value == e.specialBetValue && t.odd.odd_key.toLowerCase() == e.oddKey.toLowerCase()
                    }).map(function(t) {
                        return {
                            display: t.display,
                            live: t.live,
                            market: t.market,
                            matchId: t.matchId,
                            odd: {
                                display: t.odd.display,
                                odd_key: t.odd.odd_key,
                                odd_value: e.newOdd,
                                parent_match_id: t.odd.parent_match_id,
                                special_bet_value: t.odd.special_bet_value,
                                parsed_special_bet_value: t.odd.parsed_special_bet_value,
                                outcome_id: t.odd.outcome_id
                            },
                            initialOdd: t.initialOdd,
                            startTime: t.startTime,
                            type: t.type,
                            changed: Number(t.type) == Number(e.type) && Number(e.newOdd) != Number(t.odd.odd_value) || t.changed,
                            status: a.bet_status,
                            active: a.active,
                            profile_id: t.profile_id,
                            app_name: t.app_name,
                            parent_match_id: t.parent_match_id,
                            game_description: t.game_description,
                            bet_pick: t.bet_pick,
                            sub_type_id: t.sub_type_id,
                            market_name: t.market_name,
                            odd_key: t.odd_key,
                            odd_value: t.odd_value,
                            special_bet_value: t.special_bet_value,
                            bet_type: t.bet_type,
                            home_team: t.home_team,
                            away_team: t.away_team
                        }
                    }).forEach(function(t) {
                        i.$store.commit("replaceBetInBetslip", t)
                    })
                },
                getParameterByName: function t(e, a) {
                    a = window.location.href,
                        e = e.replace(/[\[\]]/g, "\\$&");
                    var i, n = new RegExp("[?&]" + e + "(=([^&#]*)|&|#|$)").exec(a);
                    return n ? n[2] ? decodeURIComponent(n[2].replace(/\+/g, " ")) : "" : null
                },
                track: function t() {
                    var e = !!localStorage.authenticated && JSON.parse(localStorage.authenticated)
                        , a = localStorage.user ? JSON.parse(localStorage.user) : null
                }
            }
        }
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(199)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = r(a(18)), o, s = r(a(608));
        function r(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "Top",
            components: {
                AndroidAppBanner: s.default
            },
            data: function t() {
                return {
                    option: "",
                    search: !1,
                    sports: [],
                    balanceSet: !1,
                    numberOfLiveMatches: 0,
                    numberOfBetBuilderMatches: 0,
                    registerText: "Register",
                    loggedIn: !1
                }
            },
            props: ["show"],
            computed: {
                betSlipCount: function t() {
                    return n.default.getters.betSlipCount + n.default.getters.jengabetBetSlipCount
                },
                showSearch: function t() {
                    return n.default.state.search.show
                },
                balance: function t() {
                    return n.default.getters.balance
                }
            },
            created: function t() {
                this.getSports()
            },
            beforeMount: function t() {
                var e = this;
                this.loggedIn = JSON.parse(window.localStorage.getItem("authenticated"));
                var a = localStorage.token ? JSON.parse(localStorage.token) : null;
                a && this.$axios.post(BASE_URL + "balance", {
                    token: a
                }).then(function(t) {
                    n.default.commit("setBalance", t.data.data.balance),
                        e.bonus = t.data.data.bonus,
                        e.points = t.data.data.points,
                        e.balanceSet = !0
                }).catch(function(t) {}),
                    this.getNumberOfLiveMatches(),
                    this.getNumberOfBetBuilderMatches(),
                    setInterval(function() {
                        e.getNumberOfLiveMatches(),
                            e.getNumberOfBetBuilderMatches()
                    }, 1e4)
            },
            mounted: function t() {
                "#/" === window.location.hash && (this.option = "football")
            },
            watch: {
                $route: function t() {
                    this.loggedIn = JSON.parse(window.localStorage.getItem("authenticated")),
                    "#/" === window.location.hash && (this.option = "football")
                }
            },
            methods: {
                getSportUrl: function t(e) {
                    return "/sports/" + e.sport_name.toLowerCase().split(" ").join("+") + "-" + e.sport_id
                },
                goToApp: function t() {
                    fbq("trackCustom", "clickGoToAppFromNav"),
                        this.$refs.appLink.click()
                },
                toggleRegisterText: function t() {
                    this.registerText = "Register" == this.registerText ? "Ksh. 25 Free!" : "Register"
                },
                getNumberOfLiveMatches: function t() {
                    var e = this
                        , a = this;
                    this.$axios.get(LIVE_URL + "uo/matches").then(function(t) {
                        t.data && (e.numberOfLiveMatches = t.data.meta.total,
                            a.$store.commit("setNumberOfLiveMatches", t.data.meta.total))
                    }).catch(function(t) {})
                },
                getNumberOfBetBuilderMatches: function t() {
                    var e = this;
                    this.$axios.get(BASE_URL + "betbuilder/matches?limit=1000&page=1").then(function(t) {
                        e.numberOfBetBuilderMatches = t.data.matches ? t.data.matches.length : 0
                    }).catch(function(t) {})
                },
                goTo: function t(e, a) {
                    this.option = a,
                    "/live" == e && fbq("trackCustom", "clickLiveTop"),
                    e.indexOf("betslip") > -1 && fbq("trackCustom", "clickBetslipTop"),
                        this.$router.push(e)
                },
                goToLite: function t() {
                    fbq("trackCustom", "clickGoToLite"),
                        window.location = "https://www.betika.com/lite/"
                },
                toggleSearch: function t() {
                    n.default.commit("toggleSearch")
                },
                getSports: function t() {
                    var e = this;
                    this.$axios.get(BASE_URL + "uo/sports?limit=50").then(function(t) {
                        e.sports = t.data.data
                    }).catch(function(t) {})
                },
                getImage: function t(e) {
                    try {
                        return a(612)("./" + e + ".svg")
                    } catch (t) {
                        return a(361)
                    }
                }
            }
        }
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(201)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }),
            e.default = {
                name: "android-app-banner",
                data: function t() {
                    return {
                        showAndroidBanner: !1,
                        showConfirm: !1
                    }
                },
                beforeMount: function t() {
                    this.initAndroidBanner()
                },
                methods: {
                    initAndroidBanner: function t() {
                        var e;
                        localStorage.hasOwnProperty("showAndroidBanner") || (localStorage.showAndroidBanner = JSON.stringify(!0)),
                        !!localStorage.hasOwnProperty("showAndroidBanner") && JSON.parse(localStorage.showAndroidBanner) && window.isAndroid && (this.showAndroidBanner = !0)
                    },
                    goToApp: function t() {
                        fbq("trackCustom", "clickGoToAppFromNotification"),
                            this.$refs.appLink.click()
                    },
                    close: function t() {
                        this.showConfirm = !0
                    },
                    confirm: function t(e) {
                        e ? (localStorage.showAndroidBanner = JSON.stringify(!0),
                            this.showAndroidBanner = !1,
                            document.querySelector("main").classList.remove("android-banner-active")) : (localStorage.showAndroidBanner = JSON.stringify(!1),
                            this.showAndroidBanner = !1,
                            document.querySelector("main").classList.remove("android-banner-active"))
                    }
                }
            }
    }
    , function(t, e, a) {
        var i = a(611);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("5bd29698", i, !0, {})
    }
    , function(t, e, a) {
        var i = a(645);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("5b7719d8", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(205)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = o(a(18));
        function o(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "Bottom",
            data: function t() {
                return {
                    msg: "Bottom",
                    authenticated: !1,
                    virtual: !1
                }
            },
            computed: {
                numberOfLiveMatches: function t() {
                    return this.$store.getters.numberOfLiveMatches
                },
                betslipCount: function t() {
                    return "shikisha" == this.$route.name ? this.$store.getters.jengabetBetSlipCount : this.$store.getters.betSlipCount
                },
                betslipUrl: function t() {
                    return "shikisha" == this.$route.name ? "/betslip?type=shikisha" : "/betslip"
                }
            },
            beforeMount: function t() {
                this.authenticated = JSON.parse(window.localStorage.getItem("authenticated"))
            },
            watch: {
                $route: function t() {
                    this.authenticated = JSON.parse(window.localStorage.getItem("authenticated"))
                }
            },
            methods: {
                toggleSearch: function t() {
                    n.default.commit("toggleSearch")
                },
                goTo: function t(e) {
                    "/live" == e && fbq("trackCustom", "clickLiveBottom"),
                    e.indexOf("betslip") > -1 && fbq("trackCustom", "clickBetslipBottom"),
                        this.$router.push(e)
                }
            }
        }
    }
    , function(t, e, a) {
        var i = a(648);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("e00fcd10", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(208)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = c(a(649)), o, s = c(a(256)), r, l = c(a(366));
        function c(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "Home",
            components: {
                PrebetMatches: n.default,
                Spinner: s.default,
                Slider: l.default
            }
        }
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(210)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = b(a(310)), o, s = b(a(654)), r, l = b(a(659)), c, d = b(a(363)), u, p = b(a(664)), f, m = b(a(52)), h, g = b(a(365));
        function b(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        m.default.use(g.default),
            e.default = {
                name: "prebet-matches",
                components: {
                    Match: n.default,
                    Filters: s.default,
                    MarketFilter: d.default,
                    SportFilter: l.default,
                    TagFilter: p.default
                },
                data: function t() {
                    return {
                        error: !1,
                        loading: !0,
                        loadingMore: !1,
                        filtering: !1,
                        page: 1,
                        limit: 10,
                        matches: [],
                        sports: [],
                        selectedSport: {
                            sport_id: "-1",
                            sport_name: "All Sports"
                        },
                        tabs: [{
                            id: "-1",
                            name: "Today's Highlights"
                        }, {
                            id: "-2",
                            name: "Upcoming"
                        }, {
                            id: "1",
                            name: "Tomorrow"
                        }, {
                            id: null,
                            name: ""
                        }, {
                            id: null,
                            name: ""
                        }, {
                            id: null,
                            name: ""
                        }],
                        selectedTab: {
                            id: "-1",
                            name: "Today's Highlights"
                        },
                        markets: [{
                            name: "1x2 / Winner",
                            sub_type_id: "1,186"
                        }],
                        selectedMarket: {
                            name: "1x2 / Winner",
                            sub_type_id: "1,186"
                        },
                        tags: [{
                            tag_id: "",
                            tag_title: "Top Leagues"
                        }],
                        selectedTag: {
                            tag_id: "",
                            tag_title: "Top Leagues"
                        },
                        showSportFilter: !1,
                        showMarketFilter: !1,
                        showTagFilter: !1,
                        showAllFilter: !1,
                        total: 10
                    }
                },
                created: function t() {
                    var e;
                    this.getMatches(),
                        dataLayer.push({
                            event: "pageMetaData",
                            app: "MOBILE_WEB",
                            page: {
                                type: "prematch",
                                title: "Pre-Match Matches",
                                path: "/",
                                category1: "",
                                category2: this.selectedSport.sport_name,
                                category3: this.selectedTag.tag_title,
                                environment: "production",
                                language: "EN",
                                currency: "KES"
                            },
                            user: {
                                loggedIn: !!this.$store.getters.user.id,
                                id: this.$store.getters.user.id || "",
                                mobile: this.$store.getters.user.mobile || ""
                            }
                        })
                },
                methods: {
                    truncate: function t(e) {
                        return e.length > 12 ? e.substring(0, 12) + "..." : e
                    },
                    closeSportFilter: function t() {
                        this.showSportFilter = !1
                    },
                    closeMarketFilter: function t() {
                        this.showMarketFilter = !1
                    },
                    closeTagFilter: function t() {
                        this.showTagFilter = !1
                    },
                    closeAllFilter: function t() {
                        this.showAllFilter = !1
                    },
                    filterSport: function t(e) {
                        var a = this;
                        a.selectedSport = e,
                            fbq("trackCustom", "changeSportOnFilter", {
                                sport: e.sport_name,
                                user_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : ""
                            }),
                            a.page = 1,
                            a.limit = 10,
                            a.filter()
                    },
                    filterMarket: function t(e) {
                        var a = this;
                        a.selectedMarket = e,
                            fbq("trackCustom", "changeMarketOnFilter", {
                                market: e.name,
                                user_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : ""
                            }),
                            a.filter()
                    },
                    filterTag: function t(e) {
                        var a = this;
                        a.selectedTag = e,
                            fbq("trackCustom", "changeTagOnFilter", {
                                tag: e.tag_title,
                                user_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : ""
                            }),
                            a.page = 1,
                            a.limit = 10,
                            a.filter()
                    },
                    filterTab: function t(e) {
                        var a = this;
                        a.selectedTab = e,
                            fbq("trackCustom", "changeTabOnFilter", {
                                tab: e.name,
                                user_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : ""
                            }),
                            a.page = 1,
                            a.limit = 10,
                            a.filter()
                    },
                    filter: function t() {
                        var e = this
                            , a = this;
                        this.closeAllFilter(),
                            this.closeSportFilter(),
                            this.closeMarketFilter(),
                            this.closeTagFilter(),
                            a.filtering = !0;
                        var i = {
                            page: a.page,
                            limit: a.limit,
                            tab_id: a.selectedTab.id,
                            sub_type_id: a.selectedMarket.sub_type_id,
                            sport_id: a.selectedSport.sport_id,
                            tag_id: a.selectedTag.tag_id
                        };
                        a.$axios.get(BASE_URL + "uo/matches", {
                            params: i
                        }).then(function(t) {
                            a.matches = t.data.data,
                                a.tabs = t.data.meta.tabs,
                                a.sports = t.data.meta.sports,
                                a.markets = t.data.meta.filters,
                                a.tags = t.data.meta.tags,
                                a.total = t.data.meta.total,
                                a.filtering = !1,
                                dataLayer.push({
                                    event: "pageMetaData",
                                    app: "MOBILE_WEB",
                                    page: {
                                        type: "prematch",
                                        title: "Pre-Match Matches",
                                        path: "/",
                                        category1: "",
                                        category2: e.selectedSport.sport_name,
                                        category3: e.selectedTag.tag_title,
                                        environment: "production",
                                        language: "EN",
                                        currency: "KES"
                                    },
                                    user: {
                                        loggedIn: !!e.$store.getters.user.id,
                                        id: e.$store.getters.user.id || "",
                                        mobile: e.$store.getters.user.mobile || ""
                                    }
                                })
                        }).catch(function(t) {
                            a.error = !0,
                                a.filtering = !1
                        })
                    },
                    loadMore: function t() {
                        var e = this;
                        if (!(e.page * e.limit > e.total)) {
                            e.loadingMore = !0,
                                e.page += 1;
                            var a = {
                                page: e.page,
                                limit: e.limit,
                                tab_id: e.selectedTab.id,
                                sub_type_id: e.selectedMarket.sub_type_id,
                                sport_id: e.selectedSport.sport_id,
                                tag_id: e.selectedTag.tag_id
                            };
                            e.$axios.get(BASE_URL + "uo/matches", {
                                params: a
                            }).then(function(t) {
                                e.matches = e.matches.concat(t.data.data),
                                    e.loadingMore = !1
                            }).catch(function(t) {
                                e.error = !0,
                                    e.loadingMore = !1
                            })
                        }
                    },
                    getMatches: function t() {
                        var e = this
                            , a = {
                            page: e.page,
                            limit: e.limit,
                            tab_id: e.selectedTab.id,
                            sub_type_id: e.selectedMarket.sub_type_id,
                            sport_id: e.selectedSport.sport_id
                        };
                        e.$axios.get(BASE_URL + "uo/matches", {
                            params: a
                        }).then(function(t) {
                            e.matches = t.data.data,
                                e.tabs = t.data.meta.tabs,
                                e.sports = t.data.meta.sports,
                                e.markets = t.data.meta.filters,
                                e.tags = t.data.meta.tags,
                                e.total = t.data.meta.total,
                                e.loading = !1
                        }).catch(function(t) {
                            e.error = !0,
                                e.loading = !1
                        })
                    }
                }
            }
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(212)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = l(a(364)), o, s = l(a(18)), r = a(308);
        function l(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "prebet-match",
            props: ["match", "upcoming", "selectedMarket"],
            components: {
                MatchOdd: n.default
            },
            computed: {
                odds: function t() {
                    var e = this;
                    return this.match.odds[0].odds.map(function(t) {
                        return t.parent_match_id = e.match.parent_match_id,
                            t
                    })
                },
                isMyMatchSelected: function t() {
                    var e = this, a;
                    return s.default.state.betSlip.filter(function(t) {
                        return t.matchId == e.match.match_id && t.type != e.selectedMarket.sub_type_id
                    }).length > 0
                }
            },
            methods: {
                openMarkets: function t() {
                    this.$router.push({
                        name: "prebet-markets",
                        params: {
                            matchId: this.match.match_id
                        }
                    })
                },
                getSportIcon: function t(e) {
                    return "https://www.betika.com/lite//img/sport/" + e + ".png"
                }
            },
            filters: {
                betterTime: r.betterTime
            }
        }
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(214)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = o(a(18));
        function o(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "match-odd",
            props: ["odd", "match", "market"],
            methods: {
                addToBetslip: function t() {
                    var e = this;
                    if (e.odd.odd_value) {
                        var a = {
                            matchId: e.match.match_id,
                            odd: e.odd,
                            type: e.market.sub_type_id,
                            market: e.market.name,
                            display: e.match.home_team + " vs. " + e.match.away_team,
                            startTime: e.match.start_time,
                            live: !1,
                            active: !0,
                            profile_id: e.$store.getters.user.id,
                            app_name: "MOBILE_WEB",
                            parent_match_id: e.match.parent_match_id,
                            game_description: e.match.home_team + " vs. " + e.match.away_team,
                            bet_pick: e.odd.odd_key,
                            sub_type_id: e.market.sub_type_id,
                            market_name: e.market.name,
                            odd_key: e.odd.odd_key,
                            odd_value: e.odd.odd_value,
                            special_bet_value: e.odd.special_bet_value || "",
                            bet_type: "prebet",
                            home_team: e.match.home_team,
                            away_team: e.match.away_team
                        };
                        n.default.state.betSlip.find(function(t) {
                            return t.matchId == e.match.match_id && t.odd == e.odd
                        }) ? this.removeFromBetslip() : (n.default.state.betSlip.find(function(t) {
                            return t.matchId == e.match.match_id
                        }) && this.removeFromBetslip(),
                            n.default.commit("addToBetslip", a))
                    }
                },
                removeFromBetslip: function t() {
                    this.selected = "";
                    var e = this;
                    n.default.commit("removeFromBetslip", {
                        matchId: e.match.match_id,
                        odd: e.odd,
                        type: e.market.sub_type_id,
                        market: e.market.name,
                        display: e.match.home_team + " vs. " + e.match.away_team,
                        startTime: e.match.start_time,
                        live: !1,
                        active: !0,
                        profile_id: e.$store.getters.user.id,
                        app_name: "MOBILE_WEB",
                        parent_match_id: e.match.parent_match_id,
                        game_description: e.match.home_team + " vs. " + e.match.away_team,
                        bet_pick: e.odd.odd_key,
                        sub_type_id: e.market.sub_type_id,
                        market_name: e.market.name,
                        odd_key: e.odd.odd_key,
                        odd_value: e.odd.odd_value,
                        special_bet_value: e.odd.special_bet_value || "",
                        bet_type: "prebet",
                        home_team: e.match.home_team,
                        away_team: e.match.away_team
                    })
                }
            },
            computed: {
                amISelected: function t() {
                    var e = this, a;
                    return n.default.state.betSlip.filter(function(t) {
                        return t.matchId == e.match.match_id && t.type == e.market.sub_type_id && t.odd.display == e.odd.display && t.odd.odd_key == e.odd.odd_key && t.odd.odd_value == e.odd.odd_value
                    }).length > 0
                }
            }
        }
    }
    , function(t, e, a) {
        var i = a(651);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("2767b726", i, !0, {})
    }
    , function(t, e, a) {
        var i = a(653);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("365ecf4e", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(218)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = o(a(256));
        function o(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "all-filters",
            props: {
                markets: Array,
                selectedMarket: Object,
                tags: Array,
                selectedTag: Object,
                sports: Array,
                selectedSport: Object,
                loading: Boolean
            },
            components: {
                Spinner: n.default
            },
            data: function t() {
                return {
                    dropdownOpen: !1
                }
            },
            methods: {
                closeDropdown: function t() {
                    this.dropdownOpen = !1
                },
                clickMarketsFilter: function t() {
                    this.dropdownOpen = !this.dropdownOpen
                }
            },
            computed: {
                tempSport: {
                    get: function t() {
                        return this.selectedSport
                    },
                    set: function t(e) {
                        this.$emit("selectSport", e)
                    }
                },
                tempMarket: {
                    get: function t() {
                        return this.selectedMarket
                    },
                    set: function t(e) {
                        this.$emit("selectMarket", e)
                    }
                },
                tempTag: {
                    get: function t() {
                        return this.selectedTag
                    },
                    set: function t(e) {
                        this.$emit("selectTag", e)
                    }
                }
            },
            watch: {
                tags: function t(e, a) {}
            }
        }
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(220)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }),
            e.default = {
                name: "Spinner"
            }
    }
    , function(t, e, a) {
        var i = a(656);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("d5de6140", i, !0, {})
    }
    , function(t, e, a) {
        var i = a(658);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("59938b22", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(224)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }),
            e.default = {
                name: "markets-filter",
                props: {
                    sports: Array,
                    selectedSport: Object
                }
            }
    }
    , function(t, e, a) {
        var i = a(661);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("21afe2c4", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(227)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }),
            e.default = {
                name: "markets-filter",
                props: {
                    markets: Array,
                    selectedMarket: Object
                }
            }
    }
    , function(t, e, a) {
        var i = a(663);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("24d0d84b", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(230)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }),
            e.default = {
                name: "tags-filter",
                props: {
                    tags: Array,
                    selectedTag: Object
                }
            }
    }
    , function(t, e, a) {
        var i = a(666);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("9eea7a44", i, !0, {})
    }
    , function(t, e, a) {
        var i = a(668);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("c98ae0d8", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(234)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i = a(672), n, o = s(a(57));
        function s(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "Slider",
            components: {
                swiper: i.swiper,
                swiperSlide: i.swiperSlide
            },
            data: function t() {
                return {
                    msg: "slider",
                    swiperOption: {
                        autoplay: {
                            delay: 7e3,
                            disableOnInteraction: !0
                        },
                        direction: "horizontal",
                        grabCursor: !0,
                        setWrapperSize: !0,
                        autoHeight: !0,
                        loop: !0,
                        pagination: {
                            el: ".swiper-pagination",
                            clickable: !0
                        },
                        paginationClickable: !0,
                        autoplayDisableOnInteraction: !1
                    }
                }
            },
            mounted: function t() {},
            methods: {
                goToJackpot: function t() {
                    this.$router.push("/jackpots?tab=13")
                }
            },
            computed: {
                loggedIn: function t() {
                    return JSON.parse(window.localStorage.getItem("authenticated"))
                },
                date: function t() {
                    return (0,
                        o.default)().date()
                }
            }
        }
    }
    , function(t, e, a) {
        var i = a(675);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("ffd13284", i, !0, {})
    }
    , function(t, e, a) {
        var i = a(678);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("58b4c57e", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(238)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = o(a(18));
        function o(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "snackbar",
            computed: {
                show: function t() {
                    return n.default.getters.snackBar.show
                },
                messages: function t() {
                    return n.default.getters.snackBar.messages
                },
                status: function t() {
                    return n.default.getters.snackBar.status
                }
            },
            methods: {
                close: function t() {
                    n.default.commit("toggleSnackBar", {
                        show: !1,
                        messages: [""],
                        status: "default"
                    })
                }
            }
        }
    }
    , function(t, e, a) {
        var i = a(680);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("612ab590", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(241)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = c(a(18)), o, s = c(a(310)), r, l = c(a(256));
        function c(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "search-modal",
            data: function t() {
                return {
                    keyword: "",
                    results: [],
                    loaded: !1,
                    selectedMarket: {
                        name: "3 Way",
                        sub_type_id: "1,186"
                    }
                }
            },
            components: {
                Match: s.default,
                Spinner: l.default
            },
            mounted: function t() {
                var e = this
                    , a = this;
                this.$refs.search.focus(),
                    setTimeout(function() {
                        a.$refs.search.focus()
                    }, 500),
                    a.results = [];
                var i = 1
                    , n = 50
                    , o = a.lastKeyword
                    , s = "";
                this.$axios.get(BASE_URL + "uo/matches?page=" + i + "&limit=" + n + "&keyword=" + o + "&tab=" + s + "&sub_type_id=" + this.selectedMarket.sub_type_id).then(function(t) {
                    e.results = t.data.data,
                        a.loaded = !0
                }).catch(function(t) {})
            },
            watch: {
                keyword: function t(e, a) {
                    this.search()
                },
                show: function t(e, a) {
                    var i = this
                        , n = this;
                    if (n.show) {
                        this.$nextTick(function() {
                            n.$refs.search.focus()
                        }),
                            n.results = [];
                        var o = {
                            page: 1,
                            limit: 50,
                            keyword: n.lastKeyword,
                            tab: ""
                        };
                        this.$axios.get(BASE_URL + "uo/matches?page=" + o.page + "&limit=" + o.limit + "&keyword=" + o.keyword + "&tab=" + o.tab + "&sub_type_id=" + this.selectedMarket.sub_type_id).then(function(t) {
                            i.results = t.data.data,
                                n.loaded = !0
                        }).catch(function(t) {})
                    }
                }
            },
            methods: {
                search: function t() {
                    var e = this
                        , a = this;
                    a.loaded = !1,
                        a.results = [];
                    var i = 1
                        , n = 50
                        , o = this.keyword
                        , s = "";
                    this.$axios.get(BASE_URL + "uo/matches?page=" + i + "&limit=" + n + "&keyword=" + o + "&tab=" + s + "&sub_type_id=" + this.selectedMarket.sub_type_id).then(function(t) {
                        e.results = t.data.data,
                            a.loaded = !0,
                            e.fbSearch()
                    }).catch(function(t) {})
                },
                fbSearch: function t() {
                    fbq("track", "Search", {
                        content_category: "pre_match",
                        content_ids: [window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : ""],
                        search_string: this.keyword
                    })
                },
                close: function t() {
                    window.localStorage.setItem("keyword", JSON.stringify(this.keyword)),
                        this.keyword = "",
                        n.default.commit("toggleSearch")
                },
                goTo: function t(e, a) {
                    this.option = e,
                        window.location.href = "#/" + a
                }
            },
            computed: {
                betSlipCount: function t() {
                    return n.default.getters.betSlipCount + n.default.getters.jengabetBetSlipCount
                },
                lastKeyword: function t() {
                    return window.localStorage.getItem("keyword", this.keyword) ? JSON.parse(window.localStorage.getItem("keyword", this.keyword)) : ""
                },
                show: function t() {
                    return n.default.state.search.show
                }
            }
        }
    }
    , function(t, e, a) {
        var i = a(683);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("434a63d0", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(244)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = o(a(369));
        function o(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "mini-betslip",
            components: {
                betslip: n.default
            },
            data: function t() {
                return {
                    open: !1,
                    placingBet: !1
                }
            },
            methods: {
                goToBetslip: function t() {
                    fbq("trackCustom", "clickMiniBetslip"),
                        this.$router.push("betslip")
                },
                placeBet: function t() {
                    var e = this;
                    e.placingBet = !0,
                        forcePlaceBet().then(function(t) {
                            e.placingBet = !1
                        }).catch(function(t) {
                            e.placingBet = !1
                        })
                },
                clear: function t() {
                    window.confirm("Are you sure you want to clear the betslip?") && this.$store.commit("clearBetslip")
                },
                setStake: function t(e) {
                    this.$store.commit("updateStake", e)
                },
                removeInvalidMatches: function t() {
                    this.$store.commit("removeInvalidMatches")
                }
            },
            computed: {
                showPlaceBet: function t() {
                    var e = this;
                    return e.$route.query.hasOwnProperty("watch") && "true" == e.$route.query.watch
                },
                betslipCount: function t() {
                    return this.$store.getters.betSlipCount
                },
                totalOdds: function t() {
                    var e;
                    return this.$store.getters.betSlip.map(function(t) {
                        return Number(t.odd.odd_value)
                    }).reduce(function(t, e) {
                        return t * e
                    }, 1)
                },
                possibleWinnings: function t() {
                    return this.totalOdds * this.$store.getters.stake
                },
                stake: function t() {
                    return this.$store.getters.stake
                },
                areThereAnyInvalidMatches: function t() {
                    return this.$store.getters.betSlip.filter(function(t) {
                        return "STOPPED" == t.status || !Number(t.odd.odd_value) || !t.active && t.live
                    }).length > 0
                }
            }
        }
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(246)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = r(a(685)), o, s = r(a(18));
        function r(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "Betslip",
            components: {
                Bet: n.default
            },
            props: ["keep"],
            data: function t() {
                return {
                    placeBetButton: "Place Bet",
                    placingBet: !1,
                    changedMatches: []
                }
            },
            created: function t() {},
            mounted: function t() {
                try {
                    dataLayer.push({
                        event: "pageMetaData",
                        app: "MOBILE_WEB",
                        page: {
                            type: "prematch",
                            title: "Prematch betslip",
                            path: "/betslip",
                            environment: "production",
                            language: "EN",
                            currency: "KES"
                        },
                        user: {
                            loggedIn: !!this.$store.getters.user.id,
                            id: this.$store.getters.user.id || "",
                            mobile: this.$store.getters.user.mobile || ""
                        }
                    })
                } catch (t) {}
            },
            watch: {
                amount: function t(e, a) {
                    this.amount = Math.floor(e)
                }
            },
            computed: {
                amount: {
                    get: function t() {
                        return this.$store.getters.stake
                    },
                    set: function t(e) {
                        this.$store.commit("updateStake", parseFloat(e))
                    }
                },
                bets: function t() {
                    return s.default.state.betSlip
                },
                matches: function t() {
                    return s.default.state.matches
                },
                totalOdds: function t() {
                    var e;
                    return this.bets.map(function(t) {
                        return Number(t.odd.odd_value)
                    }).reduce(function(t, e) {
                        return t * e
                    }, 1)
                },
                possibleWinnings: function t() {
                    return this.totalOdds * this.amount
                },
                bonus: function t() {
                    var e = this.bets.filter(function(t) {
                        return Number(t.odd.odd_value) >= 1.6
                    }).map(function(t) {
                        return t.odd.odd_value
                    })
                        , a = e.reduce(function(t, e) {
                        return t * e
                    }, 1)
                        , i = 5
                        , n = 5 == e.length ? 5 : 5 * (e.length - 5);
                    return e.length >= 5 ? 50 * a * (n / 100) : 0
                },
                points: function t() {
                    var t = void 0;
                    if (this.totalOdds >= 9.99)
                        switch (!0) {
                            case this.amount < 49:
                                t = 0;
                                break;
                            case this.amount >= 49 && this.amount < 100:
                                t = Number(this.amount) * Number(this.totalOdds) / 100,
                                    t = t > 25 ? 25 : t;
                                break;
                            case this.amount >= 100 && this.amount < 501:
                                t = Number(this.amount) * Number(this.totalOdds) / 100,
                                    t = t > 50 ? 50 : t;
                                break;
                            case this.amount > 500 && this.amount < 5001:
                                t = Number(this.amount) * Number(this.totalOdds) / 100,
                                    t = t > 100 ? 100 : t;
                                break;
                            case this.amount > 5e3:
                                t = Number(this.amount) * Number(this.totalOdds) / 100,
                                    t = t > 200 ? 200 : t;
                                break;
                            default:
                                t = 0
                        }
                    else
                        t = 0;
                    return t
                },
                loggedIn: function t() {
                    return JSON.parse(window.localStorage.getItem("authenticated"))
                },
                liveMatches: function t() {
                    return this.bets.filter(function(t) {
                        return t.live
                    }).length > 0
                },
                prebetMatches: function t() {
                    return this.bets.filter(function(t) {
                        return !t.live
                    }).length > 0
                },
                numberOfBets: function t() {
                    return this.bets.length
                },
                areThereAnyChangedOdds: function t() {
                    return this.bets.filter(function(t) {
                        return t.changed
                    }).length > 0
                },
                areThereAnyInvalidMatches: function t() {
                    return this.bets.filter(function(t) {
                        return "STOPPED" == t.status || !Number(t.odd.odd_value) || !t.active && t.live
                    }).length > 0
                }
            },
            methods: {
                clearBetslip: function t() {
                    1 == window.confirm("Are you sure you want to clear the betslip?") && s.default.commit("clearBetslip")
                },
                refreshBalance: function t() {
                    this.$axios.post(BASE_URL + "balance", {
                        token: window.localStorage.getItem("token") ? JSON.parse(window.localStorage.getItem("token")) : ""
                    }).then(function(t) {
                        s.default.commit("setBalance", t.data.data.balance)
                    }).catch(function(t) {})
                },
                placeBet: function t(e) {
                    var a = this;
                    if (!this.areThereAnyChangedOdds && !this.placingBet) {
                        var i = this
                            , n = new Date;
                        if (s.default.state.betSlip.find(function(t) {
                            return t.live
                        })) {
                            if (n.getUTCHours() > 22 && n.getUTCHours() < 5 && this.amount > 1e5)
                                return s.default.commit("toggleSnackBar", {
                                    show: !0,
                                    messages: ["Maximum live bets amount is 100000KES."],
                                    status: "warning"
                                }),
                                    void setTimeout(function() {
                                        s.default.commit("toggleSnackBar", {
                                            show: !1,
                                            messages: [""]
                                        })
                                    }, 5e3);
                            if (n.getUTCHours() > 5 && n.getUTCHours() < 22 && this.amount > 1e5)
                                return s.default.commit("toggleSnackBar", {
                                    show: !0,
                                    messages: ["Maximum live bets amount is 100000KES."],
                                    status: "warning"
                                }),
                                    void setTimeout(function() {
                                        s.default.commit("toggleSnackBar", {
                                            show: !1,
                                            messages: [""]
                                        })
                                    }, 5e3)
                        }
                        if (this.placeBetButton = "Placing Bet...",
                            e.target.classList.add("disabled"),
                            i.placingBet = !0,
                            !JSON.parse(window.localStorage.getItem("authenticated")))
                            return this.placeBetButton = "Place Bet",
                                e.target.classList.remove("disabled"),
                                i.placingBet = !1,
                                s.default.commit("toggleSnackBar", {
                                    show: !0,
                                    messages: ["Please login first"],
                                    status: "warning"
                                }),
                                setTimeout(function() {
                                    s.default.commit("toggleSnackBar", {
                                        show: !1,
                                        messages: [""]
                                    })
                                }, 3e3),
                                void this.$router.push("/login");
                        if (this.amount < 1)
                            return this.placeBetButton = "Place Bet",
                                e.target.classList.remove("disabled"),
                                i.placingBet = !1,
                                s.default.commit("toggleSnackBar", {
                                    show: !0,
                                    messages: ["Minimum bet amount is 1 bob."],
                                    status: "info"
                                }),
                                void setTimeout(function() {
                                    s.default.commit("toggleSnackBar", {
                                        show: !1,
                                        messages: [""]
                                    })
                                }, 5e3);
                        var o = [];
                        this.bets.forEach(function(t) {
                            var e = {
                                parent_match_id: t.parent_match_id,
                                game_description: t.game_description,
                                bet_pick: t.bet_pick,
                                sub_type_id: t.sub_type_id,
                                market_name: t.market,
                                odd_key: t.odd_key,
                                odd_value: t.odd_value,
                                outcome_id: t.odd.outcome_id,
                                special_bet_value: t.special_bet_value,
                                bet_type: t.bet_type,
                                home_team: t.home_team,
                                away_team: t.away_team
                            };
                            o.push(e)
                        });
                        var r = this.$store.getters.user.balance
                            , l = this.$store.getters.user.bonus
                            , c = this.bets.map(function(t) {
                            return {
                                sub_type_id: t.type,
                                bet_pick: t.odd.odd_key,
                                odd_value: t.odd.odd_value,
                                outcome_id: t.odd.outcome_id,
                                special_bet_value: t.odd.special_bet_value ? t.odd.special_bet_value : "",
                                parent_match_id: t.odd.parent_match_id,
                                bet_type: t.live ? 8 : 7
                            }
                        })
                            , d = {
                            profile_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : "",
                            stake: this.amount.toString(),
                            total_odd: this.totalOdds.toString(),
                            src: "MOBILE_WEB",
                            betslip: c,
                            token: window.localStorage.getItem("token") ? JSON.parse(window.localStorage.getItem("token")) : "",
                            user_agent: localStorage.user_agent ? localStorage.user_agent : null,
                            affiliate: sessionStorage.affiliate ? sessionStorage.affiliate : null
                        };
                        i.$cookie.get("btag") && (d.affiliate_id = 19,
                            d.affiliate_tag = i.$cookie.get("btag")),
                            this.fbInitiateBet();
                        var u = this.liveMatches ? 5e3 : 0;
                        setTimeout(function() {
                            i.$axios.post(BASE_URL + "uo/placebet", d).then(function(t) {
                                var n = {
                                    bet_id: t.data.bet_id,
                                    stake: a.amount,
                                    possible_win: a.possibleWinnings,
                                    total_odds: a.totalOdds,
                                    number_of_games: a.numberOfBets,
                                    wallet_balance_before: r,
                                    wallet_balance_after: t.data.user.balance,
                                    wallet_bonus_before: l,
                                    wallet_bonus_after: t.data.user.bonus,
                                    array_of_matches: o
                                };
                                i.$track.session.betslip.place(n),
                                    i.placeBetButton = "Place Bet",
                                    e.target.classList.remove("disabled"),
                                    i.placingBet = !1,
                                    1 == t.data.first_bet ? i.fbFirstTimeBet(t.data.message) : i.fbPlaceBet(t.data.message, t.data.bet_id),
                                i.keep || s.default.commit("clearBetslip"),
                                    i.$router.go("-1"),
                                    s.default.commit("toggleSnackBar", {
                                        show: !0,
                                        messages: [t.data.message],
                                        status: "success"
                                    }),
                                    setTimeout(function() {
                                        s.default.commit("toggleSnackBar", {
                                            show: !1,
                                            messages: [""]
                                        })
                                    }, 1e4),
                                    i.refreshBalance()
                            }).catch(function(t) {
                                if (i.placeBetButton = "Place Bet",
                                    e.target.classList.remove("disabled"),
                                    i.placingBet = !1,
                                    t.response) {
                                    if (401 == t.response.status)
                                        return window.localStorage.setItem("authenticated", !1),
                                            window.localStorage.removeItem("user"),
                                            window.localStorage.removeItem("token"),
                                            i.fbFailedBet("Your session has expired. Please login again"),
                                            s.default.commit("toggleSnackBar", {
                                                show: !0,
                                                messages: ["Your session has expired. Please login again"],
                                                status: "error"
                                            }),
                                            setTimeout(function() {
                                                s.default.commit("toggleSnackBar", {
                                                    show: !1,
                                                    messages: [""]
                                                })
                                            }, 5e3),
                                            void i.$router.replace("/login");
                                    i.fbFailedBet(t.response.data.message),
                                        s.default.commit("toggleSnackBar", {
                                            show: !0,
                                            messages: [t.response.data.message],
                                            status: "error"
                                        }),
                                        setTimeout(function() {
                                            s.default.commit("toggleSnackBar", {
                                                show: !1,
                                                messages: [""]
                                            })
                                        }, 1e4)
                                } else
                                    i.fbFailedBet(t.message)
                            })
                        }, u)
                    }
                },
                acceptOddChanges: function t() {
                    this.placingBet || s.default.state.betSlip.map(function(t) {
                        return t.changed = !1
                    })
                },
                removeInvalidMatches: function t() {
                    this.placingBet || s.default.commit("removeInvalidMatches")
                },
                fbPlaceBet: function t(e, a) {
                    fbq("trackCustom", "PlaceBetMobileWeb", {
                        bet_type: this.liveMatches && this.prebetMatches ? "live_match" : this.liveMatches ? "live_match" : "pre_match",
                        num_items: this.bets.length,
                        user_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : "",
                        value: this.amount,
                        currency: "KES",
                        message: e
                    }),
                        dataLayer.push({
                            event_name: "PlaceBetMobileWeb",
                            app_name: "MOBILE_WEB",
                            bet_type: this.liveMatches && this.prebetMatches ? "live_match" : this.liveMatches ? "live_match" : "pre_match",
                            num_items: this.bets.length,
                            user_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : "",
                            amount: this.amount,
                            channel: "mobile_web",
                            bet_id: a
                        }),
                        dataLayer.push({
                            event: "betPlacedSuccessful",
                            app: "MOBILE_WEB",
                            user: {
                                id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : ""
                            },
                            bet: {
                                id: a,
                                amount: this.amount,
                                type1: this.liveMatches && this.prebetMatches ? "live bet" : this.liveMatches ? "live bet" : "pre-match",
                                numberOfMatches: this.bets.length,
                                odds: this.totalOdds
                            }
                        })
                },
                fbInitiateBet: function t() {
                    fbq("trackCustom", "InitiateBetMobileWeb", {
                        bet_type: this.liveMatches && this.prebetMatches ? "live_match" : this.liveMatches ? "live_match" : "pre_match",
                        num_items: this.bets.length,
                        user_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : "",
                        value: this.amount,
                        currency: "KES"
                    })
                },
                fbFailedBet: function t(e) {
                    fbq("trackCustom", "FailedBetMobileWeb", {
                        bet_type: this.liveMatches && this.prebetMatches ? "live_match" : this.liveMatches ? "live_match" : "pre_match",
                        num_items: this.bets.length,
                        user_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : "",
                        value: this.amount,
                        currency: "KES",
                        message: e
                    })
                },
                fbFirstTimeBet: function t(e) {
                    fbq("trackCustom", "FTPlaceBetMobileWeb", {
                        bet_type: this.liveMatches && this.prebetMatches ? "live_match" : this.liveMatches ? "live_match" : "pre_match",
                        num_items: this.bets.length,
                        user_id: window.localStorage.getItem("user") ? JSON.parse(window.localStorage.getItem("user")).id : "",
                        value: this.amount,
                        currency: "KES",
                        message: e
                    })
                }
            }
        }
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(248)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = o(a(18));
        function o(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            name: "bet",
            props: ["bet"],
            methods: {
                removeBet: function t() {
                    n.default.commit("removeFromBetslip", this.bet)
                },
                getSpecialBetValue: function t() {
                    var e = this.bet.odd.special_bet_value.split("|").findIndex(function(t) {
                        return "score" == t.split("=")[0]
                    });
                    return e > -1 ? "(" + this.bet.odd.special_bet_value.split("|")[e].split("=")[1] + ")" : ""
                }
            }
        }
    }
    , function(t, e, a) {
        var i = a(687);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("e43ead0c", i, !0, {})
    }
    , function(t, e, a) {
        var i = a(689);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("3bb7330c", i, !0, {})
    }
    , function(t, e, a) {
        var i = a(691);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("2c9ce162", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(253)
            , n = a.n(i);
        for (var o in i)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return i[t]
                })
            }(o);
        e.default = n.a
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }),
            e.default = {
                name: "mini-betslip",
                props: {
                    type: {
                        type: String,
                        default: ""
                    }
                },
                watch: {
                    betslipCount: function t() {
                        var e = window.document.querySelector(".betslip-fab");
                        e && (e.style.transitionDuration = "150",
                            e.style.transform = "scale(1.1)",
                            setTimeout(function() {
                                e.style.transform = "scale(1)"
                            }, 150))
                    }
                },
                methods: {
                    toBetslip: function t() {
                        this.$router.push("/betslip?type=" + this.type)
                    }
                },
                computed: {
                    betslipCount: function t() {
                        return "shikisha" == this.type ? this.$store.getters.jengabetBetSlipCount : this.$store.getters.betSlipCount
                    }
                }
            }
    }
    , function(t, e, a) {
        var i = a(693);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("3bbe0488", i, !0, {})
    }
    , function(t, e, a) {
        var i = a(698);
        "string" == typeof i && (i = [[t.i, i, ""]]),
        i.locals && (t.exports = i.locals);
        var n, o = (0,
            a(12).default)("77d02ae4", i, !0, {})
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(297)
            , n = a(219);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(655)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Spinner.vue",
            e.default = l.exports
    }
    , , , , , , , , , , , , , , , , , , , , , , , , , , , , , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = o(a(18));
        function o(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            bet: {
                add: function t(e) {},
                success: function t(e) {
                    try {
                        fbq("trackCustom", "PlaceBetMobileWeb", {
                            bet_type: e.bet_type,
                            num_items: e.num_of_items,
                            user_id: e.user_id,
                            value: e.amount,
                            currency: "Ksh",
                            message: e.message
                        })
                    } catch (t) {}
                },
                initiate: function t(e) {
                    try {
                        fbq("trackCustom", "InitiateBetMobileWeb", {
                            bet_type: e.bet_type,
                            num_items: e.num_of_items,
                            user_id: e.user_id,
                            value: e.amount,
                            currency: "Ksh",
                            message: e.message
                        })
                    } catch (t) {}
                },
                failed: function t(e) {
                    try {
                        fbq("trackCustom", "FailedBetMobileWeb", {
                            bet_type: e.bet_type,
                            num_items: e.num_of_items,
                            user_id: e.user_id,
                            value: e.amount,
                            currency: "Ksh",
                            message: e.message
                        })
                    } catch (t) {}
                },
                firstTime: function t(e) {
                    try {
                        fbq("trackCustom", "FTPlaceBetMobileWeb", {
                            bet_type: e.bet_type,
                            num_items: e.num_of_items,
                            user_id: e.user_id,
                            value: e.amount,
                            currency: "Ksh",
                            message: e.message
                        })
                    } catch (t) {}
                }
            },
            session: {
                login: function t(e) {},
                signup: function t(e) {},
                logout: function t(e) {},
                reset_password: function t(e) {},
                deposit: {
                    click: function t(e) {},
                    success: function t(e) {
                        try {
                            fbq("trackCustom", "DepositMobileWeb", {
                                user_id: e.user_id,
                                value: e.amount,
                                currency: "KES"
                            })
                        } catch (t) {}
                    },
                    failed: function t(e) {
                        try {
                            fbq("trackCustom", "DepositFailedMobileWeb", {
                                user_id: e.user_id,
                                value: e.amount,
                                currency: "KES"
                            })
                        } catch (t) {}
                    }
                },
                withdraw: {
                    click: function t(e) {},
                    success: function t(e) {
                        try {
                            fbq("trackCustom", "WithdrawMobileWeb", {
                                user_id: e.user_id,
                                value: e.amount,
                                currency: "KES"
                            })
                        } catch (t) {}
                    },
                    failed: function t(e) {
                        try {
                            fbq("trackCustom", "WithdrawFailedMobileWeb", {
                                user_id: e.user_id,
                                value: e.amount,
                                currency: "KES"
                            })
                        } catch (t) {}
                    }
                },
                betslip: {
                    add: function t(e) {},
                    remove: function t(e) {},
                    clear: function t(e) {
                        try {
                            e.app_name = "MOBILE_WEB",
                                e.profile_id = n.default.getters.user.id
                        } catch (t) {}
                    },
                    place: function t(e) {
                        try {
                            e.app_name = "MOBILE_WEB",
                                e.profile_id = n.default.getters.user.id,
                                e.user_alternative_id = n.default.getters.user.id
                        } catch (t) {}
                    }
                }
            },
            page: {
                view: function t(e) {}
            },
            stream: {
                watch: function t(e) {
                    try {
                        fbq("trackCustom", "WatchLiveStreamMobile", {
                            provider: e.provider,
                            event: e.event,
                            user_id: e.user_id
                        })
                    } catch (t) {}
                }
            }
        }
    }
    , function(t, e, a) {
        t.exports = a.p + "img/highlights.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/atoz.svg"
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "content"
            }, [a("slider"), t._v(" "), a("prebet-matches")], 1)
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , i = t._self._c || e;
            return i("div", {
                attrs: {
                    id: "app"
                }
            }, [i("snack-bar"), t._v(" "), i("search-modal"), t._v(" "), i("div", {
                ref: "addToHomeScreen",
                staticClass: "modal modal--active claim-bonus-modal",
                staticStyle: {
                    display: "none"
                }
            }, [i("div", {
                staticClass: "modal__container claim-bonus-modal__container",
                class: {
                    "claim-bonus-modal__container--safari": t.isSafari
                }
            }, [t.isSafari ? t._e() : i("svg", {
                staticClass: "claim-bonus-modal__pointer",
                attrs: {
                    viewBox: "0 0 151 362",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink"
                }
            }, [i("g", {
                attrs: {
                    id: "Web",
                    stroke: "none",
                    "stroke-width": "1",
                    fill: "none",
                    "fill-rule": "evenodd"
                }
            }, [i("g", {
                attrs: {
                    id: "Group-23",
                    transform: "translate(2.000000, 5.000000)",
                    stroke: "#F7F7F7",
                    "stroke-width": "6"
                }
            }, [i("path", {
                attrs: {
                    d: "M0,354 C40.5444951,324.05503 69.0926271,283.620484 85.6443962,232.696364 C102.196165,181.772243 109.643504,104.540122 107.986413,1",
                    id: "Path-2",
                    "stroke-dasharray": "18,4"
                }
            }), t._v(" "), i("path", {
                attrs: {
                    d: "M108,0 L144.390157,34.7096016",
                    id: "Line-6",
                    "stroke-linecap": "square"
                }
            }), t._v(" "), i("path", {
                attrs: {
                    d: "M72,35.4742547 L107.646337,0",
                    id: "Line-6",
                    "stroke-linecap": "square"
                }
            })])])]), t._v(" "), i("span", {
                staticClass: "modal__container__close modal__container__close--left",
                on: {
                    click: function(e) {
                        t.addToHomeScreen(!1)
                    }
                }
            }, [t._v("×")]), t._v(" "), i("img", {
                staticClass: "claim-bonus-modal__logo",
                attrs: {
                    src: a(360),
                    height: "40px"
                }
            }), t._v(" "), i("p", {
                staticClass: "claim-bonus-modal__title tc"
            }, [t._v("Install Betika Web App to Watch Free Live Matches")]), t._v(" "), i("div", {
                staticClass: "claim-bonus-modal__list"
            }, [t.isSafari ? t._e() : i("p", {
                staticClass: "claim-bonus-modal__list__item claim-bonus-modal__list__item__with-icon"
            }, [i("span", [t._v("1. Click on ")]), t._v(" "), i("svg", {
                staticClass: "claim-bonus-modal__dotdot",
                attrs: {
                    height: "30px",
                    viewBox: "0 0 20 79",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink"
                }
            }, [i("g", {
                attrs: {
                    id: "Web",
                    stroke: "none",
                    "stroke-width": "1",
                    fill: "none",
                    "fill-rule": "evenodd"
                }
            }, [i("g", {
                attrs: {
                    id: "Group-26",
                    transform: "translate(-344.000000, 0.000000)",
                    fill: "#4A4A4A"
                }
            }, [i("g", {
                attrs: {
                    id: "Group-25"
                }
            }, [i("g", {
                attrs: {
                    id: "Group-24",
                    transform: "translate(344.500000, 0.000000)"
                }
            }, [i("circle", {
                attrs: {
                    id: "Oval-4",
                    cx: "9.5",
                    cy: "9.5",
                    r: "9.5"
                }
            }), t._v(" "), i("circle", {
                attrs: {
                    id: "Oval-4-Copy",
                    cx: "9.5",
                    cy: "39.5",
                    r: "9.5"
                }
            }), t._v(" "), i("circle", {
                attrs: {
                    id: "Oval-4-Copy-2",
                    cx: "9.5",
                    cy: "69.5",
                    r: "9.5"
                }
            })])])])])]), t._v(" "), i("span", [t._v("above")])]), t._v(" "), t.isSafari ? i("p", {
                staticClass: "claim-bonus-modal__list__item claim-bonus-modal__list__item__with-icon"
            }, [i("span", [t._v("1. Click on ")]), t._v(" "), i("img", {
                staticClass: "claim-bonus-modal__dotdot",
                attrs: {
                    src: a(605),
                    width: "15px"
                }
            }), t._v(" "), i("span", [t._v(" below")])]) : t._e(), t._v(" "), i("p", {
                staticClass: "claim-bonus-modal__list__item"
            }, [t._v("2. Add to Homescreen")]), t._v(" "), i("p", {
                staticClass: "claim-bonus-modal__list__item"
            }, [t._v("3. Click Betika Icon on Homescreen")]), t._v(" "), i("p", {
                staticClass: "claim-bonus-modal__list__item"
            }, [t._v("4. Watch Live!")])])])]), t._v(" "), i("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.show_banner,
                    expression: "show_banner"
                }],
                staticClass: "banner",
                on: {
                    click: t.closeBanner
                }
            }, [i("div", {
                staticClass: "banner__container",
                on: {
                    click: function(t) {
                        t.stopPropagation()
                    }
                }
            }, [i("span", {
                staticClass: "banner__close",
                on: {
                    click: t.closeBanner
                }
            }, [t._v("×")]), t._v(" "), i("img", {
                staticClass: "banner__image",
                attrs: {
                    src: a(606)
                }
            }), t._v(" "), i("div", {
                staticClass: "banner__message__container"
            }, [i("span", {
                staticClass: "banner__message_title"
            }, [t._v(t._s(t.loginMessageTitle))]), t._v(" "), i("br"), t._v(" "), i("span", {
                staticClass: "banner__message_content"
            }, [t._v(t._s(t.loginMessageContent))])])])]), t._v(" "), t.show.header ? i("top", {
                attrs: {
                    show: t.show
                }
            }) : t._e(), t._v(" "), i("main", {
                class: {
                    "android-banner-active": t.canShowAndroidBanner
                }
            }, [i("router-view")], 1), t._v(" "), t.show.footer ? i("bottom") : t._e(), t._v(" "), t.show_income_access ? i("iframe", {
                staticStyle: {
                    display: "none"
                },
                attrs: {
                    id: "incomeaccess",
                    src: "http://wlbetika.adsrv.eacdn.com/Processing/Pixels/Registration.ashx?PlayerID=" + t.user.id
                }
            }) : t._e(), t._v(" "), t.showMiniBetslip ? i("mini-betslip") : t._e()], 1)
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "betslip"
            }, [a("div", {
                staticClass: "betslip-content"
            }, [t._l(t.bets, function(e) {
                return t.bets.length > 0 ? a("bet", {
                    key: e.matchId,
                    attrs: {
                        bet: e
                    }
                }) : t._e()
            }), t._v(" "), t.bets.length <= 0 ? a("div", {
                staticClass: "betslip-content--empty"
            }, [t._v("You have no bets in your betslip."), a("br"), t._v("Kindly add some bets")]) : t._e()], 2), t._v(" "), t.bets.length > 0 ? a("div", {
                staticClass: "betslip-details"
            }, [a("table", [a("tr", [a("td", {
                staticClass: "left",
                attrs: {
                    colspan: "3"
                }
            }, [a("table", {
                attrs: {
                    width: "100%"
                }
            }, [a("tbody", [a("tr", [a("td", {
                staticClass: "dark-gray"
            }, [t._v("Total Odds")]), t._v(" "), a("td", {
                staticClass: "text-right theme-color bold"
            }, [t._v(t._s(Math.round(100 * t.totalOdds) / 100))])]), t._v(" "), a("tr", [a("td", {
                staticClass: "dark-gray"
            }, [t._v("Possible Winnings")]), t._v(" "), a("td", {
                staticClass: "text-right theme-color bold"
            }, [t._v("Ksh. " + t._s(Math.round(1 * t.possibleWinnings) / 1))])]), t._v(" "), a("tr", [a("td", {
                staticClass: "dark-gray"
            }, [t._v("Bonus")]), t._v(" "), a("td", {
                staticClass: "text-right theme-color bold"
            }, [t._v(t._s(1 * Math.round(1 * t.bonus)))])]), t._v(" "), a("tr", [a("td", {
                staticClass: "dark-gray"
            }, [t._v("Points")]), t._v(" "), a("td", {
                staticClass: "text-right theme-color bold"
            }, [t._v(t._s(1 * Math.round(1 * t.points)))])])])])]), t._v(" "), a("td", {
                staticClass: "right"
            }, [a("table", {
                attrs: {
                    width: "100%"
                }
            }, [a("tbody", [a("tr", [a("td", {
                staticClass: "bold"
            }, [t._v("Amount "), a("input", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.amount,
                    expression: "amount"
                }],
                attrs: {
                    type: "number",
                    name: "amount",
                    disabled: t.placingBet,
                    min: "1",
                    step: "1"
                },
                domProps: {
                    value: t.amount
                },
                on: {
                    input: function(e) {
                        e.target.composing || (t.amount = e.target.value)
                    }
                }
            }), t._v(" KES")])]), t._v(" "), t._m(0), t._v(" "), a("tr", [a("td", {
                attrs: {
                    colspan: "10"
                }
            }, [t.areThereAnyChangedOdds || t.areThereAnyInvalidMatches ? t._e() : a("button", {
                staticClass: "place dark-gray",
                attrs: {
                    id: "placeBetButton",
                    amount: t.amount,
                    possibleWin: t.possibleWinnings,
                    totalOdds: t.totalOdds,
                    bonus: t.bonus,
                    points: t.points,
                    numberOfBets: t.numberOfBets,
                    type: "normal",
                    disabled: t.placingBet
                },
                on: {
                    click: function(e) {
                        t.placeBet(e)
                    }
                }
            }, [t._v("\n                    " + t._s(t.placeBetButton) + "\n                    "), t.placingBet ? a("loader") : t._e()], 1), t._v(" "), t.areThereAnyChangedOdds && !t.areThereAnyInvalidMatches ? a("button", {
                staticClass: "place dark-gray",
                on: {
                    click: function(e) {
                        t.acceptOddChanges()
                    }
                }
            }, [t._v("Accept Changes")]) : t._e(), t._v(" "), t.areThereAnyInvalidMatches ? a("button", {
                staticClass: "place dark-gray",
                on: {
                    click: function(e) {
                        t.removeInvalidMatches()
                    }
                }
            }, [t._v("Remove Locked Matches")]) : t._e()])])])])])])])]) : t._e(), t._v(" "), t.bets.length > 0 ? a("button", {
                staticClass: "delete-all",
                on: {
                    click: function(e) {
                        t.clearBetslip()
                    }
                }
            }, [t._v("Delete All")]) : t._e()])
        }
            , n = [function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("tr", [a("td")])
        }
        ];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , i = t._self._c || e;
            return i("header", [i("android-app-banner"), t._v(" "), i("div", {
                staticClass: "top-search top"
            }, [i("div", {
                staticClass: "logo"
            }, [i("img", {
                attrs: {
                    src: a(360),
                    height: "25px",
                    alt: "logo"
                },
                on: {
                    click: function(e) {
                        t.goTo("/")
                    }
                }
            })]), t._v(" "), i("span", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.balanceSet && t.loggedIn,
                    expression: "balanceSet && loggedIn"
                }],
                staticClass: "balance",
                on: {
                    click: function(e) {
                        t.goTo("/profile")
                    }
                }
            }, [i("u", [t._v("Deposit")]), i("br"), i("b", [t._v("Bal: " + t._s(t.balance))])]), t._v(" "), i("div", {
                staticClass: "right"
            }, [i("div", {
                staticClass: "search-icon",
                on: {
                    click: function(e) {
                        t.$store.commit("toggleSearch")
                    }
                }
            }, [i("svg", {
                staticStyle: {
                    "enable-background": "new 0 0 451 451",
                    "margin-top": "5px"
                },
                attrs: {
                    width: "15px",
                    x: "0px",
                    y: "0px",
                    viewBox: "0 0 451 451",
                    "xml:space": "preserve"
                }
            }, [i("g", [i("path", {
                attrs: {
                    d: "M447.05,428l-109.6-109.6c29.4-33.8,47.2-77.9,47.2-126.1C384.65,86.2,298.35,0,192.35,0C86.25,0,0.05,86.3,0.05,192.3\n                s86.3,192.3,192.3,192.3c48.2,0,92.3-17.8,126.1-47.2L428.05,447c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4\n                C452.25,441.8,452.25,433.2,447.05,428z M26.95,192.3c0-91.2,74.2-165.3,165.3-165.3c91.2,0,165.3,74.2,165.3,165.3\n                s-74.1,165.4-165.3,165.4C101.15,357.7,26.95,283.5,26.95,192.3z"
                }
            })])]), t._v(" "), i("span", {
                staticClass: "search-icon__label"
            }, [t._v("Search")])]), t._v(" "), i("div", {
                staticClass: "betslip",
                on: {
                    click: function(e) {
                        t.goTo("/betslip")
                    }
                }
            }, [i("div", [i("svg", {
                attrs: {
                    width: "193px",
                    height: "239px",
                    viewBox: "0 0 193 239",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink"
                }
            }, [i("g", {
                attrs: {
                    id: "Page-1",
                    stroke: "none",
                    "stroke-width": "1",
                    fill: "none",
                    "fill-rule": "evenodd"
                }
            }, [i("path", {
                attrs: {
                    d: "M155.010674,-1.42108547e-14 L143.239326,-1.42108547e-14 C143.246418,0.165781719 143.25,0.332472526 143.25,0.5 C143.25,6.85127462 138.101275,12 131.75,12 C125.398725,12 120.25,6.85127462 120.25,0.5 C120.25,0.332472526 120.253582,0.165781719 120.260674,-1.89570581e-14 L108.489326,-1.42108547e-14 C108.496418,0.165781719 108.5,0.332472526 108.5,0.5 C108.5,6.85127462 103.351275,12 97,12 C90.6487254,12 85.5,6.85127462 85.5,0.5 C85.5,0.332472526 85.5035822,0.165781719 85.5106742,-2.10664819e-14 L73.7393258,-1.42108547e-14 C73.7464178,0.165781719 73.75,0.332472526 73.75,0.5 C73.75,6.85127462 68.6012746,12 62.25,12 C55.8987254,12 50.75,6.85127462 50.75,0.5 C50.75,0.332472526 50.7535822,0.165781719 50.7606742,-2.10664819e-14 L38.9893258,-1.42108547e-14 C38.9964178,0.165781719 39,0.332472526 39,0.5 C39,6.85127462 33.8512746,12 27.5,12 C21.1487254,12 16,6.85127462 16,0.5 C16,0.332472526 16.0035822,0.165781719 16.0106742,-1.89570581e-14 L0,-1.42108547e-14 L0,239 L193,239 L193,-1.42108547e-14 L177.989326,-1.42108547e-14 C177.996418,0.165781719 178,0.332472526 178,0.5 C178,6.85127462 172.851275,12 166.5,12 C160.148725,12 155,6.85127462 155,0.5 C155,0.332472526 155.003582,0.165781719 155.010674,-2.10664819e-14 Z M16,25 L178,25 L178,226 L16,226 L16,25 Z M35,72 L158,72 L158,91 L35,91 L35,72 Z M35,109 L158,109 L158,128 L35,128 L35,109 Z M35,146 L158,146 L158,165 L35,165 L35,146 Z",
                    id: "betslip",
                    fill: "#FFFFFF"
                }
            })])]), t._v(" "), i("span", {
                staticClass: "betslip--count",
                on: {
                    click: function(e) {
                        t.goTo("football", "betslip")
                    }
                }
            }, [t._v(t._s(t.betSlipCount))])]), t._v(" "), i("span", {
                staticClass: "betslip--label"
            }, [t._v("Betslip")])])])]), t._v(" "), t.loggedIn ? t._e() : i("div", {
                staticClass: "top-session"
            }, [i("button", {
                staticClass: "top-session--register",
                on: {
                    click: function(e) {
                        t.goTo("/signup")
                    }
                }
            }, [t._v("Register")]), t._v(" "), i("button", {
                staticClass: "top-session--login",
                on: {
                    click: function(e) {
                        t.goTo("/login")
                    }
                }
            }, [t._v("Login")])]), t._v(" "), t.show.slideNav ? i("div", {
                staticClass: "nav"
            }, [i("div", {
                staticClass: "nav--item",
                class: {
                    selected: "/shikisha" == t.$route.path
                },
                staticStyle: {
                    "margin-left": "5px"
                },
                on: {
                    click: function(e) {
                        t.goTo("/shikisha")
                    }
                }
            }, [i("img", {
                staticClass: "nav--item--icon",
                attrs: {
                    src: a(286)
                }
            }), t._v(" "), i("span", {
                staticClass: "nav--item--label text-center"
            }, [t._v("Shikisha "), i("br"), t._v(" Bet! "), t.numberOfBetBuilderMatches > 0 ? i("span", [t._v("(" + t._s(t.numberOfBetBuilderMatches) + ")")]) : t._e()])]), t._v(" "), i("div", {
                staticClass: "nav--item",
                class: {
                    selected: "/" == t.$route.path
                },
                on: {
                    click: function(e) {
                        t.goTo("/")
                    }
                }
            }, [i("img", {
                staticClass: "nav--item--icon",
                attrs: {
                    src: a(286)
                }
            }), t._v(" "), i("span", {
                staticClass: "nav--item--label text-center"
            }, [t._v("Highlights")])]), t._v(" "), i("div", {
                staticClass: "nav--item jackpots",
                class: {
                    selected: "/jackpots" == t.$route.path
                },
                on: {
                    click: function(e) {
                        t.goTo("/jackpots", "jackpots")
                    }
                }
            }, [i("svg", {
                staticClass: "nav--item--icon",
                staticStyle: {
                    "enable-background": "new 0 0 512 512"
                },
                attrs: {
                    version: "1.1",
                    id: "Layer_1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                    x: "0px",
                    y: "0px",
                    viewBox: "0 0 512 512",
                    "xml:space": "preserve"
                }
            }, [i("g", [i("g", [i("path", {
                attrs: {
                    d: "M508.796,92.805l-77.46-77.456c-2.052-2.051-4.834-3.204-7.736-3.204H88.4c-2.901,0-5.685,1.153-7.736,3.204L3.205,92.805\n                c-3.667,3.668-4.256,9.406-1.409,13.742l162.031,246.74c-6.471,13.409-10.1,28.438-10.1,44.298\n                c0,56.392,45.879,102.271,102.27,102.271s102.27-45.88,102.27-102.271c0-15.858-3.628-30.884-10.097-44.292l162.036-246.746\n                C513.052,102.211,512.463,96.473,508.796,92.805z M92.931,34.026h326.137l55.579,55.58h-77.453c-0.077,0-0.153-0.002-0.23,0\n                H115.037c-0.08-0.002-0.16,0-0.238,0H37.353L92.931,34.026z M376.905,111.485L256.169,295.317c-0.057,0-0.114-0.002-0.172-0.002\n                c-0.056,0-0.11,0.002-0.165,0.002L135.094,111.485H376.905z M31.216,111.485h77.701l122.674,186.779\n                c-21.899,5.383-41.045,17.834-54.874,34.79L31.216,111.485z M255.997,477.977c-44.327,0-80.389-36.063-80.389-80.391\n                s36.063-80.389,80.389-80.389c44.327,0,80.39,36.063,80.39,80.39C336.387,441.914,300.324,477.977,255.997,477.977z\n                M335.28,333.06c-13.827-16.957-32.974-29.409-54.872-34.793l122.674-186.782h77.701L335.28,333.06z"
                }
            })])]), t._v(" "), i("g", [i("g", [i("path", {
                attrs: {
                    d: "M259.713,357.546h-17.381c-6.042,0-10.94,4.899-10.94,10.94c0,6.041,4.898,10.94,10.94,10.94h6.44v59.161\n                c0,6.041,4.898,10.94,10.94,10.94c6.042,0,10.94-4.899,10.94-10.94v-70.101C270.653,362.444,265.755,357.546,259.713,357.546z"
                }
            })])])]), t._v(" "), i("span", {
                staticClass: "nav--item--label text-center"
            }, [t._v("Jackpot")])]), t._v(" "), i("div", {
                staticClass: "nav--item",
                class: {
                    selected: "/virtual" == t.$route.path
                },
                on: {
                    click: function(e) {
                        t.goTo("/virtual", "virtual")
                    }
                }
            }, [i("svg", {
                staticClass: "nav--item--icon",
                attrs: {
                    xmlns: "http://www.w3.org/2000/svg",
                    width: "15",
                    height: "15",
                    viewBox: "0 0 29 29"
                }
            }, [i("circle", {
                attrs: {
                    cx: "14.5",
                    cy: "14.5",
                    r: "14.5",
                    fill: "#ffd800"
                }
            }, [i("animate", {
                attrs: {
                    attributeName: "r",
                    begin: "0s",
                    dur: "1.1s",
                    repeatCount: "indefinite",
                    from: "40%",
                    to: "50%"
                }
            })])]), t._v(" "), i("span", {
                staticClass: "nav--item--label text-center"
            }, [t._v("Betika League")])]), t._v(" "), i("div", {
                staticClass: "nav--item",
                class: {
                    selected: "/betika-tv" == t.$route.path
                },
                on: {
                    click: function(e) {
                        t.goTo("/betika-tv", "betika-tv")
                    }
                }
            }, [i("img", {
                staticClass: "nav--item--icon",
                attrs: {
                    src: a(309)
                }
            }), t._v(" "), i("span", {
                staticClass: "nav--item--label"
            }, [t._v("BetikaTV")])]), t._v(" "), i("div", {
                staticClass: "nav--item",
                class: {
                    selected: "/sports" == t.$route.path
                },
                on: {
                    click: function(e) {
                        t.goTo("/sports", "sports")
                    }
                }
            }, [i("img", {
                staticClass: "nav--item--icon",
                attrs: {
                    src: a(287)
                }
            }), t._v(" "), i("span", {
                staticClass: "nav--item--label"
            }, [t._v("All Sports")])]), t._v(" "), i("div", {
                staticClass: "nav--item",
                class: {
                    selected: "/top-leagues" == t.$route.path
                },
                on: {
                    click: function(e) {
                        t.goTo("/top-leagues", "topLeagues")
                    }
                }
            }, [i("img", {
                staticClass: "nav--item--icon",
                attrs: {
                    src: a(287)
                }
            }), t._v(" "), i("span", {
                staticClass: "nav--item--label"
            }, [t._v("Top leagues")])]), t._v(" "), i("a", {
                staticClass: "nav--item lucky6",
                attrs: {
                    href: "https://lucky6.co.ke",
                    target: "_blank"
                }
            }, [i("svg", {
                attrs: {
                    width: "12px",
                    height: "17px",
                    viewBox: "0 0 12 17",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink"
                }
            }, [i("g", {
                attrs: {
                    id: "Page-1",
                    stroke: "none",
                    "stroke-width": "1",
                    fill: "none",
                    "fill-rule": "evenodd"
                }
            }, [i("g", {
                attrs: {
                    id: "Group-3",
                    transform: "translate(0.000000, 1.000000)",
                    stroke: "#4A4A4A"
                }
            }, [i("path", {
                attrs: {
                    d: "M6.94490244,2.57819635 C5.65458603,2.57819635 4.70420594,3.01569197 4.09373367,3.89069635 C3.48326139,4.76570072 3.17109275,5.94624447 3.15721838,7.43236301 C3.50407762,7.04347218 4.00354745,6.67889249 4.65564284,6.33861301 C5.30773823,5.99833353 6.02919464,5.82819635 6.82003373,5.82819635 C8.12422451,5.82819635 9.21681475,6.26221978 10.0978372,7.13027968 C10.9788597,7.99833958 11.4193644,9.15110583 11.4193644,10.588613 C11.4193644,12.0261202 10.9441743,13.1823586 9.99377999,14.057363 C9.04338565,14.9323674 7.84673919,15.369863 6.40380471,15.369863 C4.07291056,15.369863 2.46350779,14.3282068 1.57554811,12.244863 C1.07607079,11.0504126 0.826335878,9.70320385 0.826335878,8.20319635 C0.826335878,6.70318885 0.985888739,5.45320135 1.30499925,4.45319635 C1.62410976,3.45319135 2.06114585,2.6615326 2.61612065,2.07819635 C3.69832151,0.939301764 5.02677251,0.369863014 6.6015135,0.369863014 C8.17625449,0.369863014 9.60182461,0.862913639 10.8782667,1.84902968 L9.65039105,3.66152968 C9.33128054,3.38375051 8.90465006,3.13375301 8.37048682,2.91152968 C7.83632358,2.68930635 7.36113353,2.57819635 6.94490244,2.57819635 Z M4.3954997,8.69277968 C3.83358772,9.15805978 3.55263594,9.76222041 3.55263594,10.5052797 C3.55263594,11.248339 3.8231821,11.8872215 4.36428253,12.4219463 C4.90538295,12.9566712 5.58521688,13.2240297 6.40380471,13.2240297 C7.22239254,13.2240297 7.90222647,12.9844487 8.4433269,12.5052797 C8.98442733,12.0261106 9.25497348,11.4150056 9.25497348,10.6719463 C9.25497348,9.92888708 9.00523857,9.29694895 8.50576125,8.77611301 C8.00628393,8.25527708 7.33685562,7.99486301 6.49745624,7.99486301 C5.65805686,7.99486301 4.95741169,8.22749958 4.3954997,8.69277968 Z",
                    id: "6"
                }
            })])])]), t._v(" "), i("span", {
                staticClass: "nav--item--label text-center"
            }, [t._v("Lucky6")])]), t._v(" "), t._l(5, function(e, a) {
                return i("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: 0 == t.sports.length,
                        expression: "sports.length == 0"
                    }],
                    key: "dummy-" + a,
                    staticClass: "nav--item"
                })
            }), t._v(" "), t._l(t.sports, function(e, a) {
                return i("div", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.sports.length > 0,
                        expression: "sports.length > 0"
                    }],
                    key: a,
                    staticClass: "nav--item",
                    class: {
                        selected: t.$route.path == t.getSportUrl(e)
                    },
                    attrs: {
                        keyy: a
                    },
                    on: {
                        click: function(a) {
                            t.goTo(t.getSportUrl(e), t.getSportUrl(e))
                        }
                    }
                }, [i("img", {
                    staticClass: "nav--item--icon",
                    attrs: {
                        src: t.getImage(e.sport_id)
                    }
                }), t._v(" "), i("span", {
                    staticClass: "nav--item--label text-center"
                }, [t._v(t._s(e.sport_name))])])
            })], 2) : t._e()], 1)
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("footer", [a("nav", {
                staticClass: "nav",
                class: {
                    authenticated: t.authenticated,
                    virtual: t.virtual
                }
            }, [a("router-link", {
                staticClass: "nav--item",
                attrs: {
                    to: "/",
                    exact: ""
                }
            }, [a("div", [a("svg", {
                staticStyle: {
                    "enable-background": "new 0 0 512 512"
                },
                attrs: {
                    version: "1.1",
                    id: "Layer_1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                    x: "0px",
                    y: "0px",
                    viewBox: "0 0 512 512",
                    "xml:space": "preserve"
                }
            }, [a("g", [a("g", [a("path", {
                attrs: {
                    d: "M506.555,208.064L263.859,30.367c-4.68-3.426-11.038-3.426-15.716,0L5.445,208.064\n            c-5.928,4.341-7.216,12.665-2.875,18.593s12.666,7.214,18.593,2.875L256,57.588l234.837,171.943c2.368,1.735,5.12,2.57,7.848,2.57\n            c4.096,0,8.138-1.885,10.744-5.445C513.771,220.729,512.483,212.405,506.555,208.064z"
                }
            })])]), t._v(" "), a("g", [a("g", [a("path", {
                attrs: {
                    d: "M442.246,232.543c-7.346,0-13.303,5.956-13.303,13.303v211.749H322.521V342.009c0-36.68-29.842-66.52-66.52-66.52\n            s-66.52,29.842-66.52,66.52v115.587H83.058V245.847c0-7.347-5.957-13.303-13.303-13.303s-13.303,5.956-13.303,13.303v225.053\n            c0,7.347,5.957,13.303,13.303,13.303h133.029c6.996,0,12.721-5.405,13.251-12.267c0.032-0.311,0.052-0.651,0.052-1.036v-128.89\n            c0-22.009,17.905-39.914,39.914-39.914s39.914,17.906,39.914,39.914v128.89c0,0.383,0.02,0.717,0.052,1.024\n            c0.524,6.867,6.251,12.279,13.251,12.279h133.029c7.347,0,13.303-5.956,13.303-13.303V245.847\n            C455.549,238.499,449.593,232.543,442.246,232.543z"
                }
            })])])]), t._v(" "), a("p", [t._v("Home")])])]), t._v(" "), a("div", {
                staticClass: "nav--item",
                on: {
                    click: function(e) {
                        t.goTo("/live")
                    }
                }
            }, [a("div", {
                staticClass: "nav--item--container"
            }, [a("svg", {
                staticClass: "nav--item--icon",
                attrs: {
                    xmlns: "http://www.w3.org/2000/svg",
                    width: "15",
                    height: "15",
                    viewBox: "0 0 29 29"
                }
            }, [a("circle", {
                staticClass: "live",
                attrs: {
                    cx: "14.5",
                    cy: "14.5",
                    r: "14.5",
                    fill: "#c00"
                }
            }, [a("animate", {
                attrs: {
                    attributeName: "r",
                    begin: "0s",
                    dur: "1.1s",
                    repeatCount: "indefinite",
                    from: "40%",
                    to: "50%"
                }
            })])])]), t._v(" "), a("span", {
                staticClass: "nav--item--label text-center"
            }, [t._v("Live "), t.numberOfLiveMatches > 0 ? a("span", [t._v("(" + t._s(t.numberOfLiveMatches) + ")")]) : t._e()])]), t._v(" "), a("div", {
                staticClass: "nav__betslip__container",
                on: {
                    click: function(e) {
                        t.goTo(t.betslipUrl)
                    }
                }
            }, [a("div", {
                staticClass: "nav__betslip"
            }, [t._v(t._s(t.betslipCount))])]), t._v(" "), a("router-link", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.authenticated,
                    expression: "authenticated"
                }],
                staticClass: "nav--item",
                attrs: {
                    to: "/mybets",
                    exact: ""
                }
            }, [a("div", [a("svg", {
                staticStyle: {
                    "enable-background": "new 0 0 349.158 349.158"
                },
                attrs: {
                    version: "1.1",
                    id: "Capa_1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                    x: "0px",
                    y: "0px",
                    viewBox: "0 0 349.158 349.158",
                    "xml:space": "preserve"
                }
            }, [a("g", [a("g", [a("path", {
                attrs: {
                    d: "M341.658,6.227H83.455c-4.143,0-7.5,3.357-7.5,7.5V162.83H7.5c-4.143,0-7.5,3.357-7.5,7.5v127.123\n                c0,25.076,20.4,45.479,45.477,45.479c0.646,0,258.203,0,258.203,0c25.076,0,45.479-20.402,45.479-45.479V13.727\n                C349.158,9.584,345.801,6.227,341.658,6.227z M15,297.453V177.83h60.955v119.623c0,16.805-13.672,30.479-30.479,30.479\n                C28.672,327.932,15,314.258,15,297.453z M334.158,297.453c0,16.805-13.672,30.479-30.479,30.479H79.203\n                c7.299-8.07,11.752-18.766,11.752-30.479V21.227h243.203V297.453z"
                }
            }), t._v(" "), a("path", {
                attrs: {
                    d: "M202.023,92.855h96.127c4.143,0,7.5-3.357,7.5-7.5c0-4.143-3.357-7.5-7.5-7.5h-96.127c-4.143,0-7.5,3.357-7.5,7.5\n                C194.523,89.498,197.881,92.855,202.023,92.855z"
                }
            }), t._v(" "), a("path", {
                attrs: {
                    d: "M202.023,175.248h96.127c4.143,0,7.5-3.357,7.5-7.5c0-4.143-3.357-7.5-7.5-7.5h-96.127c-4.143,0-7.5,3.357-7.5,7.5\n                C194.523,171.891,197.881,175.248,202.023,175.248z"
                }
            }), t._v(" "), a("path", {
                attrs: {
                    d: "M202.023,257.643h96.127c4.143,0,7.5-3.357,7.5-7.5c0-4.143-3.357-7.5-7.5-7.5h-96.127c-4.143,0-7.5,3.357-7.5,7.5\n                C194.523,254.285,197.881,257.643,202.023,257.643z"
                }
            }), t._v(" "), a("path", {
                attrs: {
                    d: "M132.814,101.295c1.418,1.5,3.391,2.35,5.453,2.35c0.02,0,0.041,0,0.063,0c2.086-0.018,4.07-0.902,5.477-2.443\n                l41.197-45.121c2.793-3.059,2.576-7.803-0.482-10.596c-3.059-2.791-7.803-2.576-10.596,0.482l-35.752,39.156l-11.129-11.785\n                c-2.846-3.012-7.594-3.146-10.604-0.303c-3.012,2.844-3.146,7.59-0.303,10.602L132.814,101.295z"
                }
            }), t._v(" "), a("path", {
                attrs: {
                    d: "M173.926,131.941l-35.752,39.154l-11.131-11.783c-2.844-3.014-7.592-3.148-10.602-0.303\n                c-3.012,2.844-3.146,7.59-0.303,10.602l16.676,17.656c1.418,1.5,3.391,2.35,5.453,2.35c0.02,0,0.041,0,0.063,0\n                c2.086-0.018,4.07-0.902,5.477-2.443l41.197-45.119c2.791-3.059,2.576-7.803-0.482-10.596S176.719,128.881,173.926,131.941z"
                }
            }), t._v(" "), a("path", {
                attrs: {
                    d: "M173.926,217.914l-35.752,39.154l-11.131-11.783c-2.844-3.014-7.592-3.149-10.602-0.303\n                c-3.012,2.844-3.146,7.59-0.303,10.602l16.676,17.656c1.418,1.5,3.391,2.35,5.453,2.35c0.02,0,0.041,0,0.063,0\n                c2.086-0.018,4.07-0.902,5.477-2.443l41.197-45.119c2.791-3.059,2.576-7.803-0.482-10.596\n                C181.463,214.639,176.719,214.855,173.926,217.914z"
                }
            })])])]), t._v(" "), a("p", [t._v("My Bets")])])]), t._v(" "), a("router-link", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: !t.authenticated,
                    expression: "!authenticated"
                }],
                staticClass: "nav--item nav--item--jackpot",
                attrs: {
                    to: "/jackpots",
                    exact: ""
                }
            }, [a("div", {
                staticClass: "nav--item--container"
            }, [a("svg", {
                staticStyle: {
                    "enable-background": "new 0 0 512 512"
                },
                attrs: {
                    version: "1.1",
                    id: "Layer_1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                    x: "0px",
                    y: "0px",
                    viewBox: "0 0 512 512",
                    "xml:space": "preserve"
                }
            }, [a("g", [a("g", [a("path", {
                attrs: {
                    d: "M508.796,92.805l-77.46-77.456c-2.052-2.051-4.834-3.204-7.736-3.204H88.4c-2.901,0-5.685,1.153-7.736,3.204L3.205,92.805\n                          c-3.667,3.668-4.256,9.406-1.409,13.742l162.031,246.74c-6.471,13.409-10.1,28.438-10.1,44.298\n                          c0,56.392,45.879,102.271,102.27,102.271s102.27-45.88,102.27-102.271c0-15.858-3.628-30.884-10.097-44.292l162.036-246.746\n                          C513.052,102.211,512.463,96.473,508.796,92.805z M92.931,34.026h326.137l55.579,55.58h-77.453c-0.077,0-0.153-0.002-0.23,0\n                          H115.037c-0.08-0.002-0.16,0-0.238,0H37.353L92.931,34.026z M376.905,111.485L256.169,295.317c-0.057,0-0.114-0.002-0.172-0.002\n                          c-0.056,0-0.11,0.002-0.165,0.002L135.094,111.485H376.905z M31.216,111.485h77.701l122.674,186.779\n                          c-21.899,5.383-41.045,17.834-54.874,34.79L31.216,111.485z M255.997,477.977c-44.327,0-80.389-36.063-80.389-80.391\n                          s36.063-80.389,80.389-80.389c44.327,0,80.39,36.063,80.39,80.39C336.387,441.914,300.324,477.977,255.997,477.977z\n                          M335.28,333.06c-13.827-16.957-32.974-29.409-54.872-34.793l122.674-186.782h77.701L335.28,333.06z"
                }
            })])]), t._v(" "), a("g", [a("g", [a("path", {
                attrs: {
                    d: "M259.713,357.546h-17.381c-6.042,0-10.94,4.899-10.94,10.94c0,6.041,4.898,10.94,10.94,10.94h6.44v59.161\n                          c0,6.041,4.898,10.94,10.94,10.94c6.042,0,10.94-4.899,10.94-10.94v-70.101C270.653,362.444,265.755,357.546,259.713,357.546z"
                }
            })])])]), t._v(" "), a("p", [t._v("Jackpot")])])]), t._v(" "), a("router-link", {
                staticClass: "nav--item",
                attrs: {
                    to: "/profile",
                    exact: ""
                }
            }, [a("div", [a("svg", {
                staticStyle: {
                    "enable-background": "new 0 0 482.9 482.9"
                },
                attrs: {
                    version: "1.1",
                    id: "Capa_1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                    x: "0px",
                    y: "0px",
                    viewBox: "0 0 482.9 482.9",
                    "xml:space": "preserve"
                }
            }, [a("g", [a("g", [a("path", {
                attrs: {
                    d: "M239.7,260.2c0.5,0,1,0,1.6,0c0.2,0,0.4,0,0.6,0c0.3,0,0.7,0,1,0c29.3-0.5,53-10.8,70.5-30.5\n              c38.5-43.4,32.1-117.8,31.4-124.9c-2.5-53.3-27.7-78.8-48.5-90.7C280.8,5.2,262.7,0.4,242.5,0h-0.7c-0.1,0-0.3,0-0.4,0h-0.6\n              c-11.1,0-32.9,1.8-53.8,13.7c-21,11.9-46.6,37.4-49.1,91.1c-0.7,7.1-7.1,81.5,31.4,124.9C186.7,249.4,210.4,259.7,239.7,260.2z\n              M164.6,107.3c0-0.3,0.1-0.6,0.1-0.8c3.3-71.7,54.2-79.4,76-79.4h0.4c0.2,0,0.5,0,0.8,0c27,0.6,72.9,11.6,76,79.4\n              c0,0.3,0,0.6,0.1,0.8c0.1,0.7,7.1,68.7-24.7,104.5c-12.6,14.2-29.4,21.2-51.5,21.4c-0.2,0-0.3,0-0.5,0l0,0c-0.2,0-0.3,0-0.5,0\n              c-22-0.2-38.9-7.2-51.4-21.4C157.7,176.2,164.5,107.9,164.6,107.3z"
                }
            }), t._v(" "), a("path", {
                attrs: {
                    d: "M446.8,383.6c0-0.1,0-0.2,0-0.3c0-0.8-0.1-1.6-0.1-2.5c-0.6-19.8-1.9-66.1-45.3-80.9c-0.3-0.1-0.7-0.2-1-0.3\n              c-45.1-11.5-82.6-37.5-83-37.8c-6.1-4.3-14.5-2.8-18.8,3.3c-4.3,6.1-2.8,14.5,3.3,18.8c1.7,1.2,41.5,28.9,91.3,41.7\n              c23.3,8.3,25.9,33.2,26.6,56c0,0.9,0,1.7,0.1,2.5c0.1,9-0.5,22.9-2.1,30.9c-16.2,9.2-79.7,41-176.3,41\n              c-96.2,0-160.1-31.9-176.4-41.1c-1.6-8-2.3-21.9-2.1-30.9c0-0.8,0.1-1.6,0.1-2.5c0.7-22.8,3.3-47.7,26.6-56\n              c49.8-12.8,89.6-40.6,91.3-41.7c6.1-4.3,7.6-12.7,3.3-18.8c-4.3-6.1-12.7-7.6-18.8-3.3c-0.4,0.3-37.7,26.3-83,37.8\n              c-0.4,0.1-0.7,0.2-1,0.3c-43.4,14.9-44.7,61.2-45.3,80.9c0,0.9,0,1.7-0.1,2.5c0,0.1,0,0.2,0,0.3c-0.1,5.2-0.2,31.9,5.1,45.3\n              c1,2.6,2.8,4.8,5.2,6.3c3,2,74.9,47.8,195.2,47.8s192.2-45.9,195.2-47.8c2.3-1.5,4.2-3.7,5.2-6.3\n              C447,415.5,446.9,388.8,446.8,383.6z"
                }
            })])])]), t._v(" "), a("p", [t._v("Profile")])])])], 1)])
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "matches"
            }, [a("div", {
                staticClass: "filters"
            }, [a("div", {
                staticClass: "tab-filter"
            }, t._l(t.tabs, function(e, i) {
                return a("button", {
                    key: i,
                    staticClass: "tab-filter__item",
                    class: {
                        active: e.id == t.selectedTab.id
                    },
                    on: {
                        click: function(a) {
                            t.filterTab(e)
                        }
                    }
                }, [t._v(t._s(e.name))])
            })), t._v(" "), a("div", {
                staticClass: "selected-filters"
            }, [a("div", {
                staticClass: "filter-div"
            }, [a("button", {
                staticClass: "filter-button selected-sport",
                on: {
                    click: function(e) {
                        t.showSportFilter = !0
                    }
                }
            }, [t._v("\n            " + t._s(t.truncate(t.selectedSport.sport_name)) + "\n            "), a("span", {
                staticClass: "filter-button__icon"
            }, [a("svg", {
                attrs: {
                    width: "10px",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    viewBox: "0 0 129 129",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                    "enable-background": "new 0 0 129 129"
                }
            }, [a("g", [a("path", {
                attrs: {
                    d: "m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"
                }
            })])])])])]), t._v(" "), a("div", {
                staticClass: "filter-div"
            }, [a("button", {
                staticClass: "filter-button selected-market",
                on: {
                    click: function(e) {
                        t.selectedMarket && (t.showMarketFilter = !0)
                    }
                }
            }, [t._v("\n            " + t._s(t.selectedMarket ? t.truncate(t.selectedMarket.name) : "None") + "\n            "), a("span", {
                staticClass: "filter-button__icon"
            }, [a("svg", {
                attrs: {
                    width: "10px",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    viewBox: "0 0 129 129",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                    "enable-background": "new 0 0 129 129"
                }
            }, [a("g", [a("path", {
                attrs: {
                    d: "m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"
                }
            })])])])])]), t._v(" "), a("div", {
                staticClass: "filter-div"
            }, [a("button", {
                staticClass: "filter-button selected-tag",
                on: {
                    click: function(e) {
                        t.showTagFilter = !0
                    }
                }
            }, [t._v("\n            " + t._s(t.truncate(t.selectedTag.tag_title)) + "\n            "), a("span", {
                staticClass: "filter-button__icon"
            }, [a("svg", {
                attrs: {
                    width: "10px",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    viewBox: "0 0 129 129",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                    "enable-background": "new 0 0 129 129"
                }
            }, [a("g", [a("path", {
                attrs: {
                    d: "m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"
                }
            })])])])])])])]), t._v(" "), 0 != t.matches.length || t.loading || t.filtering ? t._e() : a("div", {
                staticClass: "matches--empty"
            }, [t._v("\n      There are currently no matches for the selected options"), a("br"), t._v("\n      (" + t._s(t.selectedTab.name) + " - " + t._s(t.selectedSport.sport_name) + " - " + t._s(t.selectedTag.tag_title) + ")  \n    ")]), t._v(" "), a("loader", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.loading || t.filtering,
                    expression: "loading || filtering"
                }]
            }), t._v(" "), a("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: !t.loading && !t.filtering,
                    expression: "!loading && !filtering"
                }, {
                    name: "infinite-scroll",
                    rawName: "v-infinite-scroll",
                    value: t.loadMore,
                    expression: "loadMore"
                }],
                staticClass: "matches",
                attrs: {
                    "infinite-scroll-disabled": "loadingMore",
                    "infinite-scroll-immediate-check": "false"
                }
            }, [t._l(t.matches, function(e, i) {
                return a("match", {
                    key: i,
                    attrs: {
                        match: e,
                        selectedMarket: t.selectedMarket,
                        live: !1
                    }
                })
            }), t._v(" "), a("loader", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.loadingMore,
                    expression: "loadingMore"
                }]
            }), t._v(" "), a("button", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.page * t.limit > t.total && "-2" != t.selectedTab.id,
                    expression: "page * limit > total && selectedTab.id != '-2'"
                }],
                staticClass: "matches__more",
                on: {
                    click: function(e) {
                        t.filterTab({
                            id: "-2",
                            name: "Upcoming"
                        })
                    }
                }
            }, [t._v("See all upcoming " + t._s("-1" == t.selectedSport.sport_id ? "" : t.selectedSport.sport_name) + " matches")])], 2), t._v(" "), a("modal", {
                staticClass: "markets-filter__modal__container",
                attrs: {
                    show: t.showSportFilter,
                    size: "max"
                },
                on: {
                    close: t.closeSportFilter
                }
            }, [a("sport-filter", {
                attrs: {
                    sports: t.sports,
                    selectedSport: t.selectedSport
                },
                on: {
                    selectSport: t.filterSport
                }
            })], 1), t._v(" "), a("modal", {
                staticClass: "markets-filter__modal__container",
                attrs: {
                    show: t.showMarketFilter,
                    size: "max"
                },
                on: {
                    close: t.closeMarketFilter
                }
            }, [a("market-filter", {
                attrs: {
                    markets: t.markets,
                    selectedMarket: t.selectedMarket
                },
                on: {
                    selectMarket: t.filterMarket
                }
            })], 1), t._v(" "), a("modal", {
                staticClass: "markets-filter__modal__container",
                attrs: {
                    show: t.showTagFilter,
                    size: "max"
                },
                on: {
                    close: t.closeTagFilter
                }
            }, [a("tag-filter", {
                attrs: {
                    tags: t.tags,
                    selectedTag: t.selectedTag
                },
                on: {
                    selectTag: t.filterTag
                }
            })], 1)], 1)
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return t.betslipCount > 0 ? a("div", {
                staticClass: "fab betslip-fab",
                on: {
                    click: function(e) {
                        t.toBetslip()
                    }
                }
            }, [t._v("\n   " + t._s(t.betslipCount) + "\n ")]) : t._e()
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "prebet-match"
            }, [a("div", {
                staticClass: "time"
            }, [a("span", {
                staticClass: "pull-left",
                staticStyle: {
                    width: "70%",
                    "text-align": "left"
                }
            }, [t.match.sport_name ? a("span", [a("img", {
                staticStyle: {
                    height: "10px",
                    "margin-right": "2px"
                },
                attrs: {
                    src: t.getSportIcon(t.match.sport_name.split(" ").join("").toLowerCase())
                }
            }), t._v(" \n        " + t._s(t.match.sport_name) + " - \n      ")]) : t._e(), t._v("\n      " + t._s(t.match.competition_name) + ", " + t._s(t.match.category) + "\n      "), t.upcoming ? a("span", [t._v("Upcoming")]) : t._e()]), t._v("\n    " + t._s(t._f("betterTime")(t.match.start_time)) + "\n  ")]), t._v(" "), a("div", {
                staticClass: "details",
                on: {
                    click: function(e) {
                        t.openMarkets()
                    }
                }
            }, [a("div", {
                staticClass: "teams bold upper"
            }, [t._v(t._s(t.match.home_team) + " vs. " + t._s(t.match.away_team))]), t._v(" "), t.upcoming ? t._e() : a("div", {
                staticClass: "markets bold",
                class: {
                    selected: t.isMyMatchSelected
                }
            }, [t._v("+" + t._s(t.match.side_bets))])]), t._v(" "), !t.upcoming && t.match.odds ? a("div", {
                staticClass: "odds",
                attrs: {
                    "number-of-odds": t.odds.length
                }
            }, t._l(t.odds, function(e, i) {
                return a("match-odd", {
                    key: i,
                    staticClass: "match-odd",
                    attrs: {
                        odd: e,
                        match: t.match,
                        market: t.match.odds[0]
                    }
                })
            })) : t._e()])
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "mini-betslip",
                class: {
                    "mini-betslip--show": t.betslipCount > 0
                }
            }, [a("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.showPlaceBet,
                    expression: "showPlaceBet"
                }],
                staticClass: "mini-betslip__placebet"
            }, [a("div", {
                staticClass: "mini-betslip__stake"
            }, [a("button", {
                staticClass: "mini-betslip__stake__button",
                on: {
                    click: function(e) {
                        t.setStake(0)
                    }
                }
            }, [t._v("Clear")]), t._v(" "), a("button", {
                staticClass: "mini-betslip__stake__button",
                on: {
                    click: function(e) {
                        t.setStake(t.stake + 5)
                    }
                }
            }, [t._v("+5")]), t._v(" "), a("button", {
                staticClass: "mini-betslip__stake__button",
                on: {
                    click: function(e) {
                        t.setStake(t.stake + 10)
                    }
                }
            }, [t._v("+10")]), t._v(" "), a("button", {
                staticClass: "mini-betslip__stake__button",
                on: {
                    click: function(e) {
                        t.setStake(t.stake + 50)
                    }
                }
            }, [t._v("+50")]), t._v(" "), a("button", {
                staticClass: "mini-betslip__stake__button",
                on: {
                    click: function(e) {
                        t.setStake(t.stake + 100)
                    }
                }
            }, [t._v("+100")]), t._v(" "), a("button", {
                staticClass: "mini-betslip__stake__button",
                on: {
                    click: function(e) {
                        t.setStake(t.stake + 1e3)
                    }
                }
            }, [t._v("+1000")])]), t._v(" "), t.placingBet ? t._e() : a("div", {
                staticClass: "mini-betslip__placebet__button__container"
            }, [t.areThereAnyInvalidMatches ? a("button", {
                staticClass: "mini-betslip__placebet__button",
                on: {
                    click: t.removeInvalidMatches
                }
            }, [t._v("Remove Locked")]) : a("button", {
                staticClass: "mini-betslip__placebet__button",
                on: {
                    click: t.placeBet
                }
            }, [t._v("Place Bet (Ksh. " + t._s(t.stake) + ")")]), t._v(" "), a("span", {
                staticClass: "mini-betslip__clear",
                on: {
                    click: function(e) {
                        return e.stopPropagation(),
                            t.clear(e)
                    }
                }
            }, [a("svg", {
                staticStyle: {
                    "enable-background": "new 0 0 268.476 268.476"
                },
                attrs: {
                    width: "14px",
                    xmlns: "http://www.w3.org/2000/svg",
                    x: "0px",
                    y: "0px",
                    viewBox: "0 0 268.476 268.476",
                    "xml:space": "preserve"
                }
            }, [a("g", [a("path", {
                staticStyle: {
                    "fill-rule": "evenodd",
                    "clip-rule": "evenodd"
                },
                attrs: {
                    d: "M63.119,250.254c0,0,3.999,18.222,24.583,18.222h93.072   c20.583,0,24.582-18.222,24.582-18.222l18.374-178.66H44.746L63.119,250.254z M170.035,98.442c0-4.943,4.006-8.949,8.949-8.949   c4.943,0,8.95,4.006,8.95,8.949l-8.95,134.238c0,4.943-4.007,8.949-8.949,8.949c-4.942,0-8.949-4.007-8.949-8.949L170.035,98.442z    M125.289,98.442c0-4.943,4.007-8.949,8.949-8.949c4.943,0,8.949,4.006,8.949,8.949v134.238c0,4.943-4.006,8.949-8.949,8.949   c-4.943,0-8.949-4.007-8.949-8.949V98.442z M89.492,89.492c4.943,0,8.949,4.006,8.949,8.949l8.95,134.238   c0,4.943-4.007,8.949-8.95,8.949c-4.942,0-8.949-4.007-8.949-8.949L80.543,98.442C80.543,93.499,84.55,89.492,89.492,89.492z    M218.36,35.811h-39.376V17.899C178.984,4.322,174.593,0,161.086,0L107.39,0C95.001,0,89.492,6.001,89.492,17.899v17.913H50.116   c-7.914,0-14.319,6.007-14.319,13.43c0,7.424,6.405,13.431,14.319,13.431H218.36c7.914,0,14.319-6.007,14.319-13.431   C232.679,41.819,226.274,35.811,218.36,35.811z M161.086,35.811h-53.695l0.001-17.913h53.695V35.811z",
                    fill: "#D80027"
                }
            })])])])]), t._v(" "), t.placingBet ? a("button", {
                staticClass: "mini-betslip__placebet__button"
            }, [a("loader", {
                attrs: {
                    margin: !1
                }
            })], 1) : t._e()]), t._v(" "), a("p", {
                staticClass: "mini-betslip__info",
                on: {
                    click: function(e) {
                        return e.stopPropagation(),
                            t.goToBetslip(e)
                    }
                }
            }, [a("span", {
                staticClass: "pull-left"
            }, [a("span", {
                staticClass: "mini-betslip__info__text"
            }, [t._v("Total Odds")]), t._v(" "), a("span", {
                staticClass: "mini-betslip__info__num"
            }, [t._v(t._s(Math.round(100 * t.totalOdds) / 100))])]), t._v(" "), a("span", {
                staticClass: "pull-right"
            }, [a("span", {
                staticClass: "mini-betslip__info__text"
            }, [t._v("Possible Win")]), t._v(" "), a("span", {
                staticClass: "mini-betslip__info__num"
            }, [t._v("Ksh. " + t._s(Math.round(100 * t.possibleWinnings, 2) / 100))])])])])
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this, e = t.$createElement, a;
            return (t._self._c || e)("div", {
                staticClass: "spinner"
            })
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "search",
                class: {
                    hidden: !t.show
                }
            }, [a("span", {
                staticClass: "close",
                on: {
                    click: function(e) {
                        t.close()
                    }
                }
            }, [t._v("×")]), t._v(" "), a("div", {
                staticClass: "input"
            }, [a("input", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.keyword,
                    expression: "keyword"
                }],
                ref: "search",
                staticClass: "search--input",
                attrs: {
                    type: "text",
                    id: "search",
                    placeholder: "Search games"
                },
                domProps: {
                    value: t.keyword
                },
                on: {
                    change: function(e) {
                        t.search()
                    },
                    input: function(e) {
                        e.target.composing || (t.keyword = e.target.value)
                    }
                }
            }), t._v(" "), a("svg", {
                staticClass: "search--icon",
                attrs: {
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink",
                    width: "20",
                    height: "20",
                    viewBox: "0 0 32 32"
                }
            }, [a("image", {
                attrs: {
                    "data-name": "Vector Smart Object",
                    width: "32",
                    height: "32",
                    "xlink:href": "data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAQAAADZc7J/AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfgDBQKLC2xYi7mAAACEklEQVRIx53VP0iUYRwH8M9pHSpiQ0MYFwl6SwVao1acIVJSjYpBQxGCLVkk6apDCYKzRBTVEIItIoTX4GBggzgkYkN1plsWnZCKmjU4+N4f9c7f+DzP9/M8z++F5w1dllYRLRqcFhGyKWHKmGFJu1QoBYjq1SyUsWrFUz1+ZQMKdigPzWhBXLtzjipyTJ1u00rc89m1vU4Q9kIrRnSZzVgVM6AGnfrTpwqrtnd/pdWGOx75kWWbhGdK1GqUNJkNeOiBDU3e7tYqW+LWNGg04Vv6FaJmhN3ywn41qE3CKaupTewVNpJDnA4JFdpTTxDxHWeytC5b3fTSggpbOydoEfI+xzhvLDmhLniFBvZoXnptGEV9EDiNjzkDTKI6CEQwnwfwFeVBIIQ/eQDrKAkCmziSB1CK5SCQQFUeQBQLQWAKF/MALmAqCIyhOed4mSuIB4FhK86K5QjcVWTapyCQ9BQDDuUQP64bfcGhAvRYUuPxvvGw18pMGAoOFlaxak6rWms+7Bl/7rqkq36mn4ARnXhiUPEu8XLv3MDv4FuwA9Dvvi1tZt10OKPzXebUS5p30rhIcDL4rF/yTAWWjJr01bpSUec1KcKE21aNq/RFzGI2gGLtOpzIuMC0PkP+IZJOhDL+TAXq1KtWrsSyBVPiwe+eTmQC+1cKUZB/3qKYLyq323kQIIU4GLBDxA8KbBNz/v4HQWaTbCwAiOUAAAAASUVORK5CYII="
                }
            })])]), t._v(" "), a("div", {
                staticClass: "search--results"
            }, [a("p", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.keyword.length > 0,
                    expression: "(keyword.length > 0)"
                }],
                staticClass: "search--term"
            }, [t._v("\n            Search term \n            "), a("span", {
                staticClass: "theme-color italicize"
            }, [t._v(t._s(t.keyword))])]), t._v(" "), a("p", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.lastKeyword.length > 0 && t.keyword.length <= 0,
                    expression: "(lastKeyword.length > 0) && (keyword.length <= 0)"
                }],
                staticClass: "search--term"
            }, [t._v("\n            Your previous search\n            "), a("span", {
                staticClass: "previous theme-color italicize"
            }, [t._v(t._s(t.lastKeyword))])]), t._v(" "), a("spinner", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: !t.loaded,
                    expression: "!loaded"
                }]
            }), t._v(" "), t._l(t.results, function(e, i) {
                return a("match", {
                    directives: [{
                        name: "show",
                        rawName: "v-show",
                        value: t.results.length > 0 && t.loaded && (t.keyword.length > 0 || t.lastKeyword.length > 0),
                        expression: "(results.length > 0) && loaded && (keyword.length > 0 || lastKeyword.length > 0)"
                    }],
                    key: i,
                    attrs: {
                        match: e,
                        selectedMarket: t.selectedMarket
                    }
                })
            }), t._v(" "), a("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: 0 == t.results.length && t.loaded && (t.keyword.length > 0 || t.lastKeyword.length > 0),
                    expression: "(results.length == 0) && loaded && (keyword.length > 0 || lastKeyword.length > 0)"
                }],
                staticClass: "search--results--empty"
            }, [t._v("\n            No games matching that keyword\n        ")])], 2)])
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "snackbar",
                class: {
                    show: t.show,
                    success: "success" == t.status,
                    info: "info" == t.status,
                    error: "error" == t.status,
                    warning: "warning" == t.status,
                    default: "default" == t.status
                },
                on: {
                    click: function(e) {
                        t.close()
                    }
                }
            }, [a("span", {
                staticClass: "close",
                on: {
                    click: function(e) {
                        t.close()
                    }
                }
            }, [t._v("×")]), t._v(" "), t._l(t.messages, function(e, i) {
                return a("p", {
                    key: i,
                    staticClass: "message"
                }, [t._v(t._s(e))])
            })], 2)
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , i = t._self._c || e;
            return i("swiper", {
                staticClass: "slider",
                class: {
                    "logged-in": t.loggedIn
                },
                attrs: {
                    options: t.swiperOption
                }
            }, [i("swiper-slide", [i("img", {
                staticClass: "slider__item",
                attrs: {
                    src: a(669)
                }
            })]), t._v(" "), i("swiper-slide", [i("router-link", {
                attrs: {
                    to: "/jackpots"
                }
            }, [i("img", {
                staticClass: "slider__item",
                attrs: {
                    src: a(367)
                }
            })])], 1), t._v(" "), i("swiper-slide", [i("img", {
                staticClass: "slider__item",
                attrs: {
                    src: a(670)
                }
            })]), t._v(" "), i("swiper-slide", [i("img", {
                staticClass: "slider__item",
                attrs: {
                    src: a(671)
                }
            })]), t._v(" "), i("div", {
                staticClass: "swiper-pagination",
                attrs: {
                    slot: "pagination"
                },
                slot: "pagination"
            })], 1)
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "markets-filter__modal"
            }, [a("h3", {
                staticClass: "markets-filter__modal__title"
            }, [t._v("Please select a market")]), t._v(" "), a("div", {
                staticClass: "markets-filter__modal__buttons"
            }, t._l(t.markets, function(e, i) {
                return a("button", {
                    key: i,
                    staticClass: "markets-filter__modal__button",
                    class: {
                        "markets-filter__modal__button--selected": t.selectedMarket.sub_type_id == e.sub_type_id
                    },
                    on: {
                        click: function(a) {
                            t.$emit("selectMarket", e)
                        }
                    }
                }, [t._v(t._s(e.name))])
            }))])
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "markets-filter__modal"
            }, [a("h3", {
                staticClass: "markets-filter__modal__title"
            }, [t._v("Please select a tag")]), t._v(" "), a("div", {
                staticClass: "markets-filter__modal__buttons"
            }, t._l(t.tags, function(e, i) {
                return a("button", {
                    key: i,
                    staticClass: "markets-filter__modal__button",
                    class: {
                        "markets-filter__modal__button--selected": t.selectedTag ? t.selectedTag.tag_id == e.tag_id : ""
                    },
                    on: {
                        click: function(a) {
                            t.$emit("selectTag", e)
                        }
                    }
                }, [t._v(t._s(e.tag_title))])
            }))])
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "markets-filter__modal"
            }, [a("h3", {
                staticClass: "markets-filter__modal__title"
            }, [t._v("Please select a sport")]), t._v(" "), a("div", {
                staticClass: "markets-filter__modal__buttons"
            }, t._l(t.sports, function(e, i) {
                return a("button", {
                    key: i,
                    staticClass: "markets-filter__modal__button",
                    class: {
                        "markets-filter__modal__button--selected": t.selectedSport.sport_id == e.sport_id
                    },
                    on: {
                        click: function(a) {
                            t.$emit("selectSport", e)
                        }
                    }
                }, [t._v(t._s(e.sport_name))])
            }))])
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "markets-filter__modal"
            }, [a("h3", {
                staticClass: "markets-filter__modal__title bold"
            }, [t._v("Select a sport")]), t._v(" "), a("select", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.tempSport,
                    expression: "tempSport"
                }],
                on: {
                    change: function(e) {
                        var a = Array.prototype.filter.call(e.target.options, function(t) {
                            return t.selected
                        }).map(function(t) {
                            var e;
                            return "_value"in t ? t._value : t.value
                        });
                        t.tempSport = e.target.multiple ? a : a[0]
                    }
                }
            }, t._l(t.sports, function(e) {
                return a("option", {
                    key: e.sport_id,
                    domProps: {
                        value: e
                    }
                }, [t._v(t._s(e.sport_name))])
            })), t._v(" "), t.loading ? a("spinner") : t._e(), t._v(" "), !t.loading && t.markets.length > 0 ? a("div", [a("h3", {
                staticClass: "markets-filter__modal__title bold"
            }, [t._v("Select a market")]), t._v(" "), a("select", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.tempMarket,
                    expression: "tempMarket"
                }],
                on: {
                    change: function(e) {
                        var a = Array.prototype.filter.call(e.target.options, function(t) {
                            return t.selected
                        }).map(function(t) {
                            var e;
                            return "_value"in t ? t._value : t.value
                        });
                        t.tempMarket = e.target.multiple ? a : a[0]
                    }
                }
            }, t._l(t.markets, function(e) {
                return a("option", {
                    key: e.sub_type_id,
                    domProps: {
                        value: e
                    }
                }, [t._v(t._s(e.name))])
            }))]) : t._e(), t._v(" "), !t.loading && t.tags.length > 0 ? a("div", [a("h3", {
                staticClass: "markets-filter__modal__title bold"
            }, [t._v("Select a tag")]), t._v(" "), a("select", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.tempTag,
                    expression: "tempTag"
                }],
                on: {
                    change: function(e) {
                        var a = Array.prototype.filter.call(e.target.options, function(t) {
                            return t.selected
                        }).map(function(t) {
                            var e;
                            return "_value"in t ? t._value : t.value
                        });
                        t.tempTag = e.target.multiple ? a : a[0]
                    }
                }
            }, t._l(t.tags, function(e) {
                return a("option", {
                    key: e.tag_id,
                    domProps: {
                        value: e
                    }
                }, [t._v(t._s(e.tag_title))])
            }))]) : t._e(), t._v(" "), a("br"), t._v(" "), a("button", {
                staticClass: "markets-filter__modal__button--selected",
                on: {
                    click: function(e) {
                        t.$emit("submitFilters")
                    }
                }
            }, [t._v("FILTER")])], 1)
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("button", {
                staticClass: "prebet-match-button",
                class: {
                    selected: t.amISelected
                },
                on: {
                    click: function(e) {
                        t.addToBetslip()
                    }
                }
            }, [a("span", {
                staticClass: "prebet-match-button__odd-key upper"
            }, [t._v(t._s(t.odd.display) + " "), t.odd.special_bet_value ? a("span", [t._v("(" + t._s(t.odd.special_bet_value) + ")")]) : t._e()]), t._v(" "), t.odd.odd_value ? a("span", {
                staticClass: "prebet-match-button__odd-value bold"
            }, [t._v(t._s(t.odd.odd_value))]) : a("svg", {
                attrs: {
                    height: "12px",
                    viewBox: "474 282 14 18",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink"
                }
            }, [a("path", {
                attrs: {
                    d: "M481.546303,294.437432 C481.600488,294.236761 481.631332,294.005954 481.631332,293.760388 C481.631332,292.986816 481.325249,292.359712 480.947677,292.359712 C480.570105,292.359712 480.264023,292.986816 480.264023,293.760388 C480.264023,293.999364 480.293234,294.224361 480.344739,294.421211 C480.084967,294.603206 479.915493,294.902267 479.915493,295.240346 C479.915493,295.794983 480.371616,296.244604 480.934272,296.244604 C481.496929,296.244604 481.953052,295.794983 481.953052,295.240346 C481.953052,294.912128 481.79332,294.620685 481.546303,294.437432 L481.546303,294.437432 Z M476.640956,289.518369 C475.051639,290.533867 474,292.297062 474,294.302158 C474,297.448795 476.589591,300 479.784014,300 L482.215986,300 C485.409741,300 488,297.448989 488,294.302158 C488,292.308265 486.960219,290.553451 485.385637,289.535322 C485.510499,287.87551 485.634902,282 481.137977,282 C476.656233,282 476.57209,287.835909 476.640956,289.518369 Z M478.233024,288.811553 C478.726641,288.676497 479.246789,288.604317 479.784014,288.604317 L482.215986,288.604317 C482.79807,288.604317 483.360072,288.689026 483.889982,288.846612 C483.71666,286.397425 483.357903,283.683453 481.137977,283.683453 C478.928835,283.683453 478.441091,286.378149 478.233024,288.811553 Z",
                    id: "Combined-Shape",
                    stroke: "none",
                    fill: "#D8D8D8",
                    "fill-rule": "evenodd"
                }
            })])])
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("div", {
                staticClass: "container"
            }, ["STOPPED" == t.bet.status || !Number(t.bet.odd.odd_value) || t.bet.live && !t.bet.active ? a("div", {
                staticClass: "wrapper",
                class: {
                    inactive: t.bet.live && !t.bet.active
                }
            }, [a("svg", {
                attrs: {
                    height: "40px",
                    viewBox: "474 282 14 18",
                    version: "1.1",
                    xmlns: "http://www.w3.org/2000/svg",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink"
                }
            }, [a("path", {
                attrs: {
                    d: "M481.546303,294.437432 C481.600488,294.236761 481.631332,294.005954 481.631332,293.760388 C481.631332,292.986816 481.325249,292.359712 480.947677,292.359712 C480.570105,292.359712 480.264023,292.986816 480.264023,293.760388 C480.264023,293.999364 480.293234,294.224361 480.344739,294.421211 C480.084967,294.603206 479.915493,294.902267 479.915493,295.240346 C479.915493,295.794983 480.371616,296.244604 480.934272,296.244604 C481.496929,296.244604 481.953052,295.794983 481.953052,295.240346 C481.953052,294.912128 481.79332,294.620685 481.546303,294.437432 L481.546303,294.437432 Z M476.640956,289.518369 C475.051639,290.533867 474,292.297062 474,294.302158 C474,297.448795 476.589591,300 479.784014,300 L482.215986,300 C485.409741,300 488,297.448989 488,294.302158 C488,292.308265 486.960219,290.553451 485.385637,289.535322 C485.510499,287.87551 485.634902,282 481.137977,282 C476.656233,282 476.57209,287.835909 476.640956,289.518369 Z M478.233024,288.811553 C478.726641,288.676497 479.246789,288.604317 479.784014,288.604317 L482.215986,288.604317 C482.79807,288.604317 483.360072,288.689026 483.889982,288.846612 C483.71666,286.397425 483.357903,283.683453 481.137977,283.683453 C478.928835,283.683453 478.441091,286.378149 478.233024,288.811553 Z",
                    id: "Combined-Shape",
                    stroke: "none",
                    fill: "#D8D8D8",
                    "fill-rule": "evenodd"
                }
            })]), t._v(" "), t.bet.live && !t.bet.active ? a("span", [t._v("This event has expired")]) : t._e()]) : t._e(), t._v(" "), a("table", {
                staticClass: "betslip-bet"
            }, [a("tr", [a("td", [a("table", {
                staticClass: "betslip-bet--left table--100 text-left"
            }, [a("tr", [a("td", {
                staticClass: "display bold"
            }, [t._v(t._s(t.bet.display) + " "), t.bet.live ? a("span", {
                staticClass: "live"
            }, [t._v("Live")]) : t._e(), t._v(" "), t.bet.changed ? a("span", {
                staticClass: "changed"
            }, [t._v("Changed")]) : t._e()])]), t._v(" "), a("tr", [a("td", {
                staticClass: "market"
            }, [t._v(t._s(t.bet.market))])]), t._v(" "), a("tr", [a("td", {
                staticClass: "odd-display bold"
            }, [t._v("\n           " + t._s(t.bet.odd.display) + " " + t._s(t.getSpecialBetValue()) + "\n         ")])])])]), t._v(" "), a("td", [a("table", {
                staticClass: "betslip-bet--right table--100 text-right"
            }, [a("tr", [a("td", [a("span", {
                staticClass: "remove",
                on: {
                    click: function(e) {
                        t.removeBet()
                    }
                }
            }, [t._v("✕")])])]), t._v(" "), a("tr", [a("td", {
                staticClass: "odd-value bold"
            }, [t._v(t._s(t.bet.odd.odd_value))])])])])])])])
        }
            , n = [];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        var i = function() {
            var t = this
                , e = t.$createElement
                , i = t._self._c || e;
            return t.showAndroidBanner && !t.showConfirm ? i("table", {
                staticClass: "android-app-banner"
            }, [i("a", {
                ref: "appLink",
                staticClass: "hide",
                attrs: {
                    href: "http://app.betika.com/?utm_source=Betika_Mobile_Web&utm_medium=Banner&utm_campaign=Download_App&utm_content=Click_From_Notification",
                    target: "_blank"
                }
            }, [t._v("Download App")]), t._v(" "), i("tr", [i("td", [i("span", {
                staticClass: "android-app-banner__close",
                on: {
                    click: t.close
                }
            }, [t._v("×")])]), t._v(" "), i("td", [i("img", {
                staticClass: "android-app-banner__icon",
                attrs: {
                    src: a(609)
                },
                on: {
                    click: t.goToApp
                }
            })]), t._v(" "), i("td", [i("p", {
                staticClass: "android-app-banner__text",
                on: {
                    click: t.goToApp
                }
            }, [i("span", {
                staticClass: "android-app-banner__text__title"
            }, [t._v("Betika App")]), t._v(" "), i("span", {
                staticClass: "android-app-banner__text__desc"
            }, [t._v("Save on bundles, small & free!")])])]), t._v(" "), i("td", {
                staticClass: "android-app-banner__cta__container"
            }, [i("button", {
                staticClass: "android-app-banner__cta",
                on: {
                    click: t.goToApp
                }
            }, [t._v("Install")])])])]) : t.showAndroidBanner && t.showConfirm ? i("table", {
                staticClass: "android-app-banner"
            }, [i("tr", [t._m(0), t._v(" "), i("td", {
                staticClass: "android-app-banner__cta__container"
            }, [i("button", {
                staticClass: "android-app-banner__cta",
                on: {
                    click: function(e) {
                        t.confirm(!0)
                    }
                }
            }, [t._v("Yes")]), t._v(" "), i("button", {
                staticClass: "android-app-banner__cta",
                on: {
                    click: function(e) {
                        t.confirm(!1)
                    }
                }
            }, [t._v("No")])])])]) : t._e()
        }
            , n = [function() {
            var t = this
                , e = t.$createElement
                , a = t._self._c || e;
            return a("td", [a("p", {
                staticClass: "android-app-banner__text"
            }, [a("span", {
                staticClass: "android-app-banner__text__desc"
            }, [t._v("See this notification again?")])])])
        }
        ];
        a.d(e, "a", function() {
            return i
        }),
            a.d(e, "b", function() {
                return n
            })
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }),
            e.event = void 0,
            e.betterTime = l,
            e.betterTimeWithFormat = c,
            e.getParameterByName = d,
            e.truncate = u;
        var i, n = o(a(52));
        function o(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        function s(t) {
            if (Array.isArray(t)) {
                for (var e = 0, a = Array(t.length); e < t.length; e++)
                    a[e] = t[e];
                return a
            }
            return Array.from(t)
        }
        var r = a(57);
        function l(t) {
            return r(t).tz("Africa/Nairobi").format("DD/MM, HH:mm")
        }
        function c(t, e) {
            return r(t).tz("Africa/Nairobi").format(e)
        }
        function d(t, e) {
            e = window.location.href,
                t = t.replace(/[\[\]]/g, "\\$&");
            var a, i = new RegExp("[?&]" + t + "(=([^&#]*)|&|#|$)").exec(e);
            return i ? i[2] ? decodeURIComponent(i[2].replace(/\+/g, " ")) : "" : null
        }
        function u(t, e) {
            return t.substring(0, Math.min(e, t.length)) + (t.length >= e ? "..." : "")
        }
        var p = e.event = {
            bus: null,
            init: function t() {
                return this.bus || (this.bus = new n.default),
                    this
            },
            emit: function t(e) {
                for (var a, i = arguments.length, n = Array(i > 1 ? i - 1 : 0), o = 1; o < i; o++)
                    n[o - 1] = arguments[o];
                return (a = this.bus).$emit.apply(a, [e].concat(s(n))),
                    this
            },
            on: function t() {
                var e = this
                    , a = arguments;
                return 2 === arguments.length ? this.bus.$on(arguments[0], arguments[1]) : Object.keys(arguments[0]).forEach(function(t) {
                    e.bus.$on(t, a[0][t])
                }),
                    this
            }
        }
    }
    , function(t, e, a) {
        t.exports = a.p + "img/play.svg"
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(295)
            , n = a(211);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(652)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Match.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }),
            e.askForNotificationPermission = o,
            e.getNotificationStatus = s,
            e.initAddToHomeScreen = r,
            e.registerServiceWorker = l,
            e.checkPushStatus = c,
            e.subscribePush = d,
            e.unsubscribePush = u;
        var i = "https://betika-pwa-api.now.sh/api/"
            , n = "BJ8xTGECk_uDBloseJrDfQvJ_OobMLlD0ip-lp_PaVSMn5Zi5ZszGIuhDHcvh0Q-DSKDYqX9v96zHPHwxV1z15Y";
        function o() {
            return new Promise(function(t, e) {
                    "Notification"in window ? Notification.requestPermission(function(a) {
                        "denied" === Notification.permission ? e("You have blocked notifications") : t(a)
                    }) : e("This browser does not support notifications")
                }
            )
        }
        function s() {
            return "Notification"in window ? "denied" === Notification.permission ? {
                status: !1,
                reason: "Your have blocked notifications. To enable Go to Settings > Site Settings > Notifications > Allow"
            } : "default" === Notification.permission ? {
                status: !1,
                reason: "Notifications set to ask"
            } : "granted" === Notification.permission ? {
                status: !0,
                reason: "Thank you for granting notifications"
            } : {
                status: !1,
                reason: "Notifications not set"
            } : {
                status: !1,
                reason: "Your browser does not support notifications"
            }
        }
        function r(t, e, a) {
            window.addEventListener("beforeinstallprompt", function(i) {
                var n;
                navigator.userAgent.indexOf("Chrome") > -1 && t(!0),
                    fbq("trackCustom", "AddToHomeScreenPrompt"),
                    i.userChoice.then(function(t) {
                        "dismissed" == t.outcome ? (fbq("trackCustom", "DismissAddToHomeScreen"),
                            e(!1)) : (fbq("trackCustom", "AcceptAddToHomeScreen"),
                            a(!1, !0))
                    })
            })
        }
        function l() {
            return new Promise(function(t, e) {
                    "serviceWorker"in navigator ? navigator.serviceWorker.register("service-worker.js").then(function(e) {
                        t(e)
                    }).catch(function(t) {
                        e("Service Worker Error", t)
                    }) : e("Service worker is not supported")
                }
            )
        }
        function c() {
            return new Promise(function(t, e) {
                    "denied" === Notification.permission && e("You have blocked push notifications."),
                    "PushManager"in window || e("Sorry, Push notification isn't supported in your browser."),
                        navigator.serviceWorker.ready.then(function(a) {
                            a.pushManager.getSubscription().then(function(a) {
                                a ? p(a).catch(function(e) {
                                    return t(e)
                                }).then(function(e) {
                                    return t("Probably already subscribed")
                                }) : e("Access not granted")
                            }).catch(function(t) {
                                e("Error occurred while enabling push " + t)
                            })
                        }).catch(function(t) {
                            return e(t)
                        })
                }
            )
        }
        function d(t) {
            return new Promise(function(e, a) {
                    "PushManager"in window || a({
                        status: !1,
                        reason: "Sorry, Push notification isn't supported in your browser."
                    }),
                    t.pushManager || a({
                        status: !1,
                        reason: "Sorry, Push notification isn't supported in your browser."
                    }),
                        t.pushManager.subscribe({
                            userVisibleOnly: !0,
                            applicationServerKey: m(n)
                        }).then(function(t) {
                            p(t).then(function(t) {
                                return e(t)
                            }).catch(function(t) {
                                return a(t)
                            })
                        }).catch(function(t) {
                            return a(t)
                        })
                }
            )
        }
        function u() {
            return new Promise(function(t, e) {
                    navigator.serviceWorker.ready.then(function(a) {
                        a.pushManager.getSubscription().then(function(a) {
                            a || e("Unable to unregister push notification."),
                                a.unsubscribe().then(function() {
                                    f(a).then(function(e) {
                                        return t(e)
                                    }).catch(function(t) {
                                        return e(t)
                                    })
                                }).catch(function(t) {
                                    return e(t)
                                })
                        }).catch(function(t) {
                            return e(t)
                        })
                    })
                }
            )
        }
        function p(t) {
            var e = localStorage.user ? JSON.parse(localStorage.user) : null
                , a = t.endpoint.split("fcm/send/")[1]
                , n = {
                user_id: a,
                subscription: t
            };
            return e && (n.profile_id = e.id,
                n.mobile = e.mobile),
                new Promise(function(t, e) {
                        fetch(i + "users", {
                            method: "post",
                            headers: {
                                Accept: "application/json",
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify(n)
                        }).then(function(e) {
                            return t(e)
                        }).catch(function(t) {
                            return e(t)
                        })
                    }
                )
        }
        function f(t) {
            var e = t.endpoint.split("fcm/send/")[1];
            return new Promise(function(t, a) {
                    fetch(i + "user/" + e, {
                        method: "delete",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json"
                        }
                    }).then(function(e) {
                        return t(e)
                    }).catch(function(t) {
                        return a(t)
                    })
                }
            )
        }
        function m(t) {
            for (var e, a = (t + "=".repeat((4 - t.length % 4) % 4)).replace(/\-/g, "+").replace(/_/g, "/"), i = window.atob(a), n = new Uint8Array(i.length), o = 0; o < i.length; ++o)
                n[o] = i.charCodeAt(o);
            return n
        }
    }
    , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , function(t, e) {}
    , , , , , , , , , , , , function(t, e, a) {
        var i = {
            "./af": 73,
            "./af.js": 73,
            "./ar": 74,
            "./ar-dz": 75,
            "./ar-dz.js": 75,
            "./ar-kw": 76,
            "./ar-kw.js": 76,
            "./ar-ly": 77,
            "./ar-ly.js": 77,
            "./ar-ma": 78,
            "./ar-ma.js": 78,
            "./ar-sa": 79,
            "./ar-sa.js": 79,
            "./ar-tn": 80,
            "./ar-tn.js": 80,
            "./ar.js": 74,
            "./az": 81,
            "./az.js": 81,
            "./be": 82,
            "./be.js": 82,
            "./bg": 83,
            "./bg.js": 83,
            "./bm": 84,
            "./bm.js": 84,
            "./bn": 85,
            "./bn.js": 85,
            "./bo": 86,
            "./bo.js": 86,
            "./br": 87,
            "./br.js": 87,
            "./bs": 88,
            "./bs.js": 88,
            "./ca": 89,
            "./ca.js": 89,
            "./cs": 90,
            "./cs.js": 90,
            "./cv": 91,
            "./cv.js": 91,
            "./cy": 92,
            "./cy.js": 92,
            "./da": 93,
            "./da.js": 93,
            "./de": 94,
            "./de-at": 95,
            "./de-at.js": 95,
            "./de-ch": 96,
            "./de-ch.js": 96,
            "./de.js": 94,
            "./dv": 97,
            "./dv.js": 97,
            "./el": 98,
            "./el.js": 98,
            "./en-au": 99,
            "./en-au.js": 99,
            "./en-ca": 100,
            "./en-ca.js": 100,
            "./en-gb": 101,
            "./en-gb.js": 101,
            "./en-ie": 102,
            "./en-ie.js": 102,
            "./en-il": 103,
            "./en-il.js": 103,
            "./en-nz": 104,
            "./en-nz.js": 104,
            "./eo": 105,
            "./eo.js": 105,
            "./es": 106,
            "./es-do": 107,
            "./es-do.js": 107,
            "./es-us": 108,
            "./es-us.js": 108,
            "./es.js": 106,
            "./et": 109,
            "./et.js": 109,
            "./eu": 110,
            "./eu.js": 110,
            "./fa": 111,
            "./fa.js": 111,
            "./fi": 112,
            "./fi.js": 112,
            "./fo": 113,
            "./fo.js": 113,
            "./fr": 114,
            "./fr-ca": 115,
            "./fr-ca.js": 115,
            "./fr-ch": 116,
            "./fr-ch.js": 116,
            "./fr.js": 114,
            "./fy": 117,
            "./fy.js": 117,
            "./gd": 118,
            "./gd.js": 118,
            "./gl": 119,
            "./gl.js": 119,
            "./gom-latn": 120,
            "./gom-latn.js": 120,
            "./gu": 121,
            "./gu.js": 121,
            "./he": 122,
            "./he.js": 122,
            "./hi": 123,
            "./hi.js": 123,
            "./hr": 124,
            "./hr.js": 124,
            "./hu": 125,
            "./hu.js": 125,
            "./hy-am": 126,
            "./hy-am.js": 126,
            "./id": 127,
            "./id.js": 127,
            "./is": 128,
            "./is.js": 128,
            "./it": 129,
            "./it.js": 129,
            "./ja": 130,
            "./ja.js": 130,
            "./jv": 131,
            "./jv.js": 131,
            "./ka": 132,
            "./ka.js": 132,
            "./kk": 133,
            "./kk.js": 133,
            "./km": 134,
            "./km.js": 134,
            "./kn": 135,
            "./kn.js": 135,
            "./ko": 136,
            "./ko.js": 136,
            "./ky": 137,
            "./ky.js": 137,
            "./lb": 138,
            "./lb.js": 138,
            "./lo": 139,
            "./lo.js": 139,
            "./lt": 140,
            "./lt.js": 140,
            "./lv": 141,
            "./lv.js": 141,
            "./me": 142,
            "./me.js": 142,
            "./mi": 143,
            "./mi.js": 143,
            "./mk": 144,
            "./mk.js": 144,
            "./ml": 145,
            "./ml.js": 145,
            "./mn": 146,
            "./mn.js": 146,
            "./mr": 147,
            "./mr.js": 147,
            "./ms": 148,
            "./ms-my": 149,
            "./ms-my.js": 149,
            "./ms.js": 148,
            "./mt": 150,
            "./mt.js": 150,
            "./my": 151,
            "./my.js": 151,
            "./nb": 152,
            "./nb.js": 152,
            "./ne": 153,
            "./ne.js": 153,
            "./nl": 154,
            "./nl-be": 155,
            "./nl-be.js": 155,
            "./nl.js": 154,
            "./nn": 156,
            "./nn.js": 156,
            "./pa-in": 157,
            "./pa-in.js": 157,
            "./pl": 158,
            "./pl.js": 158,
            "./pt": 159,
            "./pt-br": 160,
            "./pt-br.js": 160,
            "./pt.js": 159,
            "./ro": 161,
            "./ro.js": 161,
            "./ru": 162,
            "./ru.js": 162,
            "./sd": 163,
            "./sd.js": 163,
            "./se": 164,
            "./se.js": 164,
            "./si": 165,
            "./si.js": 165,
            "./sk": 166,
            "./sk.js": 166,
            "./sl": 167,
            "./sl.js": 167,
            "./sq": 168,
            "./sq.js": 168,
            "./sr": 169,
            "./sr-cyrl": 170,
            "./sr-cyrl.js": 170,
            "./sr.js": 169,
            "./ss": 171,
            "./ss.js": 171,
            "./sv": 172,
            "./sv.js": 172,
            "./sw": 173,
            "./sw.js": 173,
            "./ta": 174,
            "./ta.js": 174,
            "./te": 175,
            "./te.js": 175,
            "./tet": 176,
            "./tet.js": 176,
            "./tg": 177,
            "./tg.js": 177,
            "./th": 178,
            "./th.js": 178,
            "./tl-ph": 179,
            "./tl-ph.js": 179,
            "./tlh": 180,
            "./tlh.js": 180,
            "./tr": 181,
            "./tr.js": 181,
            "./tzl": 182,
            "./tzl.js": 182,
            "./tzm": 183,
            "./tzm-latn": 184,
            "./tzm-latn.js": 184,
            "./tzm.js": 183,
            "./ug-cn": 185,
            "./ug-cn.js": 185,
            "./uk": 186,
            "./uk.js": 186,
            "./ur": 187,
            "./ur.js": 187,
            "./uz": 188,
            "./uz-latn": 189,
            "./uz-latn.js": 189,
            "./uz.js": 188,
            "./vi": 190,
            "./vi.js": 190,
            "./x-pseudo": 191,
            "./x-pseudo.js": 191,
            "./yo": 192,
            "./yo.js": 192,
            "./zh-cn": 193,
            "./zh-cn.js": 193,
            "./zh-hk": 194,
            "./zh-hk.js": 194,
            "./zh-tw": 195,
            "./zh-tw.js": 195
        };
        function n(t) {
            var e = o(t);
            return a(e)
        }
        function o(t) {
            var e = i[t];
            if (!(e + 1)) {
                var a = new Error("Cannot find module '" + t + "'");
                throw a.code = "MODULE_NOT_FOUND",
                    a
            }
            return e
        }
        n.keys = function t() {
            return Object.keys(i)
        }
            ,
            n.resolve = o,
            t.exports = n,
            n.id = 359
    }
    , function(t, e, a) {
        t.exports = a.p + "img/logo.png"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/oval.svg"
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(288)
            , n = a(207);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(677)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Home.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(301)
            , n = a(226);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(662)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, "1e8da584", null);
        l.options.__file = "MarketsFilter.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(305)
            , n = a(213);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(650)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, "4d721384", null);
        l.options.__file = "MatchOdd.vue",
            e.default = l.exports
    }
    , , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(300)
            , n = a(233);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(674)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Slider.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        t.exports = a.p + "img/jackpot.jpg"
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(299)
            , n = a(237);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(679)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "SnackBar.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(290)
            , n = a(245);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(688)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Betslip.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(294)
            , n = a(252);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(692)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Fab.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        a(372),
            t.exports = a(574)
    }
    , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , function(t, e, a) {
        "use strict";
        var i, n = O(a(52)), o, s = O(a(283)), r, l = O(a(348)), c, d = O(a(349)), u, p = O(a(595)), f, m = O(a(598)), h, g = O(a(18)), b = a(599), _, v = O(a(285)), w, x = O(a(602)), k = a(603), y = a(308), S, C = O(a(604)), B, M;
        function O(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        (0,
            a(311).registerServiceWorker)().then(function(t) {
            return n.default.prototype.$swRegistration = t
        }).catch(function(t) {
            return n.default.prototype.$swRegistration = null
        });
        var j = function t() {
            return Promise.resolve().then(a.bind(null, 362))
        }
            , T = function t() {
            return a.e(4).then(a.bind(null, 704))
        }
            , A = function t() {
            return a.e(10).then(a.bind(null, 705))
        }
            , N = function t() {
            return a.e(6).then(a.bind(null, 706))
        }
            , I = function t() {
            return a.e(11).then(a.bind(null, 703))
        }
            , z = function t() {
            return a.e(21).then(a.bind(null, 707))
        }
            , L = function t() {
            return a.e(13).then(a.bind(null, 708))
        }
            , E = function t() {
            return a.e(12).then(a.bind(null, 709))
        }
            , P = function t() {
            return a.e(19).then(a.bind(null, 710))
        }
            , $ = function t() {
            return a.e(7).then(a.bind(null, 711))
        }
            , F = function t() {
            return a.e(25).then(a.bind(null, 712))
        }
            , J = function t() {
            return a.e(16).then(a.bind(null, 713))
        }
            , D = function t() {
            return a.e(17).then(a.bind(null, 714))
        }
            , V = function t() {
            return a.e(9).then(a.bind(null, 715))
        }
            , U = function t() {
            return a.e(8).then(a.bind(null, 716))
        }
            , W = function t() {
            return a.e(5).then(a.bind(null, 717))
        }
            , K = function t() {
            return a.e(20).then(a.bind(null, 718))
        }
            , R = function t() {
            return a.e(14).then(a.bind(null, 719))
        }
            , H = function t() {
            return a.e(15).then(a.bind(null, 720))
        }
            , q = function t() {
            return a.e(23).then(a.bind(null, 702))
        }
            , G = function t() {
            return a.e(24).then(a.bind(null, 721))
        }
            , Y = function t() {
            return a.e(26).then(a.bind(null, 722))
        }
            , Q = function t() {
            return a.e(27).then(a.bind(null, 723))
        }
            , Z = function t() {
            return a.e(28).then(a.bind(null, 724))
        }
            , X = function t() {
            return a.e(22).then(a.bind(null, 725))
        }
            , tt = function t() {
            return a.e(18).then(a.bind(null, 726))
        }
            , et = a(699).default.get();
        window.BASE_URL = et.baseUrl,
            window.LIVE_URL = et.liveUrl,
            window.API_KEY = et.apiKey,
            window.supportsWebSockets = "function" == typeof window.WebSocket,
            n.default.component("modal", G),
            n.default.component("loader", q),
            n.default.component("back", Z),
            n.default.component("empty", X),
            n.default.filter("time", y.betterTime),
            n.default.filter("timeFormat", y.betterTimeWithFormat),
            n.default.filter("truncate", y.truncate),
            n.default.use(s.default),
            n.default.use(l.default),
            n.default.use(m.default, {
                appName: "Betika Mobile Web",
                appVersion: k.version,
                trackingId: "UA-91438552-1",
                debug: !0,
                vueRouter: at
            }),
            String.prototype.capitalize = function() {
                return this.replace(/(?:^|\s)\S/g, function(t) {
                    return t.toUpperCase()
                })
            }
            ,
            window.utils = {
                getParameterByName: y.getParameterByName
            },
            d.default.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded",
            n.default.prototype.$axios = d.default,
            n.default.prototype.$qs = p.default,
            n.default.prototype.$store = g.default,
            window.isAndroid = !1,
            n.default.prototype.isAndroid = !1,
            window.canShowAndroidBanner = !!localStorage.hasOwnProperty("showAndroidBanner") && JSON.parse(localStorage.showAndroidBanner) && window.isAndroid,
            n.default.prototype.canShowAndroidBanner = !!localStorage.hasOwnProperty("showAndroidBanner") && JSON.parse(localStorage.showAndroidBanner) && window.isAndroid,
            n.default.prototype.$version = k.version,
            n.default.prototype.$track = v.default,
            n.default.prototype.$auth = x.default,
            n.default.prototype.$cookie = b.cookie,
            window.isPWA = "pwa" == (0,
                y.getParameterByName)("utm_source"),
            window.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent),
            n.default.prototype.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
        var at = new l.default({
            routes: [{
                path: "/betslip",
                component: A
            }, {
                path: "/",
                component: j,
                meta: {
                    title: "Betika | Best Online Sports Betting in Kenya",
                    description: "We have the fastest live, instant deposits and withdrawals, deposit cashback bonus, 25bob free for new customers. Sign up or log in to your account",
                    miniBetslipViewable: !0,
                    betslipFabViewable: !0
                }
            }, {
                path: "/prebets/markets/:matchId",
                name: "prebet-markets",
                component: N,
                meta: {
                    miniBetslipViewable: !0,
                    betslipFabViewable: !0
                }
            }, {
                path: "/shikisha/markets/:parentMatchId",
                name: "jengabet-markets",
                component: I
            }, {
                path: "/login",
                component: z
            }, {
                path: "/signup",
                component: V
            }, {
                path: "/profile",
                component: L,
                beforeEnter: function t(e, a, i) {
                    JSON.parse(window.localStorage.getItem("authenticated")) ? i() : at.push("/login")
                },
                meta: {
                    betslipFabViewable: !0
                }
            }, {
                path: "/mybets",
                component: E,
                beforeEnter: function t(e, a, i) {
                    JSON.parse(window.localStorage.getItem("authenticated")) ? i() : at.push("/login")
                },
                meta: {
                    betslipFabViewable: !0
                }
            }, {
                name: "mybets-betdetail",
                path: "/mybets/:betId",
                component: P,
                beforeEnter: function t(e, a, i) {
                    JSON.parse(window.localStorage.getItem("authenticated")) ? i() : at.replace("/login")
                },
                meta: {
                    betslipFabViewable: !0
                }
            }, {
                path: "/jackpots",
                component: $,
                meta: {
                    title: "Betika | Easiest Jackpot to Win in Kenya. Only 33bob",
                    description: "Create your own JACKPOT with Betika. Pick any 13 out of 15 games to win Ksh. 5,000,000 every week"
                }
            }, {
                path: "/sports",
                component: F,
                meta: {
                    miniBetslipViewable: !0,
                    betslipFabViewable: !0
                }
            }, {
                path: "/sports/highlights",
                component: D,
                meta: {
                    miniBetslipViewable: !0,
                    betslipFabViewable: !0
                }
            }, {
                path: "/sports/:sport",
                component: J,
                meta: {
                    miniBetslipViewable: !0,
                    betslipFabViewable: !0
                }
            }, {
                path: "/top-leagues/",
                component: Y,
                meta: {
                    miniBetslipViewable: !0,
                    betslipFabViewable: !0
                }
            }, {
                path: "/top-leagues/:leagueId",
                component: Q,
                meta: {
                    miniBetslipViewable: !0,
                    betslipFabViewable: !0
                }
            }, {
                path: "/live",
                component: U,
                meta: {
                    title: "Betika | Fastest Live Betting Website in Kenya with the Highest Odds",
                    description: "Enjoy the fastest loading live betting website in Kenya. Unaweza bet game ikiendelea. Click here to PLAY live",
                    miniBetslipViewable: !0,
                    betslipFabViewable: !0
                }
            }, {
                path: "/live/markets/:matchId",
                name: "live-markets",
                component: W,
                meta: {
                    miniBetslipViewable: !0,
                    betslipFabViewable: !0
                }
            }, {
                path: "/forgotpassword",
                component: K
            }, {
                path: "/virtual",
                component: R,
                meta: {
                    title: "Betika | Win Every 3 Minutes on Betika League",
                    description: "The Betika League is modelled on the EPL. Win Upto Ksh. 1 Million Every 3 Minutes. Click here to PLAY"
                }
            }, {
                path: "/shikisha",
                name: "shikisha",
                component: T
            }, {
                path: "/betika-tv",
                name: "betika-tv",
                component: tt
            }, {
                path: "*",
                component: j
            }],
            scrollBehavior: function t(e, a, i) {
                return i || {
                    x: 0,
                    y: 0
                }
            }
        });
        at.beforeEach(function(t, e, a) {
            var i = t.matched.slice().reverse().find(function(t) {
                return t.meta && t.meta.title
            });
            if (i)
                document.title = i.meta.title,
                    document.head.querySelector("meta[name=description]").content = i.meta.description;
            else {
                var o = {
                    title: "Betika | Best Online Sports Betting in Kenya",
                    description: "We have the fastest live, instant deposits and withdrawals, deposit cashback bonus, 25bob free for new customers. Sign up or log in to your account"
                };
                document.title = o.title,
                    document.head.querySelector("meta[name=description]").content = o.description
            }
            n.default.prototype.canShowAndroidBanner = !!localStorage.hasOwnProperty("showAndroidBanner") && JSON.parse(localStorage.showAndroidBanner) && window.isAndroid,
                a()
        });
        var it = new n.default({
            router: at,
            el: "#app",
            render: function t(e) {
                return e(C.default)
            }
        })
    }
    , , , , , , , , , , , , , , , , , , , , , , , , , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i = a(57);
        function n(t) {
            try {
                bugsnagClient.notify(t)
            } catch (t) {}
        }
        function o(t) {
            return i(t).tz("Africa/Nairobi").format("DD/MM, HH:mm")
        }
        function s(t, e) {
            return t.substring(0, Math.min(e, t.length)) + (t.length >= e ? "..." : "")
        }
        var r = {
            set: function t(e, a, i) {
                var n = "";
                if (i) {
                    var o = new Date;
                    o.setTime(o.getTime() + 24 * i * 60 * 60 * 1e3),
                        n = "; expires=" + o.toUTCString()
                }
                document.cookie = e + "=" + (a || "") + n + "; path=/"
            },
            get: function t(e) {
                for (var a = e + "=", i = document.cookie.split(";"), n = 0; n < i.length; n++) {
                    for (var o = i[n]; " " == o.charAt(0); )
                        o = o.substring(1, o.length);
                    if (0 == o.indexOf(a))
                        return o.substring(a.length, o.length)
                }
                return null
            },
            delete: function t(e) {
                document.cookie = e + "=; Max-Age=-99999999;"
            }
        };
        e.logger = n,
            e.betterTime = o,
            e.truncate = s,
            e.cookie = r
    }
    , , , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i, n = r(a(18)), o, s = r(a(285));
        function r(t) {
            return t && t.__esModule ? t : {
                default: t
            }
        }
        e.default = {
            getUser: function t() {
                return localStorage.user ? JSON.parse(localStorage.user) : null
            },
            setUser: function t(e) {
                localStorage.user = JSON.stringify(e)
            },
            getBalance: function t() {
                return localStorage.user ? JSON.parse(localStorage.user).balance : null
            },
            getAuthStatus: function t() {
                return !!localStorage.authenticated && JSON.parse(localStorage.authenticated)
            },
            setAuthStatus: function t(e) {
                localStorage.authenticated = e
            },
            getToken: function t() {
                return localStorage.token ? JSON.parse(localStorage.token) : null
            },
            logout: function t() {
                var e = {
                    profile_id: n.default.getters.user.id,
                    user_alternative_id: n.default.getters.user.id,
                    mobile: n.default.getters.user.mobile,
                    app_name: "MOBILE_WEB",
                    wallet_balance: n.default.getters.user.balance,
                    wallet_currency: "KES",
                    points_balance: n.default.getters.user.points,
                    bonus_balance: n.default.getters.user.bonus
                };
                s.default.session.logout(e),
                    dataLayer.push({
                        event_name: "RegisterMobileWeb",
                        app_name: "MOBILE_WEB",
                        profile_id: n.default.getters.user.id,
                        user_alternative_id: n.default.getters.user.id,
                        mobile: n.default.getters.user.mobile,
                        wallet_balance: n.default.getters.user.balance,
                        wallet_currency: "KES",
                        points_balance: n.default.getters.user.points,
                        bonus_balance: n.default.getters.user.bonus
                    }),
                    n.default.commit("logout")
            }
        }
    }
    , function(t) {
        t.exports = {
            name: "betika-mobile-web",
            description: "Betika Mobile Web - Live betting",
            version: "2.5.2",
            author: "Eric Githinji <eric.githinji@betika.com>",
            private: !0,
            scripts: {
                dev: "node validateEnv.js && cross-env NODE_ENV=development webpack-dev-server --mode=development --open --inline --hotOnly",
                build: "node validateEnv.js && rm -rf dist/* && cross-env NODE_ENV=production webpack --mode=production --progress --hide-modules",
                staging: "node validateEnv.js && rm -rf dist/* && cross-env NODE_ENV=staging webpack --mode=production --progress --hide-modules",
                prod: "npm run backup && npm run build",
                deploy: "npm run build && surge ./dist -d https://betika-vue.surge.sh",
                update: "git pull origin master && npm install && npm run build",
                backup: "tar -czvf ../betika-mobile-web-backup.tar.gz dist",
                restore: "tar -xzvf ../betika-mobile-web-backup.tar.gz -C ./"
            },
            dependencies: {
                axios: "^0.18.0",
                "json-loader": "^0.5.7",
                moment: "^2.22.1",
                "moment-timezone": "^0.5.21",
                qs: "^6.5.2",
                "socket.io": "^2.1.1",
                swiper: "^4.4.1",
                vue: "^2.5.17",
                "vue-awesome-swiper": "^3.1.3",
                "vue-infinite-scroll": "^2.0.2",
                "vue-resource": "^1.5.1",
                "vue-router": "^3.0.1",
                "vue-socket.io": "^2.1.1-b",
                "vue-ua": "^1.5.0",
                vuex: "^3.0.1"
            },
            devDependencies: {
                "@vue/devtools": "^1.1.0",
                "@webpack-cli/migrate": "^0.1.2",
                autoprefixer: "^8.6.5",
                "babel-core": "^6.26.3",
                "babel-loader": "^7.1.5",
                "babel-plugin-syntax-dynamic-import": "^6.18.0",
                "babel-polyfill": "^6.26.0",
                "babel-preset-env": "^1.7.0",
                "copy-webpack-plugin": "^4.5.4",
                "cross-env": "^5.2.0",
                "css-loader": "^0.28.11",
                eslint: "^4.19.1",
                "eslint-config-standard": "^11.0.0",
                "eslint-plugin-promise": "^3.8.0",
                "eslint-plugin-standard": "^3.1.0",
                "extract-text-webpack-plugin": "^3.0.2",
                "file-loader": "^1.1.11",
                "friendly-errors-webpack-plugin": "^1.7.0",
                "html-webpack-plugin": "^3.2.0",
                "http-server": "^0.11.1",
                "node-sass": "^4.9.4",
                "postcss-cssnext": "^3.1.0",
                "postcss-loader": "^2.1.6",
                "replace-webpack-plugin": "^0.1.2",
                root: "^3.2.0",
                "sass-loader": "^7.1.0",
                "style-loader": "^0.21.0",
                surge: "^0.20.1",
                "sw-precache-webpack-plugin": "^0.11.5",
                "uglifyjs-webpack-plugin": "^1.3.0",
                "vue-loader": "^15.4.2",
                "vue-style-loader": "^4.1.2",
                "vue-template-compiler": "^2.5.17",
                webpack: "^4.22.0",
                "webpack-cli": "^3.1.1",
                "webpack-dev-server": "^3.1.10",
                "workbox-webpack-plugin": "^3.6.3"
            }
        }
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(289)
            , n = a(196);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(697)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "App.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        t.exports = a.p + "img/ios.png"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/login-banner.jpg"
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(291)
            , n = a(198);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(644)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Top.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(307)
            , n = a(200);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(610)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "AndroidAppBanner.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        t.exports = a.p + "img/icon-120x120.png"
    }
    , function(t, e, a) {
        "use strict";
        var i = a(202), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".android-app-banner{max-width:768px;min-width:320px;overflow-x:scroll;background:#eee;border-bottom:2px solid #cacaca;padding:10px 5px;font-family:Arial,Helvetica,sans-serif;color:#333;width:100%}.android-app-banner__close{font-size:1.5rem;cursor:pointer;vertical-align:middle}.android-app-banner__icon{height:40px;width:40px;border-radius:5px;cursor:pointer;vertical-align:middle}.android-app-banner__text{margin:0;cursor:pointer;vertical-align:middle}.android-app-banner__text__title{display:block;font-size:1.1rem;text-transform:uppercase;font-weight:700}.android-app-banner__text__desc{display:block;font-size:.8rem;font-weight:400}.android-app-banner__cta{font-family:Arial,Helvetica,sans-serif;background:#629a22;border:none;font-size:.8rem;font-weight:700;color:#fff;padding:5px 10px;border-radius:5px;cursor:pointer;vertical-align:middle}.android-app-banner__cta__container{text-align:right}", ""])
    }
    , function(t, e, a) {
        var i = {
            "./14.svg": 613,
            "./28.svg": 614,
            "./29.svg": 615,
            "./30.svg": 616,
            "./31.svg": 617,
            "./34.svg": 618,
            "./37.svg": 619,
            "./41.svg": 620,
            "./44.svg": 621,
            "./56.svg": 622,
            "./allsports.svg": 623,
            "./android.svg": 624,
            "./arrow.svg": 625,
            "./atoz.svg": 287,
            "./betslip.svg": 626,
            "./cancel.svg": 627,
            "./cash.svg": 628,
            "./chart.svg": 629,
            "./checked.svg": 630,
            "./down-arrow.svg": 631,
            "./highlights.svg": 286,
            "./home.svg": 632,
            "./jackpot.svg": 633,
            "./live.svg": 634,
            "./mybets.svg": 635,
            "./offline.svg": 636,
            "./oval.svg": 361,
            "./play.svg": 309,
            "./profile.svg": 637,
            "./redo.svg": 638,
            "./search-black.svg": 639,
            "./search.svg": 640,
            "./thin-arrowheads-pointing-down.svg": 641,
            "./warning.svg": 642,
            "./waste-bin.svg": 643
        };
        function n(t) {
            var e = o(t);
            return a(e)
        }
        function o(t) {
            var e = i[t];
            if (!(e + 1)) {
                var a = new Error("Cannot find module '" + t + "'");
                throw a.code = "MODULE_NOT_FOUND",
                    a
            }
            return e
        }
        n.keys = function t() {
            return Object.keys(i)
        }
            ,
            n.resolve = o,
            t.exports = n,
            n.id = 612
    }
    , function(t, e, a) {
        t.exports = a.p + "img/14.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/28.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/29.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/30.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/31.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/34.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/37.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/41.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/44.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/56.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/allsports.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/android.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/arrow.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/betslip.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/cancel.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/cash.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/chart.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/checked.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/down-arrow.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/home.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/jackpot.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/live.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/mybets.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/offline.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/profile.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/redo.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/search-black.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/search.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/thin-arrowheads-pointing-down.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/warning.svg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/waste-bin.svg"
    }
    , function(t, e, a) {
        "use strict";
        var i = a(203), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, "header{display:flex;flex-direction:column;transition:margin-top .8s ease;z-index:9999;position:fixed;top:0;width:100%;margin-bottom:5px;background:#d4d4d4;max-width:768px;min-width:320px;overflow-x:scroll}header .hidden{margin-top:-50px}header .top-search{background:#75b82a;background-repeat:repeat;padding:5px 0 0}header .top-search.top{display:flex;flex-direction:row;justify-content:space-between;padding:5px;color:#263b0e;align-items:center;font-size:.8em;transition:display .8s ease;box-shadow:0 3px 6px rgba(0,0,0,.16),0 3px 6px rgba(0,0,0,.23);margin-bottom:5px}header .top-search.top--hidden{display:none}header .top-search.top .balance{position:absolute;left:50%;transform:translateX(-50%);text-align:center}header .top-search.top .wallet{flex-grow:1;text-align:center;margin:0}header .top-search.top .wallet--dropdown{position:fixed;right:5px;margin-top:15px;text-align:left;background-color:#fff;color:#24367e;border-collapse:collapse;border-radius:5px;box-shadow:1px 1px 5px 2px rgba(0,0,0,.15);opacity:0;height:20vh;transition:opacity .2s cubic-bezier(.6,.04,.98,.335)}header .top-search.top .wallet--dropdown table{width:100%;border-collapse:collapse}header .top-search.top .wallet--dropdown td{padding:5px}header .top-search.top .wallet--dropdown--top{background-color:#24367e;color:#fff}header .top-search.top .right{display:flex;flex-direction:row;align-items:center}header .top-search.top .right .search-icon{margin-right:15px;text-align:center}header .top-search.top .right .search-icon__label{display:block;font-size:12px}header .top-search.top .right .betslip{flex-grow:1;text-align:right;margin-top:5px;display:flex;flex-direction:column;align-items:flex-end}header .top-search.top .right .betslip img{display:inline-block;margin-left:20px;fill:#c7c7c7}header .top-search.top .right .betslip svg{height:15px;width:15px}header .top-search.top .right .betslip svg path{fill:#4d7a1c}header .top-search.top .right .betslip--count{background-color:#ffd800;color:#333;font-weight:700;display:inline-block;width:14px;border-radius:50%;position:relative;text-align:center;top:-12px;box-shadow:0 0 20px 2px rgba(0,0,0,.15);font-size:.9em;left:-10px;padding:1px;line-height:13px}header .top-search.top .right .betslip--label{display:block;text-align:left;font-size:12px}header .top-session{display:flex;flex-direction:row;justify-content:space-around;padding:5px 0;background:#cacaca;margin:0 0 5px}header .top-session button{font-size:.72em;padding:6px 25px;border:none;border-radius:5px;text-transform:uppercase;width:calc(50% - 6px);color:#616161;box-shadow:-2px 2px 5px 0 rgba(0,0,0,.15)}header .top-session--login{background-color:#fff;font-weight:700}header .top-session--register{background-color:#24367e;font-weight:700;color:#fff!important}header .nav{display:flex;flex-direction:row;justify-content:space-between;overflow:scroll;overflow-y:hidden;transition:display .8s ease;background:#cacaca;padding:5px 0;width:100%}header .nav::-webkit-scrollbar{display:none}header .nav--hidden{display:none}header .nav .selected{background-color:#f8e892}header .nav--item{height:50px;display:block;min-width:55px;background-color:#fff;border:none;margin-right:5px;border-radius:5px;line-height:.9;color:#484848;display:flex;flex-direction:column;align-items:center;justify-content:space-around;box-shadow:0 3px 6px rgba(0,0,0,.16),0 3px 6px rgba(0,0,0,.23);position:relative}header .nav--item:last-of-type{margin-right:5px}header .nav--item .promo{position:absolute;top:-3px;right:-7px;font-size:.6em;background-color:#ff4500;color:#fff;padding:3px 5px;transform:rotate(-20deg);z-index:999999}header .nav--item.jackpots{position:relative}header .nav--item--icon{height:15px}header .nav--item--icon .live{fill:red}header .nav--item--label{font-size:.64em;font-weight:700}header .nav--item .promo--cta{background-color:red;color:#fff;font-size:.8em;border-radius:50%;text-align:center;width:15px;height:15px;line-height:1.5;display:none}header .nav--item.dummy{background-color:transparent;box-shadow:none}header .nav--item.lucky6{text-decoration:none;position:relative}header .nav--item.lucky6 .promo{position:absolute;top:-3px;right:-7px;font-size:.6em;background-color:#ff4500;color:#fff;padding:3px 5px;transform:rotate(-20deg)}header .nav--search{position:absolute;right:6px;bottom:8%;z-index:99999999;box-shadow:0 0 15px 15px hsla(0,0%,78%,.95);background-color:hsla(0,0%,78%,.95);padding:6px 4px;display:flex;align-items:center}header .nav--search.logged-in{bottom:12%!important}header .nav--search--icon{margin:0!important}header .nav--search--icon svg{height:15px}", ""])
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(292)
            , n = a(204);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(647)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Bottom.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        var i = a(206), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, "footer{position:relative;z-index:99999}footer a{text-decoration:none}footer .nav,footer a,footer a:visited{color:#7b7b7b}footer .nav{max-width:768px;min-width:320px;position:fixed;bottom:0;width:100%;background-color:#fff;display:flex;flex-direction:row;justify-content:space-around;text-align:center;font-size:.81em;text-transform:capitalize;box-shadow:0 -3px 6px rgba(0,0,0,.16),0 3px 6px rgba(0,0,0,.23);transition:bottom .4s ease-in-out}footer .nav.virtual{background-color:#ffd800;color:#24367e;font-weight:700}footer .nav.virtual svg path{fill:#24367e}footer .nav.virtual p{color:#24367e}footer .nav .router-link-active svg,footer .nav .router-link-active svg use{fill:#7b7b7b;stroke:none}footer .nav--hidden{bottom:-50px}footer .nav--item{padding:5px;width:25%;text-align:center}footer .nav--item svg{height:20px}footer .nav--item svg path{fill:#7b7b7b}footer .nav--item svg circle{fill:#24367e}footer .nav--item svg .live{fill:red}footer .nav--item p{margin:0}footer .nav--item--container{position:relative}footer .nav--item--container .promo{position:absolute;top:-3px;right:-7px;font-size:.6em;background-color:#ff4500;color:#fff;padding:3px 5px;transform:rotate(-20deg);z-index:999999}footer .nav.authenticated--item{width:20%}footer .nav__betslip{background:#ffd800;color:#454545;height:50px;border-radius:50%;width:50px;margin:-7px auto;line-height:50px;font-weight:700;font-size:20px}footer .nav__betslip__container{position:relative;box-sizing:border-box;display:block;width:70px}", ""])
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(293)
            , n = a(209);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(667)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, "2f299db1", null);
        l.options.__file = "Matches.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        var i = a(215), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".prebet-match-button[data-v-4d721384]{background-color:#fdfdfd;color:#151515;font-size:.9em;z-index:2;display:flex;flex-direction:row;justify-content:space-between;align-items:center;min-height:35px}.prebet-match-button__odd-key[data-v-4d721384]{flex:4;text-align:left}.prebet-match-button__odd-value[data-v-4d721384]{flex:1}.prebet-match-button.selected[data-v-4d721384]{background-color:#f8e892}", ""])
    }
    , function(t, e, a) {
        "use strict";
        var i = a(216), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, '.prebet-match{font-size:.9em;background-color:#fff;margin-bottom:5px;width:100%;border-radius:5px;box-shadow:0 3px 6px rgba(0,0,0,.16),0 3px 6px rgba(0,0,0,.23);display:flex;flex-direction:column}.prebet-match .time{text-align:right;padding:5px;color:#7a7a7a;font-size:.81em;font-weight:100}.prebet-match .details{padding:0 5px 5px;display:flex;align-items:center;justify-content:space-between;color:#151515}.prebet-match .details .markets{padding:3px;border:1px solid #c7c7c7}.prebet-match .details .markets.selected{background-color:#f8e892}.prebet-match .odds{border-top:1px solid #c7c7c7;display:flex}.prebet-match .odds .match-odd{border-width:0;flex:1}.prebet-match .odds .match-odd:first-of-type{border-bottom-left-radius:5px}.prebet-match .odds .match-odd:last-of-type{border-bottom-right-radius:5px}.prebet-match .odds[number-of-odds="2"] .match-odd:first-of-type{border-right:1px solid #c7c7c7}.prebet-match .odds[number-of-odds="3"] .match-odd:nth-of-type(2){border-right:1px solid #c7c7c7;border-left:1px solid #c7c7c7}', ""])
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(304)
            , n = a(217);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(657)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, "1e17f95a", null);
        l.options.__file = "AllFilters.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        var i = a(221), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".spinner{width:40px;height:40px;margin:20px auto;background-color:#fff;border-radius:100%;-webkit-animation:sk-scaleout 1s infinite ease-in-out;animation:sk-scaleout 1s infinite ease-in-out}@-webkit-keyframes sk-scaleout{0%{-webkit-transform:scale(0)}to{-webkit-transform:scale(1);opacity:0}}@keyframes sk-scaleout{0%{-webkit-transform:scale(0);transform:scale(0)}to{-webkit-transform:scale(1);transform:scale(1);opacity:0}}", ""])
    }
    , function(t, e, a) {
        "use strict";
        var i = a(222), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".markets-filter[data-v-1e17f95a]{padding:5px 0;background:#fff;width:100%;border-bottom-left-radius:5px;border-bottom-right-radius:5px;font-size:12px;text-align:right;display:flex;align-items:center;justify-content:center}.markets-filter__text[data-v-1e17f95a]{margin-right:10px;font-weight:700;padding-left:5px}.markets-filter__button[data-v-1e17f95a]{background:none;outline:none;border:none;display:flex;align-items:center}.markets-filter__button__title[data-v-1e17f95a]{margin-right:10px}.markets-filter__button__icon[data-v-1e17f95a]{width:15px;height:15px}.markets-filter__modal[data-v-1e17f95a]{max-width:700px;padding:5px}.markets-filter__modal select[data-v-1e17f95a]{width:100%;margin:10px 0;height:30px;background:#eee;border:.4px solid #333;font-size:13px}.markets-filter__modal__title[data-v-1e17f95a]{text-align:left;font-weight:lighter;margin:0;font-size:14px;font-weight:700}.markets-filter__modal__buttons[data-v-1e17f95a]{display:flex;justify-content:space-between;flex-wrap:wrap;padding-top:5px}.markets-filter__modal__button[data-v-1e17f95a]{flex-basis:calc(50% - 2.5px);padding:5px;font-size:12px;border:1px solid #333;border-radius:5px;background:none;margin-bottom:5px}.markets-filter__modal__button[data-v-1e17f95a]:active,.markets-filter__modal__button[data-v-1e17f95a]:focus{background:#f8e892;font-weight:700}.markets-filter__modal__button--selected[data-v-1e17f95a]{width:50%;height:30px;background-color:#ffd800;border:none;font-weight:700;text-transform:uppercase;color:#484848}.markets-filter-prebets[data-v-1e17f95a]{cursor:pointer}.markets-filter-prebets__dropdown[data-v-1e17f95a]{z-index:99;display:block;position:absolute;left:290px;top:77%;min-width:150px;background-color:#fff;color:#454545;border:1px solid #535353}.markets-filter-prebets__dropdown__item[data-v-1e17f95a]{padding:1em;font-weight:400}.markets-filter-prebets__dropdown__item[data-v-1e17f95a]:hover{font-weight:700;background-color:#ffd800}.markets-filter-prebets__dropdown__item--selected[data-v-1e17f95a]{background:#f8e892;font-weight:700}", ""])
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(303)
            , n = a(223);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(660)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, "447eb3fb", null);
        l.options.__file = "SportsFilter.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        var i = a(225), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".markets-filter[data-v-447eb3fb]{padding:5px 0;background:#fff;width:100%;border-bottom-left-radius:5px;border-bottom-right-radius:5px;font-size:12px;text-align:right;display:flex;align-items:center;justify-content:center;max-width:700px;margin:0 auto}.markets-filter__text[data-v-447eb3fb]{margin-right:10px;font-weight:700;padding-left:5px}.markets-filter__button[data-v-447eb3fb]{background:none;outline:none;border:none;display:flex;align-items:center}.markets-filter__button__title[data-v-447eb3fb]{margin-right:10px}.markets-filter__button__icon[data-v-447eb3fb]{width:15px;height:15px}.markets-filter__modal[data-v-447eb3fb]{max-width:700px}.markets-filter__modal__title[data-v-447eb3fb]{text-align:left;font-weight:lighter;margin:0;font-size:15px}.markets-filter__modal__buttons[data-v-447eb3fb]{display:flex;justify-content:space-between;flex-wrap:wrap;padding-top:5px}.markets-filter__modal__button[data-v-447eb3fb]{flex-basis:calc(50% - 2.5px);padding:5px;font-size:12px;border:1px solid #333;border-radius:5px;background:none;margin-bottom:5px}.markets-filter__modal__button[data-v-447eb3fb]:active,.markets-filter__modal__button[data-v-447eb3fb]:focus{background:#f8e892;font-weight:700}.markets-filter__modal__button--selected[data-v-447eb3fb]{background:#f8e892;font-weight:700;border-color:#f9d411}", ""])
    }
    , function(t, e, a) {
        "use strict";
        var i = a(228), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".markets-filter[data-v-1e8da584]{padding:5px 0;background:#fff;width:100%;border-bottom-left-radius:5px;border-bottom-right-radius:5px;font-size:12px;text-align:right;display:flex;align-items:center;justify-content:center;max-width:700px;margin:0 auto}.markets-filter__text[data-v-1e8da584]{margin-right:10px;font-weight:700;padding-left:5px}.markets-filter__button[data-v-1e8da584]{background:none;outline:none;border:none;display:flex;align-items:center}.markets-filter__button__title[data-v-1e8da584]{margin-right:10px}.markets-filter__button__icon[data-v-1e8da584]{width:15px;height:15px}.markets-filter__modal[data-v-1e8da584]{max-width:700px}.markets-filter__modal__title[data-v-1e8da584]{text-align:left;font-weight:lighter;margin:0;font-size:15px}.markets-filter__modal__buttons[data-v-1e8da584]{display:flex;justify-content:space-between;flex-wrap:wrap;padding-top:5px}.markets-filter__modal__button[data-v-1e8da584]{flex-basis:calc(50% - 2.5px);padding:5px;font-size:12px;border:1px solid #333;border-radius:5px;background:none;margin-bottom:5px}.markets-filter__modal__button[data-v-1e8da584]:active,.markets-filter__modal__button[data-v-1e8da584]:focus{background:#f8e892;font-weight:700}.markets-filter__modal__button--selected[data-v-1e8da584]{background:#f8e892;font-weight:700;border-color:#f9d411}", ""])
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(302)
            , n = a(229);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(665)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, "0467145b", null);
        l.options.__file = "TagsFilter.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        var i = a(231), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".markets-filter[data-v-0467145b]{padding:5px 0;background:#fff;width:100%;border-bottom-left-radius:5px;border-bottom-right-radius:5px;font-size:12px;text-align:right;display:flex;align-items:center;justify-content:center}.markets-filter__text[data-v-0467145b]{margin-right:10px;font-weight:700;padding-left:5px}.markets-filter__button[data-v-0467145b]{background:none;outline:none;border:none;display:flex;align-items:center}.markets-filter__button__title[data-v-0467145b]{margin-right:10px}.markets-filter__button__icon[data-v-0467145b]{width:15px;height:15px}.markets-filter__modal[data-v-0467145b]{max-width:700px}.markets-filter__modal__title[data-v-0467145b]{text-align:left;font-weight:lighter;margin:0;font-size:15px}.markets-filter__modal__buttons[data-v-0467145b]{display:flex;justify-content:space-between;flex-wrap:wrap;padding-top:5px}.markets-filter__modal__button[data-v-0467145b]{flex-basis:calc(50% - 2.5px);padding:5px;font-size:12px;border:1px solid #333;border-radius:5px;background:none;margin-bottom:5px}.markets-filter__modal__button[data-v-0467145b]:active,.markets-filter__modal__button[data-v-0467145b]:focus{background:#f8e892;font-weight:700}.markets-filter__modal__button--selected[data-v-0467145b]{background:#f8e892;font-weight:700;border-color:#f9d411}", ""])
    }
    , function(t, e, a) {
        "use strict";
        var i = a(232), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".filters[data-v-2f299db1]{width:100%;background:#fff;border-radius:4px;margin-bottom:5px}.markets-filter[data-v-2f299db1]{flex:1;padding:5px 0;background:#fff;width:100%;border-bottom-left-radius:5px;border-bottom-right-radius:5px;font-size:12px;text-align:right;display:flex;align-items:center;justify-content:center;box-shadow:0 3px 6px rgba(0,0,0,.16),0 3px 6px rgba(0,0,0,.23)}.markets-filter__text[data-v-2f299db1]{margin-right:10px;font-weight:700;padding-left:5px}.markets-filter__button[data-v-2f299db1]{background:none;outline:none;border:none;display:flex;align-items:center}.markets-filter__button__title[data-v-2f299db1]{margin-right:10px}.markets-filter__button__icon[data-v-2f299db1]{width:15px;height:15px}.markets-filter__modal__container[data-v-2f299db1]{max-width:768px;margin:0 auto}.tab-filter[data-v-2f299db1]{display:flex;flex-direction:row;overflow-x:scroll;width:100%;border-top-left-radius:5px;border-top-right-radius:5px;position:relative;background:#eee}.tab-filter__item[data-v-2f299db1]{display:inline-block;font-size:12px;white-space:nowrap;padding:8px 10px;border:none;background:#eee;border-top-left-radius:5px;border-top-right-radius:5px}.tab-filter__item.active[data-v-2f299db1]{background:#fff;font-weight:700}.matches[data-v-2f299db1]{display:flex;flex-direction:column;justify-content:space-around;align-items:center;color:#484848;width:99%;margin:0 auto;margin-bottom:50px}.matches--empty[data-v-2f299db1]{width:calc(100% - 20px);margin:0 auto;margin-top:5px;background-color:#fff;padding:10px;border-radius:5px;color:#949494}.matches--empty[data-v-2f299db1],.matches__more[data-v-2f299db1]{box-shadow:0 0 5px -4px rgba(0,0,0,.5);text-align:center;font-size:12px}.matches__more[data-v-2f299db1]{border:none;background:#fff;padding:5px 10px;border-radius:20px;text-transform:capitalize;font-weight:400;margin:10px 0;color:#444;font-family:Helvetica,Arial,Verdana,Trebuchet MS,sans-serif;font-weight:700}.prebet-filters[data-v-2f299db1]{display:flex;width:100%}.selected-filters[data-v-2f299db1]{display:flex;align-items:center;width:calc(100% - 10px);font-size:12px;text-align:center;justify-content:space-between;background:#fff;padding:5px;border-bottom-left-radius:5px;border-bottom-right-radius:5px}.selected-filters .filter-div[data-v-2f299db1]{border-radius:5px;background-color:#fff;display:flex;flex-direction:column;flex-basis:calc(100% / 3 - 4px)}.selected-filters .filter-div .filter-button[data-v-2f299db1]{background-color:#eee;border-radius:5px;padding:8px 10px;font-size:12px;text-align:left;position:relative;border:none}.selected-filters .filter-div .filter-button__icon[data-v-2f299db1]{position:absolute;right:5px;top:60%;transform:translateY(-50%)}", ""])
    }
    , function(t, e, a) {
        t.exports = a.p + "img/no-minimum-bet.jpg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/cashback.jpg"
    }
    , function(t, e, a) {
        t.exports = a.p + "img/epl-partner.jpg"
    }
    , , , function(t, e, a) {
        "use strict";
        var i = a(235), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).i(a(676), ""),
            e.push([t.i, ".slider{margin-top:155px;height:auto}.slider.android{margin-top:185px}.slider.logged-in{margin-top:115px}.slider.logged-in.android{margin-top:145px}.slider__item{width:100%;height:auto}.slider .swiper-pagination-bullet-active{background:#fff}", ""])
    }
    , , function(t, e, a) {
        "use strict";
        var i = a(236), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".content{padding-bottom:35px;max-width:768px}.content .nav{display:flex;flex-direction:row;justify-content:space-between;overflow:scroll;margin-bottom:5px;overflow-y:hidden}.content .nav .selected{background-color:#ffd800}.content .nav :first-child{margin-left:5px}.content .nav:last-child{margin-right:10px}.content .nav--item{height:50px;display:block;min-width:55px;background-color:#fff;border:none;margin-right:5px;border-radius:5px;box-shadow:0 0 10px -4px rgba(0,0,0,.75);line-height:.7;color:#24367e}.content .nav--item--icon{display:block;margin:0 auto!important}.content .nav--item--label{font-size:.8em}", ""])
    }
    , function(t, e, a) {
        "use strict";
        var i = a(239), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".snackbar{visibility:hidden;width:calc(98% - 30px);background-color:#fff;color:#333;text-align:center;border-radius:4px;position:fixed;z-index:9999999;top:10px;left:50%;transform:translateX(-50%);font-size:.9em;padding:15px;padding-top:20px;max-width:723px}.snackbar .close{color:gray;font-size:1em;position:absolute;top:5px;right:5px}.snackbar .message{margin:0}.snackbar.info{background-color:#d9edf7;color:#31708f;border:1px solid #bce8f1}.snackbar.success{background-color:#dff0d8;color:#3c763d;border:1px solid #d6e9c6}.snackbar.warning{background-color:#fcf8e3;color:#8a6d3b;border:1px solid #faebcc}.snackbar.error{background-color:#f2dede;color:#a94442;border:1px solid #ebccd1}.snackbar.default{background-color:#b3b3b3;color:#4d4d4d}.snackbar.show{visibility:visible;animation:fadein .5s}@keyframes fadein{0%{max-height:0;opacity:0}to{max-height:30px;opacity:1}}@keyframes fadeout{0%{max-height:30px;opacity:1}to{max-height:0;opacity:0}}", ""])
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(298)
            , n = a(240);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(682)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Search.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        var i = a(242), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".search{width:100%;align-items:center;justify-content:space-around;position:fixed;padding-top:15px;top:0;left:0;height:100vh;background-color:#dcdcdc;z-index:99999999;overflow-y:scroll;margin-bottom:50px}.search.hidden{visibility:hidden}.search--scrolled{display:flex;visibility:visible}.search--scrolled .betslip{display:flex!important}.search--scrolled .search--input{margin-right:5px!important}.search--scrolled .search--icon{position:fixed;height:25px;padding-top:3px}.search--scrolled .hamburger{display:block!important}.search .input{width:100%;text-align:center;position:relative}.search .input .search--input{width:calc(95% - 40px);border:0;height:40px;box-shadow:0 0 5px 2px rgba(0,0,0,.15);border-radius:2px;padding-left:40px;font-size:.8em}.search .input .search--icon{position:absolute;height:25px;right:6%;padding-top:8px}.search .input .search--close{position:absolute;right:7%;top:25%;color:#484848}.search .search--enter{font-size:1.5em;position:absolute;right:6%;padding:5px}.search--term{font-size:.9em;text-align:center}.search--results{margin-top:10px;padding-bottom:100px}.search--results .prebet-match{margin:0 auto;width:95%;margin-bottom:5px}.search--results--empty{margin:0 auto;text-align:center;font-size:.9em}.search .close{position:fixed;bottom:5%;right:45%;font-size:2.3em;z-index:99999999999;background-color:rgba(36,54,126,.6);color:#fff;box-shadow:0 0 20px 0 rgba(36,54,126,.35);border-radius:50%;height:40px;width:40px;text-align:center}.modal-enter,.modal-leave-active{opacity:0}.modal-enter .modal-container,.modal-leave-active .modal-container{-webkit-transform:scale(1.1);transform:scale(1.1)}", ""])
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(296)
            , n = a(243);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(690)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "MiniBetslip.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        a.r(e);
        var i = a(306)
            , n = a(247);
        for (var o in n)
            "default" !== o && function(t) {
                a.d(e, t, function() {
                    return n[t]
                })
            }(o);
        var s = a(686)
            , r = a(2)
            , l = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
        l.options.__file = "Bet.vue",
            e.default = l.exports
    }
    , function(t, e, a) {
        "use strict";
        var i = a(249), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, '.font-smallest{font-size:.8em}.font-small{font-size:.9em}.font-large{font-size:1.4em}.font-largest{font-size:1.6em}.border-radius-top-left{border-top-left-radius:5px}.border-radius-top-right{border-top-right-radius:5px}.border-radius-bottom-left{border-bottom-left-radius:5px}.border-radius-bottom-right{border-bottom-right-radius:5px}.hide{display:none!important}.show{display:table!important}.text-right{text-align:right}.text-left{text-align:left}.text-center{text-align:center}.theme-color{color:#24367e}.light{font-weight:lighter}.italicize{font-style:italic}.table--100{width:100%}.padding-top{padding-top:10px}.padding-bottom{padding-bottom:10px}.pull-left{float:left}.pull-right{float:right}.clearfix{*zoom:1}.clearfix:after{content:"";display:table;line-height:0;clear:both}.ml{margin-left:1.2em}.mr{margin-right:1.2em}.mt{margin-top:1.2em}.mb{margin-bottom:1.2em}.m-1-2-l{margin-left:.6em}.m-1-2-r{margin-right:.6em}.m-1-2-t{margin-top:.6em}.m-1-2-b{margin-bottom:.6em}.pl{padding-left:1em}.pr{padding-right:1em}.pt{padding-top:1em}.pb{padding-bottom:1em}.pa{padding:1em}.tl{text-align:left}.tr{text-align:right}.tc{text-align:center}.bold{font-weight:700}.invert{filter:invert(100%)}.upper{text-transform:uppercase}.lower{text-transform:lowercase}.container{position:relative;margin:0 auto;width:98%}.container .wrapper{width:100%;height:100%;z-index:10;position:absolute;top:0;left:0;display:flex;align-items:center;justify-content:center;background:hsla(0,0%,100%,.75);font-size:.8em;flex-direction:column;border-radius:5px}.container .wrapper.inactive{color:rgba(255,0,0,.6)}.container .wrapper.inactive path{fill:rgba(255,0,0,.4)}.betslip-bet{font-size:.81em;background-color:#fff;width:100%;border-radius:5px;box-shadow:0 0 5px -4px rgba(0,0,0,.5);padding:5px;margin-bottom:5px;color:#484848}.betslip-bet--left .live{background-color:#c00}.betslip-bet--left .changed,.betslip-bet--left .live{color:#fff;display:inline-table;border-radius:5px;padding:1px 3px;font-size:.8em;margin:0}.betslip-bet--left .changed{background-color:#ffb733}.betslip-bet--right td{padding:5px 0}.betslip-bet--right td .remove{background-color:#c7c7c7;border-radius:50%;padding:4px 6px;color:#fff;z-index:9999}', ""])
    }
    , function(t, e, a) {
        "use strict";
        var i = a(250), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, "", ""])
    }
    , function(t, e, a) {
        "use strict";
        var i = a(251), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".mini-betslip{position:fixed;width:100%;max-width:768px;min-width:320px;overflow-x:scroll;background-color:#ffd800;color:#333;bottom:47px;z-index:9999;padding:8px 0;transition:max-height .5s ease-in-out;visibility:hidden;max-height:0}.mini-betslip--show{visibility:visible;max-height:100%}.mini-betslip__stake{display:flex;align-items:center;flex-wrap:wrap}.mini-betslip__stake__button{background:#333;color:#fff;border-radius:5px;padding:5px;border:none;box-shadow:1px 1px 0 0 #000;margin-right:2px;margin-bottom:4px;min-width:25px}.mini-betslip__placebet{display:flex;flex-direction:row;justify-content:space-between;padding:0 5px 6px;font-size:.7rem}.mini-betslip__placebet__button{background:#333;color:#fff;border-radius:4px;padding:5px 10px;border:none;box-shadow:1px 1px 0 0 #000;margin-right:5px;margin-bottom:4px}.mini-betslip__placebet__button__container{display:flex;flex-direction:row;justify-content:space-evenly;align-items:center}.mini-betslip__clear{background:#d6b92d;padding:2px;border-radius:2px;box-shadow:inset 0 0 1px 0 rgba(0,0,0,.3)}.mini-betslip__info{font-size:.7rem;margin:0;text-align:center;position:relative;padding:0 5px}.mini-betslip__info__num{text-align:right;font-weight:700}", ""])
    }
    , function(t, e, a) {
        "use strict";
        var i = a(254), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, ".fab{height:40px;width:40px;border-radius:50%;background-color:#ffd800;color:#333;position:fixed;bottom:73px;left:calc(50% - 20px);display:flex;justify-content:center;align-items:center;border:2px solid #ffd800;font-weight:bolder;z-index:9999;box-shadow:0 5px 18px rgba(0,0,0,.25),0 1px 1px rgba(0,0,0,.2);animation:grow .15s;animation-direction:alternate;font-size:1.1rem}@keyframes grow{0%{transform:scale(0)}to{transform:scale(1)}}.fab--hidden{bottom:-50px}.fab:active{border:none}", ""])
    }
    , function(t, e, a) {
        "use strict";
        (function(t) {
                var i, n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
                        return typeof t
                    }
                    : function(t) {
                        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
                    }
                ;
                !function(o, s) {
                    var r = "0.7.17"
                        , l = ""
                        , c = "?"
                        , d = "function"
                        , u = "undefined"
                        , p = "object"
                        , f = "string"
                        , m = "major"
                        , h = "model"
                        , g = "name"
                        , b = "type"
                        , _ = "vendor"
                        , v = "version"
                        , w = "architecture"
                        , x = "console"
                        , k = "mobile"
                        , y = "tablet"
                        , S = "smarttv"
                        , C = "wearable"
                        , B = "embedded"
                        , M = {
                        extend: function t(e, a) {
                            var i = {};
                            for (var n in e)
                                a[n] && a[n].length % 2 == 0 ? i[n] = a[n].concat(e[n]) : i[n] = e[n];
                            return i
                        },
                        has: function t(e, a) {
                            return "string" == typeof e && -1 !== a.toLowerCase().indexOf(e.toLowerCase())
                        },
                        lowerize: function t(e) {
                            return e.toLowerCase()
                        },
                        major: function t(e) {
                            return "string" === (void 0 === e ? "undefined" : n(e)) ? e.replace(/[^\d\.]/g, "").split(".")[0] : void 0
                        },
                        trim: function t(e) {
                            return e.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "")
                        }
                    }
                        , O = {
                        rgx: function t(e, a) {
                            for (var i = 0, o, s, r, l, c, u; i < a.length && !c; ) {
                                var p = a[i]
                                    , f = a[i + 1];
                                for (o = s = 0; o < p.length && !c; )
                                    if (c = p[o++].exec(e))
                                        for (r = 0; r < f.length; r++)
                                            u = c[++s],
                                                "object" === (void 0 === (l = f[r]) ? "undefined" : n(l)) && l.length > 0 ? 2 == l.length ? n(l[1]) == d ? this[l[0]] = l[1].call(this, u) : this[l[0]] = l[1] : 3 == l.length ? n(l[1]) !== d || l[1].exec && l[1].test ? this[l[0]] = u ? u.replace(l[1], l[2]) : void 0 : this[l[0]] = u ? l[1].call(this, u, l[2]) : void 0 : 4 == l.length && (this[l[0]] = u ? l[3].call(this, u.replace(l[1], l[2])) : void 0) : this[l] = u || void 0;
                                i += 2
                            }
                        },
                        str: function t(e, a) {
                            for (var i in a)
                                if ("object" === n(a[i]) && a[i].length > 0) {
                                    for (var o = 0; o < a[i].length; o++)
                                        if (M.has(a[i][o], e))
                                            return "?" === i ? void 0 : i
                                } else if (M.has(a[i], e))
                                    return "?" === i ? void 0 : i;
                            return e
                        }
                    }
                        , j = {
                        browser: {
                            oldsafari: {
                                version: {
                                    "1.0": "/8",
                                    1.2: "/1",
                                    1.3: "/3",
                                    "2.0": "/412",
                                    "2.0.2": "/416",
                                    "2.0.3": "/417",
                                    "2.0.4": "/419",
                                    "?": "/"
                                }
                            }
                        },
                        device: {
                            amazon: {
                                model: {
                                    "Fire Phone": ["SD", "KF"]
                                }
                            },
                            sprint: {
                                model: {
                                    "Evo Shift 4G": "7373KT"
                                },
                                vendor: {
                                    HTC: "APA",
                                    Sprint: "Sprint"
                                }
                            }
                        },
                        os: {
                            windows: {
                                version: {
                                    ME: "4.90",
                                    "NT 3.11": "NT3.51",
                                    "NT 4.0": "NT4.0",
                                    2000: "NT 5.0",
                                    XP: ["NT 5.1", "NT 5.2"],
                                    Vista: "NT 6.0",
                                    7: "NT 6.1",
                                    8: "NT 6.2",
                                    8.1: "NT 6.3",
                                    10: ["NT 6.4", "NT 10.0"],
                                    RT: "ARM"
                                }
                            }
                        }
                    }
                        , T = {
                        browser: [[/(opera\smini)\/([\w\.-]+)/i, /(opera\s[mobiletab]+).+version\/([\w\.-]+)/i, /(opera).+version\/([\w\.]+)/i, /(opera)[\/\s]+([\w\.]+)/i], [g, v], [/(opios)[\/\s]+([\w\.]+)/i], [[g, "Opera Mini"], v], [/\s(opr)\/([\w\.]+)/i], [[g, "Opera"], v], [/(kindle)\/([\w\.]+)/i, /(lunascape|maxthon|netfront|jasmine|blazer)[\/\s]?([\w\.]+)*/i, /(avant\s|iemobile|slim|baidu)(?:browser)?[\/\s]?([\w\.]*)/i, /(?:ms|\()(ie)\s([\w\.]+)/i, /(rekonq)\/([\w\.]+)*/i, /(chromium|flock|rockmelt|midori|epiphany|silk|skyfire|ovibrowser|bolt|iron|vivaldi|iridium|phantomjs|bowser)\/([\w\.-]+)/i], [g, v], [/(trident).+rv[:\s]([\w\.]+).+like\sgecko/i], [[g, "IE"], v], [/(edge)\/((\d+)?[\w\.]+)/i], [g, v], [/(yabrowser)\/([\w\.]+)/i], [[g, "Yandex"], v], [/(puffin)\/([\w\.]+)/i], [[g, "Puffin"], v], [/((?:[\s\/])uc?\s?browser|(?:juc.+)ucweb)[\/\s]?([\w\.]+)/i], [[g, "UCBrowser"], v], [/(comodo_dragon)\/([\w\.]+)/i], [[g, /_/g, " "], v], [/(micromessenger)\/([\w\.]+)/i], [[g, "WeChat"], v], [/(QQ)\/([\d\.]+)/i], [g, v], [/m?(qqbrowser)[\/\s]?([\w\.]+)/i], [g, v], [/xiaomi\/miuibrowser\/([\w\.]+)/i], [v, [g, "MIUI Browser"]], [/;fbav\/([\w\.]+);/i], [v, [g, "Facebook"]], [/headlesschrome(?:\/([\w\.]+)|\s)/i], [v, [g, "Chrome Headless"]], [/\swv\).+(chrome)\/([\w\.]+)/i], [[g, /(.+)/, "$1 WebView"], v], [/((?:oculus|samsung)browser)\/([\w\.]+)/i], [[g, /(.+(?:g|us))(.+)/, "$1 $2"], v], [/android.+version\/([\w\.]+)\s+(?:mobile\s?safari|safari)*/i], [v, [g, "Android Browser"]], [/(chrome|omniweb|arora|[tizenoka]{5}\s?browser)\/v?([\w\.]+)/i], [g, v], [/(dolfin)\/([\w\.]+)/i], [[g, "Dolphin"], v], [/((?:android.+)crmo|crios)\/([\w\.]+)/i], [[g, "Chrome"], v], [/(coast)\/([\w\.]+)/i], [[g, "Opera Coast"], v], [/fxios\/([\w\.-]+)/i], [v, [g, "Firefox"]], [/version\/([\w\.]+).+?mobile\/\w+\s(safari)/i], [v, [g, "Mobile Safari"]], [/version\/([\w\.]+).+?(mobile\s?safari|safari)/i], [v, g], [/webkit.+?(gsa)\/([\w\.]+).+?(mobile\s?safari|safari)(\/[\w\.]+)/i], [[g, "GSA"], v], [/webkit.+?(mobile\s?safari|safari)(\/[\w\.]+)/i], [g, [v, O.str, j.browser.oldsafari.version]], [/(konqueror)\/([\w\.]+)/i, /(webkit|khtml)\/([\w\.]+)/i], [g, v], [/(navigator|netscape)\/([\w\.-]+)/i], [[g, "Netscape"], v], [/(swiftfox)/i, /(icedragon|iceweasel|camino|chimera|fennec|maemo\sbrowser|minimo|conkeror)[\/\s]?([\w\.\+]+)/i, /(firefox|seamonkey|k-meleon|icecat|iceape|firebird|phoenix|palemoon|basilisk|waterfox)\/([\w\.-]+)$/i, /(mozilla)\/([\w\.]+).+rv\:.+gecko\/\d+/i, /(polaris|lynx|dillo|icab|doris|amaya|w3m|netsurf|sleipnir)[\/\s]?([\w\.]+)/i, /(links)\s\(([\w\.]+)/i, /(gobrowser)\/?([\w\.]+)*/i, /(ice\s?browser)\/v?([\w\._]+)/i, /(mosaic)[\/\s]([\w\.]+)/i], [g, v]],
                        cpu: [[/(?:(amd|x(?:(?:86|64)[_-])?|wow|win)64)[;\)]/i], [[w, "amd64"]], [/(ia32(?=;))/i], [[w, M.lowerize]], [/((?:i[346]|x)86)[;\)]/i], [[w, "ia32"]], [/windows\s(ce|mobile);\sppc;/i], [[w, "arm"]], [/((?:ppc|powerpc)(?:64)?)(?:\smac|;|\))/i], [[w, /ower/, "", M.lowerize]], [/(sun4\w)[;\)]/i], [[w, "sparc"]], [/((?:avr32|ia64(?=;))|68k(?=\))|arm(?:64|(?=v\d+;))|(?=atmel\s)avr|(?:irix|mips|sparc)(?:64)?(?=;)|pa-risc)/i], [[w, M.lowerize]]],
                        device: [[/\((ipad|playbook);[\w\s\);-]+(rim|apple)/i], [h, _, [b, y]], [/applecoremedia\/[\w\.]+ \((ipad)/], [h, [_, "Apple"], [b, y]], [/(apple\s{0,1}tv)/i], [[h, "Apple TV"], [_, "Apple"]], [/(archos)\s(gamepad2?)/i, /(hp).+(touchpad)/i, /(hp).+(tablet)/i, /(kindle)\/([\w\.]+)/i, /\s(nook)[\w\s]+build\/(\w+)/i, /(dell)\s(strea[kpr\s\d]*[\dko])/i], [_, h, [b, y]], [/(kf[A-z]+)\sbuild\/[\w\.]+.*silk\//i], [h, [_, "Amazon"], [b, y]], [/(sd|kf)[0349hijorstuw]+\sbuild\/[\w\.]+.*silk\//i], [[h, O.str, j.device.amazon.model], [_, "Amazon"], [b, k]], [/\((ip[honed|\s\w*]+);.+(apple)/i], [h, _, [b, k]], [/\((ip[honed|\s\w*]+);/i], [h, [_, "Apple"], [b, k]], [/(blackberry)[\s-]?(\w+)/i, /(blackberry|benq|palm(?=\-)|sonyericsson|acer|asus|dell|meizu|motorola|polytron)[\s_-]?([\w-]+)*/i, /(hp)\s([\w\s]+\w)/i, /(asus)-?(\w+)/i], [_, h, [b, k]], [/\(bb10;\s(\w+)/i], [h, [_, "BlackBerry"], [b, k]], [/android.+(transfo[prime\s]{4,10}\s\w+|eeepc|slider\s\w+|nexus 7|padfone)/i], [h, [_, "Asus"], [b, y]], [/(sony)\s(tablet\s[ps])\sbuild\//i, /(sony)?(?:sgp.+)\sbuild\//i], [[_, "Sony"], [h, "Xperia Tablet"], [b, y]], [/android.+\s([c-g]\d{4}|so[-l]\w+)\sbuild\//i], [h, [_, "Sony"], [b, k]], [/\s(ouya)\s/i, /(nintendo)\s([wids3u]+)/i], [_, h, [b, x]], [/android.+;\s(shield)\sbuild/i], [h, [_, "Nvidia"], [b, x]], [/(playstation\s[34portablevi]+)/i], [h, [_, "Sony"], [b, x]], [/(sprint\s(\w+))/i], [[_, O.str, j.device.sprint.vendor], [h, O.str, j.device.sprint.model], [b, k]], [/(lenovo)\s?(S(?:5000|6000)+(?:[-][\w+]))/i], [_, h, [b, y]], [/(htc)[;_\s-]+([\w\s]+(?=\))|\w+)*/i, /(zte)-(\w+)*/i, /(alcatel|geeksphone|lenovo|nexian|panasonic|(?=;\s)sony)[_\s-]?([\w-]+)*/i], [_, [h, /_/g, " "], [b, k]], [/(nexus\s9)/i], [h, [_, "HTC"], [b, y]], [/d\/huawei([\w\s-]+)[;\)]/i, /(nexus\s6p)/i], [h, [_, "Huawei"], [b, k]], [/(microsoft);\s(lumia[\s\w]+)/i], [_, h, [b, k]], [/[\s\(;](xbox(?:\sone)?)[\s\);]/i], [h, [_, "Microsoft"], [b, x]], [/(kin\.[onetw]{3})/i], [[h, /\./g, " "], [_, "Microsoft"], [b, k]], [/\s(milestone|droid(?:[2-4x]|\s(?:bionic|x2|pro|razr))?(:?\s4g)?)[\w\s]+build\//i, /mot[\s-]?(\w+)*/i, /(XT\d{3,4}) build\//i, /(nexus\s6)/i], [h, [_, "Motorola"], [b, k]], [/android.+\s(mz60\d|xoom[\s2]{0,2})\sbuild\//i], [h, [_, "Motorola"], [b, y]], [/hbbtv\/\d+\.\d+\.\d+\s+\([\w\s]*;\s*(\w[^;]*);([^;]*)/i], [[_, M.trim], [h, M.trim], [b, S]], [/hbbtv.+maple;(\d+)/i], [[h, /^/, "SmartTV"], [_, "Samsung"], [b, S]], [/\(dtv[\);].+(aquos)/i], [h, [_, "Sharp"], [b, S]], [/android.+((sch-i[89]0\d|shw-m380s|gt-p\d{4}|gt-n\d+|sgh-t8[56]9|nexus 10))/i, /((SM-T\w+))/i], [[_, "Samsung"], h, [b, y]], [/smart-tv.+(samsung)/i], [_, [b, S], h], [/((s[cgp]h-\w+|gt-\w+|galaxy\snexus|sm-\w[\w\d]+))/i, /(sam[sung]*)[\s-]*(\w+-?[\w-]*)*/i, /sec-((sgh\w+))/i], [[_, "Samsung"], h, [b, k]], [/sie-(\w+)*/i], [h, [_, "Siemens"], [b, k]], [/(maemo|nokia).*(n900|lumia\s\d+)/i, /(nokia)[\s_-]?([\w-]+)*/i], [[_, "Nokia"], h, [b, k]], [/android\s3\.[\s\w;-]{10}(a\d{3})/i], [h, [_, "Acer"], [b, y]], [/android.+([vl]k\-?\d{3})\s+build/i], [h, [_, "LG"], [b, y]], [/android\s3\.[\s\w;-]{10}(lg?)-([06cv9]{3,4})/i], [[_, "LG"], h, [b, y]], [/(lg) netcast\.tv/i], [_, h, [b, S]], [/(nexus\s[45])/i, /lg[e;\s\/-]+(\w+)*/i, /android.+lg(\-?[\d\w]+)\s+build/i], [h, [_, "LG"], [b, k]], [/android.+(ideatab[a-z0-9\-\s]+)/i], [h, [_, "Lenovo"], [b, y]], [/linux;.+((jolla));/i], [_, h, [b, k]], [/((pebble))app\/[\d\.]+\s/i], [_, h, [b, C]], [/android.+;\s(oppo)\s?([\w\s]+)\sbuild/i], [_, h, [b, k]], [/crkey/i], [[h, "Chromecast"], [_, "Google"]], [/android.+;\s(glass)\s\d/i], [h, [_, "Google"], [b, C]], [/android.+;\s(pixel c)\s/i], [h, [_, "Google"], [b, y]], [/android.+;\s(pixel xl|pixel)\s/i], [h, [_, "Google"], [b, k]], [/android.+(\w+)\s+build\/hm\1/i, /android.+(hm[\s\-_]*note?[\s_]*(?:\d\w)?)\s+build/i, /android.+(mi[\s\-_]*(?:one|one[\s_]plus|note lte)?[\s_]*(?:\d\w)?)\s+build/i, /android.+(redmi[\s\-_]*(?:note)?(?:[\s_]*[\w\s]+)?)\s+build/i], [[h, /_/g, " "], [_, "Xiaomi"], [b, k]], [/android.+(mi[\s\-_]*(?:pad)?(?:[\s_]*[\w\s]+)?)\s+build/i], [[h, /_/g, " "], [_, "Xiaomi"], [b, y]], [/android.+;\s(m[1-5]\snote)\sbuild/i], [h, [_, "Meizu"], [b, y]], [/android.+a000(1)\s+build/i], [h, [_, "OnePlus"], [b, k]], [/android.+[;\/]\s*(RCT[\d\w]+)\s+build/i], [h, [_, "RCA"], [b, y]], [/android.+[;\/]\s*(Venue[\d\s]*)\s+build/i], [h, [_, "Dell"], [b, y]], [/android.+[;\/]\s*(Q[T|M][\d\w]+)\s+build/i], [h, [_, "Verizon"], [b, y]], [/android.+[;\/]\s+(Barnes[&\s]+Noble\s+|BN[RT])(V?.*)\s+build/i], [[_, "Barnes & Noble"], h, [b, y]], [/android.+[;\/]\s+(TM\d{3}.*\b)\s+build/i], [h, [_, "NuVision"], [b, y]], [/android.+[;\/]\s*(zte)?.+(k\d{2})\s+build/i], [[_, "ZTE"], h, [b, y]], [/android.+[;\/]\s*(gen\d{3})\s+build.*49h/i], [h, [_, "Swiss"], [b, k]], [/android.+[;\/]\s*(zur\d{3})\s+build/i], [h, [_, "Swiss"], [b, y]], [/android.+[;\/]\s*((Zeki)?TB.*\b)\s+build/i], [h, [_, "Zeki"], [b, y]], [/(android).+[;\/]\s+([YR]\d{2}x?.*)\s+build/i, /android.+[;\/]\s+(Dragon[\-\s]+Touch\s+|DT)(.+)\s+build/i], [[_, "Dragon Touch"], h, [b, y]], [/android.+[;\/]\s*(NS-?.+)\s+build/i], [h, [_, "Insignia"], [b, y]], [/android.+[;\/]\s*((NX|Next)-?.+)\s+build/i], [h, [_, "NextBook"], [b, y]], [/android.+[;\/]\s*(Xtreme\_?)?(V(1[045]|2[015]|30|40|60|7[05]|90))\s+build/i], [[_, "Voice"], h, [b, k]], [/android.+[;\/]\s*(LVTEL\-?)?(V1[12])\s+build/i], [[_, "LvTel"], h, [b, k]], [/android.+[;\/]\s*(V(100MD|700NA|7011|917G).*\b)\s+build/i], [h, [_, "Envizen"], [b, y]], [/android.+[;\/]\s*(Le[\s\-]+Pan)[\s\-]+(.*\b)\s+build/i], [_, h, [b, y]], [/android.+[;\/]\s*(Trio[\s\-]*.*)\s+build/i], [h, [_, "MachSpeed"], [b, y]], [/android.+[;\/]\s*(Trinity)[\-\s]*(T\d{3})\s+build/i], [_, h, [b, y]], [/android.+[;\/]\s*TU_(1491)\s+build/i], [h, [_, "Rotor"], [b, y]], [/android.+(KS(.+))\s+build/i], [h, [_, "Amazon"], [b, y]], [/android.+(Gigaset)[\s\-]+(Q.+)\s+build/i], [_, h, [b, y]], [/\s(tablet|tab)[;\/]/i, /\s(mobile)(?:[;\/]|\ssafari)/i], [[b, M.lowerize], _, h], [/(android.+)[;\/].+build/i], [h, [_, "Generic"]]],
                        engine: [[/windows.+\sedge\/([\w\.]+)/i], [v, [g, "EdgeHTML"]], [/(presto)\/([\w\.]+)/i, /(webkit|trident|netfront|netsurf|amaya|lynx|w3m)\/([\w\.]+)/i, /(khtml|tasman|links)[\/\s]\(?([\w\.]+)/i, /(icab)[\/\s]([23]\.[\d\.]+)/i], [g, v], [/rv\:([\w\.]+).*(gecko)/i], [v, g]],
                        os: [[/microsoft\s(windows)\s(vista|xp)/i], [g, v], [/(windows)\snt\s6\.2;\s(arm)/i, /(windows\sphone(?:\sos)*)[\s\/]?([\d\.\s]+\w)*/i, /(windows\smobile|windows)[\s\/]?([ntce\d\.\s]+\w)/i], [g, [v, O.str, j.os.windows.version]], [/(win(?=3|9|n)|win\s9x\s)([nt\d\.]+)/i], [[g, "Windows"], [v, O.str, j.os.windows.version]], [/\((bb)(10);/i], [[g, "BlackBerry"], v], [/(blackberry)\w*\/?([\w\.]+)*/i, /(tizen)[\/\s]([\w\.]+)/i, /(android|webos|palm\sos|qnx|bada|rim\stablet\sos|meego|contiki)[\/\s-]?([\w\.]+)*/i, /linux;.+(sailfish);/i], [g, v], [/(symbian\s?os|symbos|s60(?=;))[\/\s-]?([\w\.]+)*/i], [[g, "Symbian"], v], [/\((series40);/i], [g], [/mozilla.+\(mobile;.+gecko.+firefox/i], [[g, "Firefox OS"], v], [/(nintendo|playstation)\s([wids34portablevu]+)/i, /(mint)[\/\s\(]?(\w+)*/i, /(mageia|vectorlinux)[;\s]/i, /(joli|[kxln]?ubuntu|debian|[open]*suse|gentoo|(?=\s)arch|slackware|fedora|mandriva|centos|pclinuxos|redhat|zenwalk|linpus)[\/\s-]?(?!chrom)([\w\.-]+)*/i, /(hurd|linux)\s?([\w\.]+)*/i, /(gnu)\s?([\w\.]+)*/i], [g, v], [/(cros)\s[\w]+\s([\w\.]+\w)/i], [[g, "Chromium OS"], v], [/(sunos)\s?([\w\.]+\d)*/i], [[g, "Solaris"], v], [/\s([frentopc-]{0,4}bsd|dragonfly)\s?([\w\.]+)*/i], [g, v], [/(haiku)\s(\w+)/i], [g, v], [/cfnetwork\/.+darwin/i, /ip[honead]+(?:.*os\s([\w]+)\slike\smac|;\sopera)/i], [[v, /_/g, "."], [g, "iOS"]], [/(mac\sos\sx)\s?([\w\s\.]+\w)*/i, /(macintosh|mac(?=_powerpc)\s)/i], [[g, "Mac OS"], [v, /_/g, "."]], [/((?:open)?solaris)[\/\s-]?([\w\.]+)*/i, /(aix)\s((\d)(?=\.|\)|\s)[\w\.]*)*/i, /(plan\s9|minix|beos|os\/2|amigaos|morphos|risc\sos|openvms)/i, /(unix)\s?([\w\.]+)*/i], [g, v]]
                    }
                        , A = function t(e, a) {
                        if ("object" === (void 0 === e ? "undefined" : n(e)) && (a = e,
                            e = void 0),
                            !(this instanceof t))
                            return new t(e,a).getResult();
                        var i = e || (o && o.navigator && o.navigator.userAgent ? o.navigator.userAgent : "")
                            , s = a ? M.extend(T, a) : T;
                        return this.getBrowser = function() {
                            var t = {
                                name: void 0,
                                version: void 0
                            };
                            return O.rgx.call(t, i, s.browser),
                                t.major = M.major(t.version),
                                t
                        }
                            ,
                            this.getCPU = function() {
                                var t = {
                                    architecture: void 0
                                };
                                return O.rgx.call(t, i, s.cpu),
                                    t
                            }
                            ,
                            this.getDevice = function() {
                                var t = {
                                    vendor: void 0,
                                    model: void 0,
                                    type: void 0
                                };
                                return O.rgx.call(t, i, s.device),
                                    t
                            }
                            ,
                            this.getEngine = function() {
                                var t = {
                                    name: void 0,
                                    version: void 0
                                };
                                return O.rgx.call(t, i, s.engine),
                                    t
                            }
                            ,
                            this.getOS = function() {
                                var t = {
                                    name: void 0,
                                    version: void 0
                                };
                                return O.rgx.call(t, i, s.os),
                                    t
                            }
                            ,
                            this.getResult = function() {
                                return {
                                    ua: this.getUA(),
                                    browser: this.getBrowser(),
                                    engine: this.getEngine(),
                                    os: this.getOS(),
                                    device: this.getDevice(),
                                    cpu: this.getCPU()
                                }
                            }
                            ,
                            this.getUA = function() {
                                return i
                            }
                            ,
                            this.setUA = function(t) {
                                return i = t,
                                    this
                            }
                            ,
                            this
                    };
                    A.VERSION = "0.7.17",
                        A.BROWSER = {
                            NAME: g,
                            MAJOR: m,
                            VERSION: v
                        },
                        A.CPU = {
                            ARCHITECTURE: w
                        },
                        A.DEVICE = {
                            MODEL: h,
                            VENDOR: _,
                            TYPE: b,
                            CONSOLE: x,
                            MOBILE: k,
                            SMARTTV: S,
                            TABLET: y,
                            WEARABLE: C,
                            EMBEDDED: B
                        },
                        A.ENGINE = {
                            NAME: g,
                            VERSION: v
                        },
                        A.OS = {
                            NAME: g,
                            VERSION: v
                        },
                        n(e) !== u ? (n(t) !== u && t.exports && (e = t.exports = A),
                            e.UAParser = A) : n(a(695)) === d && a(696) ? void 0 === (i = function() {
                            return A
                        }
                            .call(e, a, e, t)) || (t.exports = i) : o && (o.UAParser = A);
                    var N = o && (o.jQuery || o.Zepto);
                    if ((void 0 === N ? "undefined" : n(N)) !== u) {
                        var I = new A;
                        N.ua = I.getResult(),
                            N.ua.get = function() {
                                return I.getUA()
                            }
                            ,
                            N.ua.set = function(t) {
                                I.setUA(t);
                                var e = I.getResult();
                                for (var a in e)
                                    N.ua[a] = e[a]
                            }
                    }
                }("object" === ("undefined" == typeof window ? "undefined" : n(window)) ? window : void 0)
            }
        ).call(this, a(358)(t))
    }
    , , , function(t, e, a) {
        "use strict";
        var i = a(255), n, o = a.n(i).a
    }
    , function(t, e, a) {
        (e = t.exports = a(8)(!1)).push([t.i, "@import url(https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700);", ""]),
            e.push([t.i, '/*! normalize.css v5.0.0 | MIT License | github.com/necolas/normalize.css */html{font-family:sans-serif;line-height:1.15;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,footer,header,nav,section{display:block}h1{font-size:2em;margin:.67em 0}figcaption,figure,main{display:block}figure{margin:1em 40px}hr{box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace,monospace;font-size:1em}a{background-color:transparent;-webkit-text-decoration-skip:objects}a:active,a:hover{outline-width:0}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:inherit;font-weight:bolder}code,kbd,samp{font-family:monospace,monospace;font-size:1em}dfn{font-style:italic}mark{background-color:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}audio,video{display:inline-block}audio:not([controls]){display:none;height:0}img{border-style:none}svg:not(:root){overflow:hidden}button,input,optgroup,select,textarea{font-family:sans-serif;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}[type=reset],[type=submit],button,html [type=button]{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring,button:-moz-focusring{outline:1px dotted ButtonText}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{display:inline-block;vertical-align:baseline}textarea{overflow:auto}[type=checkbox],[type=radio]{box-sizing:border-box;padding:0}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-cancel-button,[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details,menu{display:block}summary{display:list-item}canvas{display:inline-block}[hidden],template{display:none}html{background-color:#d4d4d4;background-repeat:repeat;max-width:768px;min-width:320px;overflow-x:scroll;margin:0 auto}button,html{font-family:Arial,Helvetica,sans-serif;font-family:Roboto Condensed,sans-serif}button,input{outline:none}button:disabled,input:disabled{pointer-events:none}main{margin:0 auto}input[type=color],input[type=date],input[type=datetime-local],input[type=datetime],input[type=email],input[type=month],input[type=number],input[type=password],input[type=search],input[type=tel],input[type=text],input[type=time],input[type=url],input[type=week],select,textarea{font-size:16px}.font-smallest{font-size:.8em}.font-small{font-size:.9em}.font-large{font-size:1.4em}.font-largest{font-size:1.6em}.border-radius-top-left{border-top-left-radius:5px}.border-radius-top-right{border-top-right-radius:5px}.border-radius-bottom-left{border-bottom-left-radius:5px}.border-radius-bottom-right{border-bottom-right-radius:5px}.hide{display:none!important}.show{display:table!important}.text-right{text-align:right}.text-left{text-align:left}.text-center{text-align:center}.theme-color{color:#24367e}.light{font-weight:lighter}.italicize{font-style:italic}.table--100{width:100%}.padding-top{padding-top:10px}.padding-bottom{padding-bottom:10px}.pull-left{float:left}.pull-right{float:right}.clearfix{*zoom:1}.clearfix:after{content:"";display:table;line-height:0;clear:both}.ml{margin-left:1.2em}.mr{margin-right:1.2em}.mt{margin-top:1.2em}.mb{margin-bottom:1.2em}.m-1-2-l{margin-left:.6em}.m-1-2-r{margin-right:.6em}.m-1-2-t{margin-top:.6em}.m-1-2-b{margin-bottom:.6em}.pl{padding-left:1em}.pr{padding-right:1em}.pt{padding-top:1em}.pb{padding-bottom:1em}.pa{padding:1em}.tl{text-align:left}.tr{text-align:right}.tc{text-align:center}.bold{font-weight:700}.invert{filter:invert(100%)}.upper{text-transform:uppercase}.lower{text-transform:lowercase}.filter{display:flex;flex-direction:row;justify-content:space-around;color:#333;text-align:center;text-transform:uppercase;font-size:.8em;font-weight:700;box-shadow:0 0 6px rgba(0,0,0,.16),0 0 6px rgba(0,0,0,.23);width:100%;border-radius:5px}.filter--hidden{display:none}.filter--item{background-color:#fff;font-size:12px;border-bottom:1px solid #eee;width:100%;padding:7px 0}.filter--item:first-child{border-top-left-radius:5px;border-bottom-left-radius:5px}.filter--item:last-child{border-top-right-radius:5px;border-bottom-right-radius:5px}.filter--item.selected{border-bottom:2px solid #333}.button{display:inline-block;text-decoration:none;box-sizing:border-box;color:#fff;padding:.5em 1em;border-radius:4px;border:none;margin:.2em auto;background:#c7c7c7;cursor:pointer}.button__secondary{background:#27357f;color:#fff}.button__primary{color:#454545;background:#ffd800}.live-marker{background:red}.live-marker,.upcoming-marker{color:#fff;padding:.25em .5em;font-size:11px;border-radius:4px}.upcoming-marker{background:#333}main.android-banner-active{padding-top:65px}.online{text-align:center;display:flex;align-items:center;justify-content:center;z-index:99999;position:fixed;top:0;width:100%;font-size:.9em;color:#fff}.online .icon{fill:red}.online p{padding:10px}.modal{position:fixed;top:0;right:0;bottom:0;left:0;background:rgba(0,0,0,.25);z-index:100000000000000000;pointer-events:none}.modal__container{margin:0 auto;margin-top:20px;width:90%;background:#fff;box-shadow:0 2px 5px 0 hsla(0,0%,100%,.15);pointer-events:all;padding:10px;position:relative}.modal__container__close{position:absolute;top:5px;right:10px;color:#333;font-size:1.5rem;cursor:pointer}.modal__container__close--left{right:95%}.modal--active{display:""}.claim-bonus-modal__container{margin-top:5px;display:flex;flex-direction:column;align-items:center;background:#75b82a}.claim-bonus-modal__container--safari{position:fixed;bottom:5px;left:50%;transform:translateX(-50%)}.claim-bonus-modal__title{color:#24367e;text-transform:uppercase;margin:20px 0;font-size:15px;font-weight:700;width:80%}.claim-bonus-modal__list__item{font-size:20px;line-height:20px;margin-bottom:15px 0;color:#fff;text-transform:uppercase;text-align:center}.claim-bonus-modal__list__item__with-icon{display:flex;flex-direction:row;align-items:center;justify-content:center}.claim-bonus-modal__dotdot{margin:0 10px}.claim-bonus-modal__pointer{position:absolute;top:5px;right:5px;height:150px}.add-to-homescreen-modal__container{text-align:center;color:#333;background:#75b82a;text-transform:uppercase}.add-to-homescreen-modal__text{color:#fff;font-weight:700}.add-to-homescreen-modal__cta{color:#24367e;font-weight:700;font-size:1.2rem}.add-to-homescreen-modal__list{text-align:left;width:80%;margin:0 auto;color:#fff}.add-to-homescreen-modal svg{animation:bounce 2s infinite}@keyframes bounce{0%,to{transform:translateY(0)}50%{transform:translateY(-5px)}}.banner{position:fixed;z-index:1000000;top:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,.5);transition:opacity .3s ease;cursor:pointer}.banner__container{position:relative;width:90vw;max-width:768px;margin:0 auto;top:10%;cursor:default}.banner__close{position:absolute;height:25px;width:25px;border-radius:50%;background:#fff;color:#454545;font-size:1rem;right:-12.5px;top:-12.5px;text-align:center;line-height:25px;box-shadow:0 2px 5px 0 rgba(0,0,0,.1);cursor:pointer}.banner__image{width:100%}.banner__message__container{position:absolute;top:20px;left:5%;width:60%;display:flex;flex-direction:column}.banner__message_title{color:#fef341;font-size:1.4rem;font-weight:700}.banner__message_content{color:#fff;font-size:1.2rem;font-weight:700;line-height:1.4rem}', ""])
    }
    , function(t, e, a) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i = a(700)
            , n = {
            get: function t() {
                return i
            }
        };
        e.default = n
    }
    , function(t) {
        t.exports = {
            baseUrl: "https://api.betika.com/v1/",
            liveUrl: "https://live.betika.com/v1/",
            apiKey: "",
            socketUrl: "https://ws.betika.com"
        }
    }
]));
