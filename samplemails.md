Hi [NAME],

I’m [My Name], a [College Name] student from [City]. In the past three years, I’ve helped my clients generate an
 additional $250,000 in revenue, and have worked for major brands across Canada and the United States through 
 my web development agency, Toy Soldier Marketing. I came across your [INDUSTRY] site, and it hasn’t been updated since
 [YEAR]. Here’s a mock-up of what your site could look like:

[Link to Wordpress template in their niche, through a subdomain of mine]

Your new site will be mobile-responsive (it isn’t currently), and indexed correctly for Google search engines, 
which will improve your search ranking. Everything is completely customizable and will make your site stand out from the competition.

Do you want more customers? If so, I can help. Message me, let’s chat.

-Neal
